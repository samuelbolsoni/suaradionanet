<?php
$mensagem_enviada = "<!DOCTYPE html>
<html>
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<title>radios_site</title>
<style>
a {
	text-decoration:none;
}
img {
	display:block;
}
</style>
</head>
<body bgcolor=\"#f4f4f4\">
<center>
<table bgcolor=\"#FFFFFF\" style=\"margin: 0 auto;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"550\">
  <tr>
   <td background=\"$url_site/web/base_email/images/001.png\" width=\"550\" height=\"157\" colspan=\"3\" valign=\"bottom\">
   	<font color=\"#4a4a4a\" face=\"Arial, Helvetica, sans-serif\" style=\"font-size:20px\">
    	<p style=\"margin:0 0 1.5em;padding: 0 20px\">$EmailAssunto</p>
    </font>
   </td>
  </tr>
  <tr>
   <td width=\"20\"><img src=\"$url_site/web/base_email/images/spacer.gif\" style=\"display:block\" /></td>
   <td width=\"510\" valign=\"top\">
   	<font face=\"Arial, Helvetica, sans-serif\" color=\"\" style=\"font-size:13px;\">
    	<p style=\"margin: 0;\">
        	$mensagem_corpo
        </p>
    </font>
   </td>
   <td width=\"20\"><img src=\"$url_site/web/base_email/images/spacer.gif\" style=\"display:block\" /></td>
  </tr>
  <tr>
   <td colspan=\"3\"><img src=\"$url_site/web/base_email/images/$imagem_rede\" usemap=\"#n003Map\" border=\"0\" style=\"display:block; border: 0;\" /></td>
  </tr>
  <tr bgcolor=\"#F4F4F4\">
 	<td width=\"20\" bgcolor=\"#F4F4F4\"><img src=\"$url_site/web/base_email/images/spacer.gif\" style=\"display:block\" /></td>
   <td>
   	<font face=\"Arial, Helvetica, sans-serif\" color=\"#999999\" style=\"font-size: 10px\">
    	<p align=\"center\">
        	Por favor, não responda directamente a este e-mail, pois ele foi enviado a partir de um sistema automático.<br />
Para mais informações utilize o nosso formulário de contato: <strong><a style=\"color:#999999; text-decoration:none;\" href=\"$url_site\" target=\"_blank\">$url_site/</a></strong><br />
<br />
Pense na sua responsabilidade com o ambiente imprima somente se necessário
        </p>
    </font>
   </td>
   <td width=\"20\" bgcolor=\"#F4F4F4\"><img src=\"$url_site/web/base_email/images/spacer.gif\" style=\"display:block\" /></td>
  </tr>
</table>

<map name=\"n003Map\" id=\"n003Map\">
  <area shape=\"rect\" coords=\"479,24,507,46\" href=\"$twitter\" target=\"_blank\" alt=\"Twitter\" />
  <area shape=\"rect\" coords=\"508,24,521,46\" href=\"$facebook\" target=\"_blank\" />
</map>
</body>
</center>
</html>";
?>