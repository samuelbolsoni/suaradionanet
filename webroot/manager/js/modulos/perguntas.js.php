<script type="text/javascript">
//funcoes para o modulo Perguntas
var menu = "";
var submenu = "";
var caminho = "";
var linhas = "";
var busca = "";
var limit = "";
var offset = "";

function perguntasListar(buscar, limit, offset){
	$("#loading").show(); //Loader
	cont = $("#accordion h3").length; //numero de linhas na tabela
	busca = $(".inpBox").val(); //valor de busca, caso exista		
	if(!buscar) { // se a tag 'buscar' nao for ativada, o conteudo recebido sera aclopado as linhas ja existentes
		if(cont < 25) { 
			limit = "";
			offset = "";
		}
		else { 
			limit = cont + 25;
			offset = cont;
		}
	}
	else{ // caso contrario carregara uma nova tabela 
		if(!limit && !offset){ //verifica a existencia de um limit e offset pre-definidos
			limit = "";
			offset = "";
		}		
		$("#todos").attr("checked", ""); //ao carregar uma tabela nova, limpa o check de 'todos'
	}
	$.ajax({
		url: "../perguntas/listar.php",
		type: "post",
		dataType: "html",
		data: "offset="+offset+"&limit="+limit+"&busca="+busca,
		cache: false,
		success: function(dataServer){
			if(buscar) { $("#accordion, .interna_criado").html("");} //se existir a tag buscar, o conteudo de tbody e .interna_criado sera deletado
			var res = dataServer.split("-----SEPARADOR-----"); //organiza a resposta com uma divisao pre-definida no arquivo .php
			$("#accordion").append(res[0]); //adiciona as tabelas a uma ja existente			
			
			if((!isNaN(res[1])) && (res[1] > 1)){ // verifica se a divisao da resposta e um numero e se ele e maior que um
				$(".interna_criado").html(res[1]+" registros encontrados."); //resposta no plural
			}
			else{
				if((!isNaN(res[1])) && (res[1] == 1)){ // verifica se a divisao da resposta e um numero e se ele e igual a um
					$(".interna_criado").html(res[1]+" registro encontrado."); //resposta no singular
				}
			}
			var cont2 = $("#accordion h3").length; //calcula a quantidade de linhas atual			
			if((!isNaN(res[1])) && ((res[1] - cont2) > 0)){ //verifica se ha diferenca do numero total de registro com o numero atual de linhas
				$(".interna_voltar").show(); //caso a diferenca exista e seja positiva, a opcao de mostrar os proximos itens estara ativa			
			}
			else{
				$(".interna_voltar").hide(); //caso contrario sera desativada
			}			
		},
		complete: function(){
			$("#accordion").accordion("destroy");
			$("#accordion").accordion( {
				autoHeight: false
			});
			$("#loading").hide(); // ao completar a funcao o loader sumira
		}
	});
}

function perguntasInserir(id){
	menu = $(".titulo span:eq(0)").html();
	submenu = $(".titulo span:eq(1)").html();
	caminho = $("#caminho").val();
	cont = $(".tbl_lista tbody tr").length;
	busca = $(".inpBox").val();
	if(id){		
		var data = "id="+id+"&limit="+cont+"&busca="+busca+"&menu_post="+menu+"&submenu_post="+submenu+"&caminho_post="+caminho;
	}
	else{
		var data = "limit="+cont+"&busca="+busca+"&menu_post="+menu+"&submenu_post="+submenu+"&caminho_post="+caminho;
	}	
	$.ajax({
		url: "../perguntas/inserir.php",
		type: "post",
		dataType: "html",
		data: data,
		cache: false,
		success: function(dataServer){
			$(".conteudo").html(dataServer);
		},
		complete: function(){
		    /*delete CKEDITOR.instances[ 'editor' ];
			CKEDITOR.replace('editor',
					{
						language: "pt-br",
						skin: "v2",
						width: "618",
						height: "178",
						htmlEncodeOutput: true,
						toolbar:
				            [
								['Source'],
								['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', '-', 'About']
				            ]
					});	*/		
		         
		}
	});
}

function perguntasInsere(){
	$("#loading").show();
	
	//var pergunta = CKEDITOR.instances.editor.getData();
	var pergunta = $("#pergunta").val();
	if(pergunta == ""){
		$(".interna_msg p").text("Pergunta inv&aacute;lida!");
		$(".interna_msg").show();
		$("#loading").hide();
	}
	else{
		var data = "pergunta="+pergunta;
		
		$.ajax({
			url: "../perguntas/insere.php",
			type: "post",
			dataType:"html",
			data: data,
			cache: false,
			success: function(dataServer){
				if(dataServer == 1) {
					Menu(caminho, submenu, menu, limit, 0, busca, 'cadastrado');
					menusAtualiza();
				}
				else{
					$(".interna_msg p").text("Ocorreu um erro, tente novamente!");
					$(".interna_msg").show();
					$("#loading").hide();
				}
			}
		});
	}
}
</script>