<script type="text/javascript">
function logar(){
	$("#botao_logar").hide();
	$("#loading").show();
	
	var login = $("#login").val();
	var senha = $("#senha").val();
	
	if(login == "" || senha == ""){
		if(login == "") {
			$("#loading").hide();
			$("#botao_logar").show();			
			$(".login_msg p").html("Informe o login!");
			$(".login_msg").show();
		}
		else if(senha == ""){
			$("#loading").hide();
			$("#botao_logar").show();
			$(".login_msg p").html("Informe a senha!");
			$(".login_msg").show();
		}
	}
	else{
		$.ajax({
			url: "../login/login_ajax.php",
			type: "post",
			dataType: "html",
			data: "login="+login+"&senha="+senha,
			cache: false,
			success: function(dataServer){
				if(dataServer == 1){
					document.location=("<?=$url_site?>/manager");
				}
				else{
					$("#loading").hide();
					$("#botao_logar").show();
					$(".login_msg p").html(dataServer);
					$(".login_msg").show();
				}
			}		
		});
	}
	
}

function enviar(){
	$("#login_enviar").hide();
	$("#loading2").show();
	
	var email = $("#email").val();
	
	if(email == ""){
		$("#loading2").hide();
		$("#login_enviar").show();
		$(".login_msg p").html("Informe o e-mail!");
		$(".login_msg").show();
	}
	else{
		if(!mailvalido(email)){
			$("#loading2").hide();
			$("#login_enviar").show();
			$(".login_msg p").html("E-mail inv&aacute;lido!");
			$(".login_msg").show();
		}
		else{
			$.ajax({
				url: "../../inc/esqueciSenha.php",
				type: "post",
				dataType: "html",
				data: "email="+email,
				cache: false,
				success: function(dataServer){
					if(dataServer == 1){
						$("#email").val("");
						$("#loading2").hide();
						$("#login_enviar").show();
						$('.senha').fadeOut( 450 , function() {
							$(".login").stop().fadeIn( 450 );			  
						});
						$(".login_msg p").html("Nova senha enviada com sucesso!");
						$(".login_msg").show();
					}
					else{
						if(dataServer == 2){
							$("#loading2").hide();
							$("#login_enviar").show();
							$(".login_msg p").html("O e-mail informado n&atilde;o est&aacute; cadastrado!");
							$(".login_msg").show();
						}
						else{
							$("#loading2").hide();
							$("#login_enviar").show();
							$(".login_msg p").html("Erro ao verificar e-mail!");
							$(".login_msg").show();
						}
					}
				}
			});
		}
	}
}
</script>