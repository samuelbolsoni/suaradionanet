<script type="text/javascript">
//Funções para o submenu mudar_senha
var menu = "";
var submenu = "";
var caminho = "";
var linhas = "";
var busca = "";
var limit = "";
var offset = "";

function mudar_senhaInsere(){	
	$("#loading").show();
	
	var senha_atual = $("#senha_atual").val();
	var nova_senha = $("#nova_senha").val();
	var repita_senha = $("#repita_senha").val();
	
	if(senha_atual == "" || repita_senha == "" || nova_senha == ""){
		$(".interna_msg p").text("Preencha todos os campos para salvar a nova senha.");
		$(".interna_msg").show(500).delay(2500).hide(500);
		$("#loading").hide();
	}
	else{
		if(nova_senha != repita_senha){
			$(".interna_msg p").text("Os campos de nova senha não são iguais!");
			$(".interna_msg").show(500).delay(2500).hide(500);
			$("#loading").hide();
		}
		else{
			var data = "senha_atual="+senha_atual+"&nova_senha="+nova_senha;
			
			$.ajax({
				url: "../mudar_senha/insere.php",
				type: "post",
				dataType: "html",
				data: data,
				cache: false,
				success: function(dataServer){
					if(dataServer == 1){
						 $("input").val("");
						 $(".interna_msg p").html("Sua senha foi alterada com sucesso!");
						 $(".interna_msg").show(500).delay(5000).hide(500);
					}
					else {
						if(dataServer == 3){
							$(".interna_msg p").html("A senha atual não está correta!");
							$(".interna_msg").show(500).delay(5000).hide(500);
						}
						else{
							$(".interna_msg p").html("Erro ao salvar!");
							$(".interna_msg").show(500).delay(5000).hide(500);
						}
					}
				},
				complete: function(){
					$("#loading").hide();
				}
			});
		}
	}
}
</script>