<script type="text/javascript">
//Funções para o submenu permissoes
var menu = "";
var submenu = "";
var caminho = "";
var linhas = "";
var busca = "";
var limit = "";
var offset = "";
//variaveis carregadas em geral.js

function permissoesListar(buscar, limit, offset){
	$("#loading").show(); //Loader
	cont = $(".tbl_lista tbody tr").length; //numero de linhas na tabela
	busca = $(".inpBox").val(); //valor de busca, caso exista		
	if(!buscar) { // se a tag 'buscar' nao for ativada, o conteudo recebido sera aclopado as linhas ja existentes
		if(cont < 25) { 
			limit = "";
			offset = "";
		}
		else { 
			limit = cont + 25;
			offset = cont;
		}
	}
	else{ // caso contrario carregara uma nova tabela 
		if(!limit && !offset){ //verifica a existencia de um limit e offset pre-definidos
			limit = "";
			offset = "";
		}		
		$("#todos").attr("checked", ""); //ao carregar uma tabela nova, limpa o check de 'todos'
	}
	$.ajax({
		url: "../permissoes/listar.php",
		type: "post",
		dataType: "html",
		data: "offset="+offset+"&limit="+limit+"&busca="+busca,
		cache: false,
		success: function(dataServer){
			if(buscar) { $("tbody, .interna_criado").html("");} //se existir a tag buscar, o conteudo de tbody e .interna_criado sera deletado
			var res = dataServer.split("-----SEPARADOR-----"); //organiza a resposta com uma divisao pre-definida no arquivo .php
			$(".conteudo table tbody").append(res[0]); //adiciona as tabelas a uma ja existente			
			
			if((!isNaN(res[1])) && (res[1] > 1)){ // verifica se a divisao da resposta e um numero e se ele e maior que um
				$(".interna_criado").html(res[1]+" registros encontrados."); //resposta no plural
			}
			else{
				if((!isNaN(res[1])) && (res[1] == 1)){ // verifica se a divisao da resposta e um numero e se ele e igual a um
					$(".interna_criado").html(res[1]+" registro encontrado."); //resposta no singular
				}
			}
			var cont2 = $(".tbl_lista tbody tr").length; //calcula a quantidade de linhas atual			
			if((!isNaN(res[1])) && ((res[1] - cont2) > 0)){ //verifica se ha diferenca do numero total de registro com o numero atual de linhas
				$(".interna_voltar").show(); //caso a diferenca exista e seja positiva, a opcao de mostrar os proximos itens estara ativa			
			}
			else{
				$(".interna_voltar").hide(); //caso contrario sera desativada
			}			
		},
		complete: function(){
			$("#loading").hide(); // ao completar a funcao o loader sumira
		}
	});
}

function permissoesInserir(id, select, caminho_p, submenu_p, menu_p, limit_p, busca_p, msg_p){	
	if((menu_p) && (menu_p != "")) menu = menu_p; 	
	else menu = $(".titulo span:eq(0)").html();
	if((submenu_p) && (submenu_p != "")) submenu = submenu_p;
	else submenu = $(".titulo span:eq(1)").html();
	if((caminho_p) && (caminho_p != "")) caminho = caminho_p; 
	else caminho = $("#caminho").val();
	if((limit_p) && (limit_p != "")) cont = limit_p; 
	else cont = $(".tbl_lista tbody tr").length;
	if((busca_p) && (busca_p)) busca = busca_p; 
	else busca = $(".inpBox").val();	
	
	if((select) && (select != "")){
		var id = "";		
		var checks = $("input:checked").length;
		if($("input:checked").length == 0){
			$(".interna_msg p").html("Nenhum registro selecionado!");
			$(".interna_msg").show(500).delay(5000).hide(500);
			return false;
		}
		else{
			var n_id = "";
			var inicio = "";
			$("input:checked").each(function(){
				if($(this).attr("id") != "todos"){
					n_id += inicio + $(this).val();
					inicio = "|";
				}
			});	
		}
		id = n_id; 
	}
	if(id){		
		var data = "id="+id+"&limit="+cont+"&busca="+busca+"&menu_post="+menu+"&submenu_post="+submenu+"&caminho_post="+caminho;
	}
	else{
		var data = "limit="+cont+"&busca="+busca+"&menu_post="+menu+"&submenu_post="+submenu+"&caminho_post="+caminho;
	}	
	$.ajax({
		url: "../permissoes/inserir.php",
		type: "post",
		dataType: "html",
		data: data,
		cache: false,
		success: function(dataServer){
			$(".conteudo").html(dataServer);
		}
	});
}

function permissoesInsere(){
	$("#loading").show();
	
	var data = "";
	var inicio = "";
	var cont_input = 0;
		
	$("input:checked").each(function(){
		if($(this).attr("id") != "todos"){
			data += inicio + "campo_" + cont_input + "=" + $(this).attr("id");
			inicio = "&";
			cont_input++;
		}
	});
		
	var id = $("#id").val();
	if(id != "") data += "&id="+id;
	
	$.ajax({
		url: "../permissoes/insere.php",
		type: "post",
		dataType: "html",
		data: data,
		cache: false,
		success: function(dataServer){
			if(dataServer == 1){
				Menu(caminho, submenu, menu, limit, 0, busca, 'caadstrado');
				menusAtualiza();
			}
			else{
				$(".interna_msg p").html("Erro ao salvar!");
				$(".interna_msg").show(500).delay(5000).hide(500);
				$("#loading").hide();									
			}			
		},
		complete: function(){
			$("#loading").hide();
		}
	});

}

function permissoesAcoes(){
	var acao = $("#acao_lista").val();
	switch(acao){
		case 'editar':
			permissoesInserir('', 1);
			break;
		case 'excluir':
			permissoesExcluir('', 1);
		case 'default':
			break;
	}
}

function permissoesExcluir(id, select){		
	if(select){
		var checks = $("input:checked").length;
		if($("input:checked").length == 0){
			$(".interna_msg p").html("Nenhum registro selecionado!");
			$(".interna_msg").show(500).delay(5000).hide(500);			
		}
		else{
			var n_id = "";
			var inicio = "";
			$("input:checked").each(function(){
				if($(this).attr("id") != "todos"){
					n_id += inicio + $(this).val();
					inicio = "|";
				}
			});			
			$("#modal_id").val(n_id);
			$.colorbox({ inline:true, href:'#localizacao'});			
		}
	}
	else{
		if(id){
			$("#modal_id").val(id);
			$.colorbox({ inline:true, href:'#localizacao'});
		}
		
	}	
}

function permissoesExclui(){
	$("#loading2").show();
	var id = $("#modal_id").val();
	$.ajax({
		url: "../permissoes/exclui.php",
		type: "post",
		dataType: "html",
		data: "id="+id,
		cache: false,
		success: function(dataServer){
			if(dataServer == 1){				
				$.colorbox.close();
				$(".interna_msg p").html("Permiss&otilde;es exclu&iacute;das com sucesso!");
				$(".interna_msg").show(500).delay(5000).hide(500);
			}
			else{			
				$(".msg p").html("Erro ao exluir permiss&otilde;es!");
				$(".msg").show(500).delay(5000).hide(500);
			}
		},
		complete: function(){			
			$("#loading2").hide();						
			var cont = $(".tbl_lista tbody tr").length;
			var limit = cont;
			permissoesListar('buscar', limit, 0);			
		}
	});
}
</script>