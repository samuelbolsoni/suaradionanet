<script type="text/javascript">
//Funções para o submenu Usuarios
var menu = "";
var submenu = "";
var caminho = "";
var linhas = "";
var busca = "";
var limit = "";
var offset = "";

function usuariosListar(buscar, limit, offset){
	$("#loading").show(); //Loader
	cont = $(".tbl_lista tbody tr").length; //numero de linhas na tabela
	busca = $(".inpBox").val(); //valor de busca, caso exista		
	if(!buscar) { // se a tag 'buscar' nao for ativada, o conteudo recebido sera aclopado as linhas ja existentes
		if(cont < 25) { 
			limit = "";
			offset = "";
		}
		else { 
			limit = cont + 25;
			offset = cont;
		}
	}
	else{ // caso contrario carregara uma nova tabela 
		if(!limit && !offset){ //verifica a existencia de um limit e offset pre-definidos
			limit = "";
			offset = "";
		}		
		$("#todos").attr("checked", ""); //ao carregar uma tabela nova, limpa o check de 'todos'
	}
	$.ajax({
		url: "../usuarios/listar.php",
		type: "post",
		dataType: "html",
		data: "offset="+offset+"&limit="+limit+"&busca="+busca,
		cache: false,
		success: function(dataServer){
			if(buscar) { $("tbody, .interna_criado").html("");} //se existir a tag buscar, o conteudo de tbody e .interna_criado sera deletado
			var res = dataServer.split("-----SEPARADOR-----"); //organiza a resposta com uma divisao pre-definida no arquivo .php
			$(".conteudo table tbody").append(res[0]); //adiciona as tabelas a uma ja existente			
			
			if((!isNaN(res[1])) && (res[1] > 1)){ // verifica se a divisao da resposta e um numero e se ele e maior que um
				$(".interna_criado").html(res[1]+" registros encontrados."); //resposta no plural
			}
			else{
				if((!isNaN(res[1])) && (res[1] == 1)){ // verifica se a divisao da resposta e um numero e se ele e igual a um
					$(".interna_criado").html(res[1]+" registro encontrado."); //resposta no singular
				}
			}
			var cont2 = $(".tbl_lista tbody tr").length; //calcula a quantidade de linhas atual			
			if((!isNaN(res[1])) && ((res[1] - cont2) > 0)){ //verifica se ha diferenca do numero total de registro com o numero atual de linhas
				$(".interna_voltar").show(); //caso a diferenca exista e seja positiva, a opcao de mostrar os proximos itens estara ativa			
			}
			else{
				$(".interna_voltar").hide(); //caso contrario sera desativada
			}			
		},
		complete: function(){
			$("#loading").hide(); // ao completar a funcao o loader sumira
		}
	});
}

function usuariosInserir(id){
	menu = $(".titulo span:eq(0)").html();
	submenu = $(".titulo span:eq(1)").html();
	caminho = $("#caminho").val();
	cont = $(".tbl_lista tbody tr").length;
	busca = $(".inpBox").val();
	if(id){		
		var data = "id="+id+"&limit="+cont+"&busca="+busca+"&menu_post="+menu+"&submenu_post="+submenu+"&caminho_post="+caminho;
	}
	else{
		var data = "limit="+cont+"&busca="+busca+"&menu_post="+menu+"&submenu_post="+submenu+"&caminho_post="+caminho;
	}	
	$.ajax({
		url: "../usuarios/inserir.php",
		type: "post",
		dataType: "html",
		data: data,
		cache: false,
		success: function(dataServer){
			$(".conteudo").html(dataServer);
		},
		complete: function(){
			$("#tipo_usuario").msDropDown({mainCSS:'select_3'});
			$("#id_instrutor").msDropDown({mainCSS:'select_3'});
		}
	});
}

function usuariosInsere(){	
	$("#loading").show();
	var cont_input = 0;	
	var id = $("#id").val();
	
	if((id) && (id != "")){
		$("input[type=text]").each(function(){		
			if($(this).val() != "") {
				cont_input++;
			}
		});		
	}
	else{
		$("input[type=text]").each(function(){		
			if($(this).val() != "") {
				cont_input++;
			}
		});
		$("input[type=password]").each(function(){
			if($(this).val() == ""){
				cont_input = 0;
			}
		});
	}
			
	if(cont_input == 0){
		$(".interna_msg p").html("Preencha os campos corretamente!");
		$(".interna_msg").show(500).delay(5000).hide(500);
		$("#loading").hide();
	}
	else{	
		var email = $("#email").val();
		var senha = $("#senha").val();
		var senha_confirma = $("#senha_confirma").val();
		if((!mailvalido(email)) || (senha != senha_confirma)){
			if(!mailvalido(email)){				
				$(".interna_msg p").html("E-mail inv&aacute;lido!");
				$(".interna_msg").show(500).delay(5000).hide(500);
				$("#loading").hide();
			}
			else if(senha != senha_confirma){
				$(".interna_msg p").html("Os campos de senha n&atilde;o s&atilde;o iguais!");
				$(".interna_msg").show(500).delay(5000).hide(500);
				$("#loading").hide();
			}
		}
		else{			
			var inicio = "";
			var data = "";
			$("input[type=text]").each(function(){			
				data += inicio + $(this).attr('id') + "=" + $(this).val();
				inicio = "&";				
			});		
			
			var senha = $("#senha").val();
			var senha_confirma = $("#senha_confirma").val();
			
			if(senha != ""){
				data += "&senha=" + senha + "&senha_confirma=" + senha_confirma;  
			}
			
			var tipo_usuario = $("#tipo_usuario").val();
			var id_instrutor = $("#id_instrutor").val();
			var configurar_check = 0;
			if($("#configurar_check").attr("checked")){
				configurar_check = 1; 
			}
			data += "&id_instrutor="+id_instrutor;
			data += "&tipo_usuario="+tipo_usuario;
						
			if(id != "") data += "&id="+id;
			
			$.ajax({
				url: "../usuarios/insere.php",
				type: "post",
				dataType: "html",
				data: data,
				cache: false,
				success: function(dataServer){
					var teste = dataServer.split("-");
					if(teste[0] == 1){						
						if(configurar_check == 1) permissoesInserir(teste[1], '', caminho, submenu, menu, limit, busca, 'cadastrado');
						else Menu(caminho, submenu, menu, limit, 0, busca, 'cadastrado');						
					}
					else{
						if(dataServer == 2){
							if(configurar_check == 1) permissoesInserir(id, '', caminho, submenu, menu, limit, busca, 'cadastrado');
							else Menu(caminho, submenu, menu, limit, 0, busca, 'atualizado');							
						}
						else{
							var retorna = dataServer;
							retorna = retorna.split("|");
							if(retorna[0] == 3){
								$(".interna_msg p").html(retorna[1] + " usu&aacute;rio j&aacute; existe!");
								$(".interna_msg").show(500).delay(5000).hide(500);
								for(var i=2; i<retorna.length; i++){
									$("#"+retorna[i]).css("color", "red").click(function(){
										$(this).css("color", "#242B33");
									});
								}
								$("#loading").hide();
							}						
							else{
								$(".interna_msg p").html("Erro ao salvar!");
								$(".interna_msg").show(500).delay(5000).hide(500);
								$("#loading").hide();
							}
						}
					}			
				},
				complete: function(){
					$("#loading").hide();
				}
			});
		}
	}
}

function usuariosAcoes(){
	var acao = $("#acao_lista").val();
	switch(acao){
		case 'excluir':
			usuariosExcluir('', 1);
			break;
		case 'default':
			break;
	}
}

function usuariosExcluir(id, select){		
	if(select){
		var checks = $("input:checked").length;
		if($("input:checked").length == 0){
			$(".interna_msg p").html("Nenhum registro selecionado!");
			$(".interna_msg").show(500).delay(5000).hide(500);			
		}
		else{
			var n_id = "";
			var inicio = "";
			$("input:checked").each(function(){
				if($(this).attr("id") != "todos"){
					n_id += inicio + $(this).val();
					inicio = "|";
				}
			});			
			$("#modal_id").val(n_id);
			$.colorbox({ inline:true, href:'#localizacao'});			
		}
	}
	else{
		if(id){
			$("#modal_id").val(id);
			$.colorbox({ inline:true, href:'#localizacao'});
		}
		
	}	
}

function usuariosExclui(){
	$("#loading2").show();
	var id = $("#modal_id").val();
	$.ajax({
		url: "../usuarios/exclui.php",
		type: "post",
		dataType: "html",
		data: "id="+id,
		cache: false,
		success: function(dataServer){
			if(dataServer == 1){				
				$.colorbox.close();
				$(".interna_msg p").html("Registro(s) exclu&iacute;do(s) com sucesso!");
				$(".interna_msg").show(500).delay(5000).hide(500);
			}
			else{			
				$(".msg p").html("Erro ao exluir registro(s)!");
				$(".msg").show(500).delay(5000).hide(500);
			}
		},
		complete: function(){			
			$("#loading2").hide();			
			var n_id = id.split("|");
			n_id = n_id.length;
			var cont = $(".tbl_lista tbody tr").length;
			var limit = cont - n_id;
			usuariosListar('buscar', limit, 0);			
		}
	});
}
</script>