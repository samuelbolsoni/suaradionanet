//Funções para o submenu duvidas
var menu = "";
var submenu = "";
var caminho = "";
var linhas = "";
var busca = "";
var limit = "";
var offset = "";

function duvidasListar(buscar, limit, offset){	
	$("#loading").show(); //Loader
	cont = $(".tbl_lista tbody tr").length; //numero de linhas na tabela
	busca = $(".inpBox").val(); //valor de busca, caso exista		
	if(!buscar) { // se a tag 'buscar' nao for ativada, o conteudo recebido sera aclopado as linhas ja existentes
		if(cont < 6) { 
			limit = "";
			offset = "";
		}
		else { 
			limit = cont + 6;
			offset = cont;
		}
	}
	else{ // caso contrario carregara uma nova tabela 
		if(!limit && !offset){ //verifica a existencia de um limit e offset pre-definidos
			limit = "";
			offset = "";
		}		
		$("#todos").attr("checked", ""); //ao carregar uma tabela nova, limpa o check de 'todos'
	}
	$.ajax({
		url: "../duvidas/listar.php",
		type: "post",
		dataType: "html",
		data: "offset="+offset+"&limit="+limit+"&busca="+busca,
		cache: false,
		success: function(dataServer){
			if(buscar) { $("tbody, .interna_criado").html("");} //se existir a tag buscar, o conteudo de tbody e .interna_criado sera deletado
			var res = dataServer.split("-----SEPARADOR-----"); //organiza a resposta com uma divisao pre-definida no arquivo .php
			$(".conteudo table tbody").append(res[0]); //adiciona as tabelas a uma ja existente			
			
			if((!isNaN(res[1])) && (res[1] > 1)){ // verifica se a divisao da resposta e um numero e se ele e maior que um
				$(".interna_criado").html(res[1]+" registros encontrados."); //resposta no plural
			}
			else{
				if((!isNaN(res[1])) && (res[1] == 1)){ // verifica se a divisao da resposta e um numero e se ele e igual a um
					$(".interna_criado").html(res[1]+" registro encontrado."); //resposta no singular
				}
			}
			var cont2 = $(".tbl_lista tbody tr").length; //calcula a quantidade de linhas atual			
			if((!isNaN(res[1])) && ((res[1] - cont2) > 0)){ //verifica se ha diferenca do numero total de registro com o numero atual de linhas
				$(".interna_voltar").show(); //caso a diferenca exista e seja positiva, a opcao de mostrar os proximos itens estara ativa			
			}
			else{
				$(".interna_voltar").hide(); //caso contrario sera desativada
			}			
		},
		complete: function(){
			$("#loading").hide(); // ao completar a funcao o loader sumira
		}
	});
}

function duvidasInserir(id){	
	menu = $(".titulo span:eq(0)").html();
	submenu = $(".titulo span:eq(1)").html();
	caminho = $("#caminho").val();
	cont = $(".tbl_lista tbody tr").length;
	busca = $(".inpBox").val();
	if(id){		
		var data = "id="+id+"&limit="+cont+"&busca="+busca+"&menu_post="+menu+"&submenu_post="+submenu+"&caminho_post="+caminho;
	}
	else{
		var data = "limit="+cont+"&busca="+busca+"&menu_post="+menu+"&submenu_post="+submenu+"&caminho_post="+caminho;
	}	
	$.ajax({
		url: "../duvidas/inserir.php",
		type: "post",
		dataType: "html",
		data: data,
		cache: false,
		success: function(dataServer){
			$(".conteudo").html(dataServer);
		},
		complete: function(){
			$("#resposta").tinymce({
				script_url : 'http://localhost/catalogopoa/web/webroot/manager/inc/tiny_mce/tiny_mce.js',
			    theme : "advanced",
			    language: "pt",
			    plugins: "paste",
			    entity_encoding : "raw",
			    paste_auto_cleanup_on_paste : true,
			    theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,|,fontselect,fontsizeselect,cut,copy,paste,|,bullist,numlist,|,undo,redo,|,link,unlink,|,code,",
			    theme_advanced_buttons2 : "forecolor,backcolor,|,removeformat,|,charmap,",
			    theme_advanced_buttons3 : "",
		        theme_advanced_toolbar_location : "top",
		        theme_advanced_toolbar_align : "left",
		        theme_advanced_statusbar_location : "bottom",
		        theme_advanced_resizing : true,
		        force_br_newlines : true,
		        force_p_newlines : false,
		        forced_root_block : "",
		        relative_urls: false,
		        convert_urls : false,
		        convert_newlines_to_brs : true,
		        width: "618",
		        height: "245",
		        skin: "o2k7",
		        skin_variant : "silver"
			});
			$("select#publicar").msDropDown({mainCSS:'select_3'});
			menusAtualiza();
		}
	});
}

function duvidasInsere(){	
	$("#loading").show();
	
	var str = ""; var inicio = "";
	$("#form select, #form input[type=text]").each(function(){
		str+= inicio + $(this).attr("id") + "=" + $(this).val();
		inicio = "&";
	});
	
	var pergunta = $("#pergunta").val();

	var resposta = $("#resposta").tinymce().getContent();
	resposta = url_encode(resposta);

	str+= inicio + "resposta=" + resposta + inicio + "pergunta=" + pergunta;
	
	var id = $("#id").val();

	var data = str;

	if(id != "") data += "&id="+id;

	$.ajax({
		url: "../duvidas/insere.php",
		type: "post",
		dataType: "html",
		data: data,
		cache: false,
		success: function(dataServer){
			if(dataServer == 1){
				 Menu(caminho, submenu, menu, limit, 0, busca, 'cadastrado');
			}
			else {
				if(dataServer == 2){
					Menu(caminho, submenu, menu, limit, 0, busca, 'atualizado');
				}
				else {
					if(dataServer == 3){
						$(".interna_msg p").html("Essa cidade já existe!");
						$(".interna_msg").show(500).delay(5000).hide(500);
					}
					else{
						$(".interna_msg p").html("Erro ao salvar!");
						$(".interna_msg").show(500).delay(5000).hide(500);
					}
				}
			}
		},
		complete: function(){
			$("#loading").hide();
			menusAtualiza();
		}
	});
}

function duvidasAcoes(){
	var acao = $("#acao_lista").val();
	switch(acao){
		case 'excluir':
			duvidasExcluir('', 1);
			break;		
		case 'default':
			break;
	}
}

function duvidasExcluir(id, select){		
	if(select){
		var checks = $("input:checked").length;
		if($("input:checked").length == 0){
			$(".interna_msg p").html("Nenhum registro selecionado!");
			$(".interna_msg").show(500).delay(5000).hide(500);			
		}
		else{
			var n_id = "";
			var inicio = "";
			$("input:checked").each(function(){
				if($(this).attr("id") != "todos"){
					n_id += inicio + $(this).val();
					inicio = "|";
				}
			});
			$("#modal_id").val(n_id);
			$.colorbox({ inline:true, href:'#localizacao'});
		}
	}
	else{
		if(id){
			$("#modal_id").val(id);
			$.colorbox({ inline:true, href:'#localizacao', transition: "fade"});
		}
	}
}

function duvidasExclui(){
	$("#loading2").show();
	var id = $("#modal_id").val();
	$.ajax({
		url: "../duvidas/exclui.php",
		type: "post",
		dataType: "html",
		data: "id="+id,
		cache: false,
		success: function(dataServer){
			if(dataServer == 1){
				$.colorbox.close();
				$(".interna_msg p").html("Registro(s) exclu&iacute;do(s) com sucesso!");
				$(".interna_msg").show(500).delay(5000).hide(500);
			}
			else{
				$(".msg p").html("Erro ao exluir registro(s)!");
				$(".msg").show(500).delay(5000).hide(500);
			}
		},
		complete: function(){
			$("#loading2").hide();
			var n_id = id.split("|");
			n_id = n_id.length;
			var cont = $(".tbl_lista tbody tr").length;
			var limit = cont - n_id;
			duvidasListar('buscar', limit, 0);
			menusAtualiza();
		}
	});
}