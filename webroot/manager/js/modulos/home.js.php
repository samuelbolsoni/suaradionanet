<script type="text/javascript">
//Funções para o submenu home
var menu = "";
var submenu = "";
var caminho = "";
var linhas = "";
var busca = "";
var limit = "";
var offset = "";

function homeListar(buscar, limit, offset){	
	$("#loading").show(); //Loader
	cont = $(".tbl_lista tbody tr").length; //numero de linhas na tabela
	busca = $(".inpBox").val(); //valor de busca, caso exista
	if(!buscar) { //se a tag 'buscar' nao for ativada, o conteudo recebido sera aclopado as linhas ja existentes
		if(cont < 25) { 
			limit = "";
			offset = "";
		}
		else { 
			limit = cont + 25;
			offset = cont;
		}
	}
	else{ // caso contrario carregara uma nova tabela 
		if(!limit && !offset){ //verifica a existencia de um limit e offset pre-definidos
			limit = "";
			offset = "";
		}		
		$("#todos").attr("checked", ""); //ao carregar uma tabela nova, limpa o check de 'todos'
	}
	$.ajax({
		url: "../home_site/listar.php",
		type: "post",
		dataType: "html",
		data: "offset="+offset+"&limit="+limit+"&busca="+busca,
		cache: false,
		success: function(dataServer){
			if(buscar) { $("tbody, .interna_criado").html("");} //se existir a tag buscar, o conteudo de tbody e .interna_criado sera deletado
			var res = dataServer.split("-----SEPARADOR-----"); //organiza a resposta com uma divisao pre-definida no arquivo .php
			$(".conteudo table tbody").append(res[0]); //adiciona as tabelas a uma ja existente			
			
			if((!isNaN(res[1])) && (res[1] > 1)){ // verifica se a divisao da resposta e um numero e se ele e maior que um
				$(".interna_criado").html(res[1]+" registros encontrados."); //resposta no plural
			}
			else{
				if((!isNaN(res[1])) && (res[1] == 1)){ // verifica se a divisao da resposta e um numero e se ele e igual a um
					$(".interna_criado").html(res[1]+" registro encontrado."); //resposta no singular
				}
			}
			var cont2 = $(".tbl_lista tbody tr").length; //calcula a quantidade de linhas atual			
			if((!isNaN(res[1])) && ((res[1] - cont2) > 0)){ //verifica se ha diferenca do numero total de registro com o numero atual de linhas
				$(".interna_voltar").show(); //caso a diferenca exista e seja positiva, a opcao de mostrar os proximos itens estara ativa			
			}
			else{
				$(".interna_voltar").hide(); //caso contrario sera desativada
			}			
		},
		complete: function(){
			$("#loading").hide(); // ao completar a funcao o loader sumira
		}
	});
}

function homeInserir(id){
	menu = $(".titulo span:eq(0)").html();
	submenu = $(".titulo span:eq(1)").html();
	caminho = $("#caminho").val();
	cont = $(".tbl_lista tbody tr").length;
	busca = $(".inpBox").val();
	if(id){
		var data = "id="+id+"&limit="+cont+"&busca="+busca+"&menu_post="+menu+"&submenu_post="+submenu+"&caminho_post="+caminho;
	}
	else{
		var data = "limit="+cont+"&busca="+busca+"&menu_post="+menu+"&submenu_post="+submenu+"&caminho_post="+caminho;
	}
	$.ajax({
		url: "../home_site/inserir.php",
		type: "post",
		dataType: "html",
		data: data,
		cache: false,
		success: function(dataServer){
			$(".conteudo").html(dataServer);
		},
		complete: function(){
			$("select#garota").msDropDown({mainCSS:'select_3'});
			$(".conceitual_imagens").sortable({
				update: function(){
			        var data = ""; var inicio = ""; var sep="";
			        $(".imagem").each(function(){
			            sep = $(this).attr("id");
			            sep = sep.split("_");sep = sep[1];
			        	data+= inicio + sep;
			            inicio = "|";
			        });
			        $("#imagem").val(data);
				}
			}).disableSelection();
			verticalSliding();
			$("#texto").tinymce({
				script_url : '<?=$url_site?>/web/webroot/manager/inc/tiny_mce/tiny_mce.js',
			    theme : "advanced",
			    language: "pt",
			    plugins: "paste",
			    entity_encoding : "raw",
			    paste_auto_cleanup_on_paste : true,
			    theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,|,fontselect,fontsizeselect,cut,copy,paste,|,bullist,numlist,|,undo,redo,|,link,unlink,|,code,",
			    theme_advanced_buttons2 : "forecolor,backcolor,|,removeformat,|,charmap,",
			    theme_advanced_buttons3 : "",
		        theme_advanced_toolbar_location : "top",
		        theme_advanced_toolbar_align : "left",
		        theme_advanced_statusbar_location : "bottom",
		        theme_advanced_resizing : true,
		        force_br_newlines : true,
		        force_p_newlines : false,
		        forced_root_block : "",
		        width: "618",
		        height: "245",
		        skin: "o2k7",
		        skin_variant : "silver"
			});
		}
	});
}

function homeInsere(){
	$("#loading").show();
	
	var data = "";
	var inicio = "";
	var cont_ = 0;
	
	$("#form input[type=text]").each(function(){
		if($(this).val() == ""){
			cont_++;
		}
	});

	if(cont_ > 0){
		$(".interna_msg p").text("Preencha todos os campos para salvar a postagem.");
		$(".interna_msg").show(500).delay(2500).hide(500);
		$("#loading").hide();
	}
	else{

		$("#form input[type=text], #form select, #form input[type=radio]").each(function(){
			if($(this).attr("id") == "cadastro" && $(this).attr("checked")){
				data+= inicio + $(this).attr("id") + "=" + $(this).val();
				inicio = "&";
			}
			if($(this).attr("id") != "cadastro"){
				data+= inicio + $(this).attr("id") + "=" + $(this).val();
				inicio = "&";
			}
				
		});

		var imagem = $("#imagem").val();
		data += "&imagem="+imagem;

		var id = $("#id").val();

		if(id != "") data += "&id="+id;

		$.ajax({
			url: "../home_site/insere.php",
			type: "post",
			dataType: "html",
			data: data,
			cache: false,
			success: function(dataServer){
				if(dataServer == 1){
					 Menu(caminho, submenu, menu, limit, 0, busca, 'cadastrado');
				}
				else {
					if(dataServer == 2){
						Menu(caminho, submenu, menu, limit, 0, busca, 'atualizado');
					}
					else {
						var teste = dataServer.split("|");
						if(teste[0] == 3){
							$(".interna_msg p").html("Produto já cadastrado!");
							$(".interna_msg").show(500).delay(5000).hide(500);
							$("#titulo").css("color", "red").focus().click(function(){
								$(this).css("color", "#242B33");
							});
						}
						else{
							$(".interna_msg p").html("Erro ao salvar!");
							$(".interna_msg").show(500).delay(5000).hide(500);
						}
					}
				}
			},
			complete: function(){
				$("#loading").hide();
			}
		});
	}
}

function homeAcoes(id){
	if(id && id != ""){
		var acao = id;
	}
	else{
		var acao = $("#acao_lista").val();
	}
	switch(acao){
		case 'excluir':
			homeExcluir('', 1);
			break;
		case 'default':
			break;
	}
}

function homeExcluir(id, select){		
	if(select){
		var checks = $("input:checked").length;
		if($("input:checked").length == 0){
			$(".interna_msg p").html("Nenhum registro selecionado!");
			$(".interna_msg").show(500).delay(5000).hide(500);			
		}
		else{
			var n_id = "";
			var inicio = "";
			$("input:checked").each(function(){
				if($(this).attr("id") != "todos"){
					n_id += inicio + $(this).val();
					inicio = "|";
				}
			});			
			$("#modal_id").val(n_id);
			$.colorbox({ inline:true, href:'#localizacao'});			
		}
	}
	else{
		if(id){
			$("#modal_id").val(id);
			$.colorbox({ inline:true, href:'#localizacao', transition: "fade"});
		}
		
	}	
}

function homeExclui(){
	$("#loading2").show();
	var id = $("#modal_id").val();
	$.ajax({
		url: "../home_site/exclui.php",
		type: "post",
		dataType: "html",
		data: "id="+id,
		cache: false,
		success: function(dataServer){
			if(dataServer == 1){				
				$.colorbox.close();
				$(".interna_msg p").html("Registro(s) exclu&iacute;do(s) com sucesso!");
				$(".interna_msg").show(500).delay(5000).hide(500);
			}
			else{			
				$(".msg p").html("Erro ao exluir registro(s)!");
				$(".msg").show(500).delay(5000).hide(500);
			}
		},
		complete: function(){			
			$("#loading2").hide();			
			var n_id = id.split("|");
			n_id = n_id.length;
			var cont = $(".tbl_lista tbody tr").length;
			var limit = cont - n_id;
			homeListar('buscar', limit, 0);			
		}
	});
}


//Funcoes para SWFUpload

//Funcoes para SWFUpload
function startBox(){
	$("#getFile").val("");
	$("#title_imagem").val("");
	$("#progressbar").progressbar("destroy");
	$.colorbox.remove();
	$.colorbox.init();
	$.colorbox({
		inline: true,
		href: '#localizacao', 
		minHeigth: 432, 
		minWidth: 790, 
		scrolling: false		
	});
}

function dialog_start_home(){	
	var files = swfu.getStats().files_queued;
	$("#title_imagem").val("");
	$("#getFile").val("");
	if(files > 0){
		swfu.cancelQueue();
	}
}

function dialog_complete_home(number, queued, total){	
	var files = swfu.getStats().files_queued;	
	var qerrors = swfu.getStats().queue_errors;
	var uploads = swfu.getStats().successful_uploads;
	var cancelled = swfu.getStats().upload_cancelled;
	var uerrors = swfu.getStats().upload_errors;

	var todos = files + qerrors + uploads + cancelled + uerrors;
	//swfu.getFile(todos-1).name;
	if(files > 0){
		var iN = "";
		var nam = "";
		for(var i = 0; i < files; i++){
			nam = $("#getFile").val();
			$("#getFile").val(nam + iN + swfu.getFile(i).name);
			iN = "; ";
		}		
		//var file = swfu.getFile(todos-1).name;
		//$("#getFile").val(file);
	}
	//upload_start_cli();
	
}

function file_queue_error_home(file_object, error_code, message){
	//alert(error_code);
	switch(error_code){		
		case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
			//$("#msg_upload").text("Escolha um arquivo de no máximo 1 MB!").show(500).delay(2500).hide(500);
			alert("Escolha um arquivo de no máximo 1 MB!");
			break;
	}
}

function upload_start_home(){	
	var files = swfu.getStats().files_queued;
	var qerrors = swfu.getStats().queue_errors;
	var uploads = swfu.getStats().successful_uploads;
	var cancelled = swfu.getStats().upload_cancelled;
	var uerrors = swfu.getStats().upload_errors;

	var todos = files + qerrors + uploads + cancelled  + uerrors;
	//swfu.getFile(todos-1).name;
	if(files > 0){		
		//var file = swfu.getFile(todos-1).id;
		var title_imagem = $("#title_imagem").val();
		swfu.addPostParam("title", title_imagem);
		//swfu.startUpload(file);		
		swfu.startUpload();
		if(files >= 1){
			if($("#progressbar").html() == ""){
				var n_files = 100 / files;
				$("#progressbar").progressbar({
					value: n_files
				});
			}
		}
		$("#fazerUpload").hide();
		$("#loading2").show();
	}	
}

function upload_success_home(file_object, server_data, received_response){
	if(server_data != "") {		
		if(server_data == 0){			
			$(".msg p").text("Imagem pequena demais!");
			$(".msg").show(500).delay(2500).hide(500);
			var files = swfu.getStats().files_queued;	
			var qerrors = swfu.getStats().queue_errors;
			var uploads = swfu.getStats().successful_uploads;
			var cancelled = swfu.getStats().upload_cancelled;
			var uerrors = swfu.getStats().upload_errors;

			var todos = files + qerrors + uploads + cancelled  + uerrors;
			var file = swfu.getFile(todos-1).id;
			swfu.cancelUpload(file);
		}
		else{			
			
				var files = swfu.getStats().files_queued;
				if(files >= 0){					
					var n_progress = $("#progressbar").progressbar("value");
					
					var files = swfu.getStats().files_queued;	
					var qerrors = swfu.getStats().queue_errors;
					var uploads = swfu.getStats().successful_uploads;
					var cancelled = swfu.getStats().upload_cancelled;
					var uerrors = swfu.getStats().upload_errors;

					var todos = files + qerrors + uploads + cancelled  + uerrors;
					var n_files = 100 / todos;
					n_progress = n_progress + n_files;
					$("#progressbar").progressbar("value", n_progress);
                    
                    var fotos = $("#imagem").val();
                    var foto = server_data.split("_");
                    foto = foto[0];
                    if(fotos == "" || fotos == 0){
                        fotos = foto;
                    }
                    else{
                        fotos+= "|" + foto;
                    }
                    $("#imagem").val(fotos);
                    var id_reg = $("#id").val();
                    $(".conceitual_imagens").prepend("<div id=\"img_" + foto + "\" class='imagem' id='" + foto + "' style='width: 122px; height: 92px'>"+
														"<img src='../../../uploads/img/home_site/thumb_"+ server_data +"?rel=" + Math.random() + "' width='120' height='90' />"+
														"<div class='links' style='width: 122px'>"+
															"<p class='excluir' style='width: 122px'><a style='padding: 2px 25px 0' href='javascript:;' onclick='homeFotoExcluir("+ foto +", \"" + id_reg + "\");'>Excluir</a></p>"+
														"</div>"+		
													"</div>");
					$("div.imagem").css("background", "url('')");
					$(".links").hide();		
					$('.conceitual_imagens .imagem').hover(function(){
						$(".links", this).stop().animate({height:'20px'},{queue:false,duration:300});
					}, function() {
						$(".links", this).stop().animate({height:'0px'},{queue:false,duration:300});
					});
				}
				
														
		}
	}	
	$("#loading2").hide();
	$("#fazerUpload").show();
	//alert(server_data);
}

function upload_complete_home(file_object){
	var files = swfu.getStats().files_queued;
	if(files == 0){
		$.colorbox.close();
	}
}

function queueComplete_home(numFilesUploaded) {	
	//$.colorbox.close();
	//atualizaThumbs();
}

function upload_error_home(file, error, message){
	alert(error);
	alert(message);
	$("#loading2").hide();
	$("#enviar").show();
}
// END Funcoes para SWFUpload

function homeFotoExcluir(id, reg){
	if(!reg) var reg = "";
	$("div#img_"+id+" .links").html("<p style='width: 61px'><a style='padding:2px 15px 0' href='javascript:;' onclick='homeFotoExclui(" + id + ", \"" + reg + "\");'>Sim</a></p>" +
					 "<p style='width: 51px'><a style='padding:2px 15px 0' href='javascript:;' onclick='homeFotoNao(" + id + ", \"" + reg + "\");'>N&atilde;o</a></p>");	
}

function homeFotoNao(id, reg){
	if(!reg) var reg = "";
	$("div#img_"+id+" .links").html("<p style='width: 122px'><a style='padding: 2px 25px 0' href='javascript:;' onclick='homeFotoExcluir("+ id + ", \"" + reg + "\");'>Excluir</a></p>");
}

function homeFotoExclui(id, reg){
	if(!reg) { var reg = ""; }
	if((id) && (id != "")){
		$.ajax({
			url: "../home_site/fotoExclui.php",
			type: "post",
			dataType: "html",
			data: "id="+id+"&reg="+reg,
			cache: false,
			success: function(dataServer){
				if(dataServer == 1){
					$(".conceitual_imagens div#img_"+id).remove();
				}
			},
			complete: function(){
				var img = $("#imagem").val();
				img = img.split("|");				 	
				var string = "";
				var inicio = "";
				for(var i = 0; i < img.length; i++){
					if(img[i] != id){
						string+= inicio + img[i];
						inicio = "|";
					}
				}
				$("#imagem").val(string);
			}
		});
	}
}
</script>