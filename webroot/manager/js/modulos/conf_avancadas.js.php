<script type="text/javascript">
//funcoes para SWFUpload e conf. avancadas
var menu = "";
var submenu = "";
var caminho = "";

function dialog_complete_ca(number, queued, total){	
	var files = swfu.getStats().files_queued;	
	var qerrors = swfu.getStats().queue_errors;
	var uploads = swfu.getStats().successful_uploads;
	var cancelled = swfu.getStats().upload_cancelled;
	var uerrors = swfu.getStats().upload_errors;

	var todos = files + qerrors + uploads + cancelled + uerrors;
	//swfu.getFile(todos-1).name;
	if(files > 0){
		var file = swfu.getFile(todos-1).name;
		$("#getFile").val(file);
	}
}

function file_queue_error_ca(file_object, error_code, message){
	//alert(error_code);
	switch(error_code){		
		case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
			$("#msg_upload").text("Escolha um arquivo de no máximo 1 MB!").show(500).delay(2500).hide(500);
			break;
	}
}

function upload_start_ca(){	
	var files = swfu.getStats().files_queued;	
	var qerrors = swfu.getStats().queue_errors;
	var uploads = swfu.getStats().successful_uploads;
	var cancelled = swfu.getStats().upload_cancelled;
	var uerrors = swfu.getStats().upload_errors;

	var todos = files + qerrors + uploads + cancelled  + uerrors;
	//swfu.getFile(todos-1).name;
	if(files > 0){		
		var file = swfu.getFile(todos-1).id;
		swfu.startUpload(file);		
		$("#loading").show();
		$("#enviar").hide();
	}	
}

function upload_success_ca(file_object, server_data, received_response){	
	if(server_data != "") {
		$(".box_logos img:eq(0)").remove();
		$(".box_logos").prepend("<img src='../../../uploads/img/logo/"+server_data+"' />");
	}
	$("#loading").hide();
	$("#enviar").show();	
}

function upload_error_ca(file, error, message){
	alert(error);
	alert(message);
	$("#loading").hide();
	$("#enviar").show();
}

function conf_avancadasInsere(){
	$("#loading2").show();
	
	var data = "";
	var inicio = "";

	$("#form input[type=text]").each(function(){
		if($(this).attr("id") == "url_site" || $(this).attr("id") == "email_contato" || $(this).attr("id") == "email_suporte"){
			if($(this).val() == ""){				
				$(".interna_msg p").text("Url do Site, E-mail para contato e E-mail para Suporte são obrigatórios!");
				$(".interna_msg").show(500).delay(2500).hide(500);
				$("#loading2").hide();
				return false;
			}
		}
		data += inicio + $(this).attr("id") + "=" + $(this).val();
		inicio = "&";
	});
	
	data += "&" + "status_site=" + $("#tipo_usuario").val();	
	
	$.ajax({
		url: "../conf_avancadas/insere.php",
		type: "post",
		dataType: "html",
		data: data,
		cache: false,
		success: function(dataServer){
			if(dataServer == 1){
				$("#loading2").hide();
				$(".interna_msg p").text("Dados salvos com sucesso!");
				$(".interna_msg").show(500).delay(2500).hide(500);				
			}
			else{
				$("#loading2").hide();
				$(".interna_msg p").text("Um erro inesperado ocorreu!");
				$(".interna_msg").show(500).delay(2500).hide(500);
			}
		}
	});
}

function conf_avancadasInserirHome(id){
	menu = $(".titulo span:eq(0)").html();
	submenu = $(".titulo span:eq(1)").html();
	caminho = $("#caminho").val();
	if((id) && (id != "")){		
		var data = "id="+id+"&menu_post="+menu+"&submenu_post="+submenu+"&caminho_post="+caminho;
	}
	else{
		var data = "menu_post="+menu+"&submenu_post="+submenu+"&caminho_post="+caminho;
	}	
	$.ajax({
		url: "../conf_avancadas/inserir_home.php",
		type: "post",
		dataType: "html",
		data: data,
		cache: false,
		success: function(dataServer){
			$(".conteudo").html(dataServer);
		}
	});
}

function conf_avancadasInsereHome(box){
	$("#loading").show();
	var titulo = $("#box_titulo").val();
	var id_split = "";
	var data = "";
	var inicio = "";
	$(".inpP.left").each(function(){
		if($(this).val() != ""){
			id_split = $(this).attr("id");
			id_split = id_split.split("_");
			data += inicio + "linha_" + id_split[1] + "=" + $(this).val() + "-|-" + $("#link_" + id_split[1]).val();					
			if($("#link_"+id_split[1]).val() == 1){
				data += "-|-" + $("#url_"+id_split[1]).val();
			}
			inicio = "&";
		}
	});
	data += inicio + "titulo=" + titulo;
	data += "&id=" + box;
	
	$.ajax({
		url: "../conf_avancadas/insereHome.php",
		type: "post",
		dataType: "html",
		data: data,
		cache: false,
		success: function(dataServer){
			if(dataServer == 1){
				$(".interna_msg p").text("Dados salvos com sucesso!");
				$(".interna_msg").show(500).delay(2500).hide(500);
				$("#loading").hide();
			}
			else{
				$(".interna_msg p").text("Ocorreram erros, tente novamente!");
				$(".interna_msg").show(500).delay(2500).hide(500);
				$("#loading").hide();
			}
		}
	});
}

function conf_avancadasAddLink(){	
	var selects = $("select").length;	
	var quant = selects + 1;
	if(quant - 1 < 4) {
		$(".home_boxes div label:lt(2)").each(function(){
			$(this).clone().appendTo(".home_boxes div:first");	
			$(".home_boxes div label:last span").text(quant);
			$(".home_boxes div label:last").attr("title", "label_"+quant);
		});
		
		$(".home_boxes div:first").append("<br clear='all' />");
		
		$(".home_boxes div input:first").clone().appendTo(".home_boxes div:first");
		$(".home_boxes div input:last").attr("id", "atalho_" + quant).val("");
		
		$(".home_boxes div select:first").clone().appendTo(".home_boxes div:first");
		$(".home_boxes select:last").attr("id", "link_" + quant).val("");
		//$("#link_"+quant).msDropDown({mainCSS:'select_4'});		
		
		$(".home_boxes div:first").append("<br clear='all' />");
		if(quant < 4){	
					
			$("#bt_add").insertAfter("label.left:last");
			$("#bt_add").attr("style", "float: left; margin: 10px 20px 5px -36px;");			
		}
		else{
			
			$("#bt_add").hide();
		}
	}
}

function linkExterno(valor, id, box){	
	var temp = id.split("_");
	if(valor == 1){
		if($("input[id=url_"+temp[1]+"]").length == 0){
			$("label[title=label_" + temp[1] + "]:eq(1)").after("<label title='label_"+ temp[1] +"' class='left' style='width:150px; padding:10px 0 0 0;'>Url do link <span>"+ temp[1] +"</span></label>");
			$("#"+id).after("<input id='url_"+ temp[1] +"' type='text' class='inpP' style='width:194px; margin-left: 8px;' />");
			//$("#link_"+temp[1]).msDropDown({mainCSS:'select_4'});
			var selects = $("select").length;		
			if($("#url_"+ selects).length > 0) {
				$("#bt_add").hide();
			}
			else{
				$("#bt_add").attr("style", "float: left; margin: 10px 20px 5px -36px; display: block;");
			}
		}
	}
	else{
		$("label[title=label_" + temp[1] + "]:eq(2)").remove();
		$("input[id=url_" + temp[1] + "]").remove();
		var selects = $("select").length;		
		if($("input[id=url_"+ selects +"]").length > 0) {
			$("#bt_add").attr("style", "");
		}
		else{
			$("#bt_add").attr("style", "float: left; margin: 10px 20px 5px -36px;");
		}
	}
}

function setColorbox(){
	$.colorbox({ inline:true, href:"#localizacao", transition: "none"});
}


function addImagem(imagem, id){
	if(((imagem) && (imagem != "")) && ((id) && (id != "")) ){
		$("#box_img").remove();
		$("#box_titulo").after("<img id='box_img' src='../../img/home/"+ imagem +"' class='impGM_img' style='cursor:pointer' onclick='setColorbox();' />")
		
		$.ajax({
			url: "../conf_avancadas/addImg.php",
			type: "post",
			dataType: "html",
			data: "imagem="+imagem+"&id="+id,
			cache: false,
			success: function(dataServer){
				if(dataServer == 1){
					$.colorbox.close();
				}
			}
		});
	}
}
</script>