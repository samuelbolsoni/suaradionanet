<script type="text/javascript">
//funcoes para o modulo Faq
var menu = "";
var submenu = "";
var caminho = "";
var linhas = "";
var busca = "";
var limit = "";
var offset = "";

function faqListar(buscar, limit, offset){
	$("#loading").show(); //Loader
	cont = $(".tbl_lista tbody tr").length; //numero de linhas na tabela
	busca = $(".inpBox").val(); //valor de busca, caso exista		
	if(!buscar) { // se a tag 'buscar' nao for ativada, o conteudo recebido sera aclopado as linhas ja existentes
		if(cont < 25) { 
			limit = "";
			offset = "";
		}
		else { 
			limit = cont + 25;
			offset = cont;
		}
	}
	else{ // caso contrario carregara uma nova tabela 
		if(!limit && !offset){ //verifica a existencia de um limit e offset pre-definidos
			limit = "";
			offset = "";
		}		
		$("#todos").attr("checked", ""); //ao carregar uma tabela nova, limpa o check de 'todos'
	}
	$.ajax({
		url: "../faq/listar.php",
		type: "post",
		dataType: "html",
		data: "offset="+offset+"&limit="+limit+"&busca="+busca,
		cache: false,
		success: function(dataServer){
			if(buscar) { $("tbody, .interna_criado").html("");} //se existir a tag buscar, o conteudo de tbody e .interna_criado sera deletado
			var res = dataServer.split("-----SEPARADOR-----"); //organiza a resposta com uma divisao pre-definida no arquivo .php
			$(".conteudo table tbody").append(res[0]); //adiciona as tabelas a uma ja existente			
			
			if((!isNaN(res[1])) && (res[1] > 1)){ // verifica se a divisao da resposta e um numero e se ele e maior que um
				$(".interna_criado").html(res[1]+" registros encontrados."); //resposta no plural
			}
			else{
				if((!isNaN(res[1])) && (res[1] == 1)){ // verifica se a divisao da resposta e um numero e se ele e igual a um
					$(".interna_criado").html(res[1]+" registro encontrado."); //resposta no singular
				}
			}
			var cont2 = $(".tbl_lista tbody tr").length; //calcula a quantidade de linhas atual			
			if((!isNaN(res[1])) && ((res[1] - cont2) > 0)){ //verifica se ha diferenca do numero total de registro com o numero atual de linhas
				$(".interna_voltar").show(); //caso a diferenca exista e seja positiva, a opcao de mostrar os proximos itens estara ativa			
			}
			else{
				$(".interna_voltar").hide(); //caso contrario sera desativada
			}			
		},
		complete: function(){
			$("#loading").hide(); // ao completar a funcao o loader sumira
		}
	});
}

function faqInserir(id){
	menu = $(".titulo span:eq(0)").html();
	submenu = $(".titulo span:eq(1)").html();
	caminho = $("#caminho").val();
	cont = $(".tbl_lista tbody tr").length;
	busca = $(".inpBox").val();
	if(id){		
		var data = "id="+id+"&limit="+cont+"&busca="+busca+"&menu_post="+menu+"&submenu_post="+submenu+"&caminho_post="+caminho;
	}
	else{
		var data = "limit="+cont+"&busca="+busca+"&menu_post="+menu+"&submenu_post="+submenu+"&caminho_post="+caminho;
	}	
	$.ajax({
		url: "../faq/inserir.php",
		type: "post",
		dataType: "html",
		data: data,
		cache: false,
		success: function(dataServer){
			$(".conteudo").html(dataServer);
		},
		complete: function(){
			$("#ativo").msDropDown({mainCSS:'select_3'});
		         
		}				
	});
}

function faqInsere(){	
	$("#loading").show();	
	var pergunta = $("#pergunta").val();
	var resposta = $("#editor").val();

	if(pergunta == "" || resposta == "") {
		$(".interna_msg p").html("Preencha todos os campos!");
		$(".interna_msg").show(500).delay(5000).hide(500);
	}
	else{
		
		var tags = "";
		var inicio = "";
		
		$("span[title='tag']").each(function(){
			tags += inicio + $(this).text();
			inicio = "-|-";
		});
		
		var ativo = $("#ativo").val();
		
		var data = "pergunta="+pergunta+"&resposta="+resposta+"&tags="+tags+"&ativo="+ativo;
		
		var id = $("#id").val();	
							
		if(id != "") data += "&id="+id;
		
		$.ajax({
			url: "../faq/insere.php",
			type: "post",
			dataType: "html",
			data: data,
			cache: false,
			success: function(dataServer){
				var teste = dataServer.split("-");
				if(teste[0] == 1){						
					 Menu(caminho, submenu, menu, limit, 0, busca, 'cadastrado');						
				}
				else{
					if(dataServer == 2){					
						Menu(caminho, submenu, menu, limit, 0, busca, 'atualizado');							
					}
					else{
						var retorna = dataServer;
						retorna = retorna.split("|");
						if(retorna[0] == 3){
							$(".interna_msg p").html(retorna[1] + " FAQ j&aacute; existe!");
							$(".interna_msg").show(500).delay(5000).hide(500);
							for(var i=2; i<retorna.length; i++){
								$("#"+retorna[i]).css("color", "red").click(function(){
									$(this).css("color", "#242B33");
								});
							}
							$("#loading").hide();
						}						
						else{
							$(".interna_msg p").html("Erro ao salvar!");
							$(".interna_msg").show(500).delay(5000).hide(500);
							$("#loading").hide();
						}
					}
				}			
			},
			complete: function(){
				$("#loading").hide();
				menusAtualiza();
			}
		});
	}
}

function faqAcoes(){
	var acao = $("#acao_lista").val();
	switch(acao){
		case 'excluir':
			faqExcluir('', 1);
			break;
		case 'default':
			break;
	}
}

function faqExcluir(id, select){		
	if(select){
		var checks = $("input:checked").length;
		if($("input:checked").length == 0){
			$(".interna_msg p").html("Nenhum registro selecionado!");
			$(".interna_msg").show(500).delay(5000).hide(500);			
		}
		else{
			var n_id = "";
			var inicio = "";
			$("input:checked").each(function(){
				if($(this).attr("id") != "todos"){
					n_id += inicio + $(this).val();
					inicio = "|";
				}
			});			
			$("#modal_id").val(n_id);
			$.colorbox({ inline:true, href:'#localizacao'});			
		}
	}
	else{
		if(id){
			$("#modal_id").val(id);
			$.colorbox({ inline:true, href:'#localizacao', transition: "fade"});
		}
		
	}	
}

function faqExclui(){
	$("#loading2").show();
	var id = $("#modal_id").val();
	$.ajax({
		url: "../faq/exclui.php",
		type: "post",
		dataType: "html",
		data: "id="+id,
		cache: false,
		success: function(dataServer){
			if(dataServer == 1){				
				$.colorbox.close();
				$(".interna_msg p").html("Registro(s) exclu&iacute;do(s) com sucesso!");
				$(".interna_msg").show(500).delay(5000).hide(500);
			}
			else{			
				$(".msg p").html("Erro ao exluir registro(s)!");
				$(".msg").show(500).delay(5000).hide(500);
			}
		},
		complete: function(){			
			$("#loading2").hide();			
			var n_id = id.split("|");
			n_id = n_id.length;
			var cont = $(".tbl_lista tbody tr").length;
			var limit = cont - n_id;
			faqListar('buscar', limit, 0);
			menusAtualiza();
		}
	});
}

function excluirTag(id){
	$("#"+id).remove();
}

function addTag(event){
	var tag = $("#tag").val();
	var cont_tag = $("span[title='tag']").length;
	var tag = tag.split(/[,;]/g);
	
	if(event){
		var navegador = navigator.appVersion;	
		var navegador = navegador.split(" ");
		var tipo   = navegador[2];
		
		if ( tipo == "MSIE" ) var code = event.keyCode;
		else var code = event.which;
		
		if(code == 13) {
			for(var i = 0; i < tag.length; i++){
				if(tag[i] != ""){
					$(".interna_box_cinza2").prepend("<div class=\"left\" id=\"tag_"+(cont_tag + i) +"\"><img style=\"cursor: pointer\" src=\"../../img/geral_btn_x.png\" onclick=\"excluirTag('tag_"+ (cont_tag + i) +"');\"/><span title=\"tag\">"+tag[i]+"</span></div>");
					$("#tag").val("");
				}
			}
		}		
	}	
	else{
		for(var i = 0; i < tag.length; i++){
			if(tag[i] != ""){
				$(".interna_box_cinza2").prepend("<div class=\"left\" id=\"tag_"+(cont_tag + i) +"\"><img src=\"../../img/geral_btn_x.png\" onclick=\"excluirTag('tag_"+(cont_tag + i) +"');\"/><span title=\"tag\">"+tag[i]+"</span></div>");
				$("#tag").val("");
			}
		}
	}
}
</script>