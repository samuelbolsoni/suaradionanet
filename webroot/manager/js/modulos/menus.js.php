<script type="text/javascript">
//Funções para o submenu Menus
var menu = "";
var submenu = "";
var caminho = "";
var linhas = "";
var busca = "";
var limit = "";
var offset = "";

function menusListar(buscar, limit, offset){
	$("#loading").show(); //Loader
	cont = $(".tbl_lista tbody tr").length; //numero de linhas na tabela
	busca = $(".inpBox").val(); //valor de busca, caso exista		
	if(!buscar) { // se a tag 'buscar' nao for ativada, o conteudo recebido sera aclopado as linhas ja existentes
		if(cont < 25) { 
			limit = "";
			offset = "";
		}
		else { 
			limit = cont + 25;
			offset = cont;
		}
	}
	else{ // caso contrario carregara uma nova tabela 
		if(!limit && !offset){ //verifica a existencia de um limit e offset pre-definidos
			limit = "";
			offset = "";
		}		
		$("#todos").attr("checked", ""); //ao carregar uma tabela nova, limpa o check de 'todos'
	}
	$.ajax({
		url: "../menus/listar.php",
		type: "post",
		dataType: "html",
		data: "offset="+offset+"&limit="+limit+"&busca="+busca,
		cache: false,
		success: function(dataServer){
			if(buscar) { $("tbody, .interna_criado").html("");} //se existir a tag buscar, o conteudo de tbody e .interna_criado sera deletado
			var res = dataServer.split("-----SEPARADOR-----"); //organiza a resposta com uma divisao pre-definida no arquivo .php
			$(".conteudo table tbody").append(res[0]); //adiciona as tabelas a uma ja existente			
			
			if((!isNaN(res[1])) && (res[1] > 1)){ // verifica se a divisao da resposta e um numero e se ele e maior que um
				$(".interna_criado").html(res[1]+" registros encontrados."); //resposta no plural
			}
			else{
				if((!isNaN(res[1])) && (res[1] == 1)){ // verifica se a divisao da resposta e um numero e se ele e igual a um
					$(".interna_criado").html(res[1]+" registro encontrado."); //resposta no singular
				}
			}
			var cont2 = $(".tbl_lista tbody tr").length; //calcula a quantidade de linhas atual			
			if((!isNaN(res[1])) && ((res[1] - cont2) > 0)){ //verifica se ha diferenca do numero total de registro com o numero atual de linhas
				$(".interna_voltar").show(); //caso a diferenca exista e seja positiva, a opcao de mostrar os proximos itens estara ativa			
			}
			else{
				$(".interna_voltar").hide(); //caso contrario sera desativada
			}			
		},
		complete: function(){
			$("#loading").hide(); // ao completar a funcao o loader sumira
		}
	});
}


function menusInserir(id){
	menu = $(".titulo span:eq(0)").html();
	submenu = $(".titulo span:eq(1)").html();
	caminho = $("#caminho").val();
	cont = $(".tbl_lista tbody tr").length;
	busca = $(".inpBox").val();
	if(id){		
		var data = "id="+id+"&limit="+cont+"&busca="+busca+"&menu_post="+menu+"&submenu_post="+submenu+"&caminho_post="+caminho;
	}
	else{
		var data = "limit="+cont+"&busca="+busca+"&menu_post="+menu+"&submenu_post="+submenu+"&caminho_post="+caminho;
	}	
	$.ajax({
		url: "../menus/inserir.php",
		type: "post",
		dataType: "html",
		data: data,
		cache: false,
		success: function(dataServer){
			$(".conteudo").html(dataServer);
		}
	});
}

function menusInsere(){
	$("#loading").show();
	var cont = 0;
	$("input.inpG").each(function(){
		if($(this).val() != ""){
			cont++;
		}
	});	
	if(cont == 0){
		$(".interna_msg p").html("Nenhum campo preenchido!");
		$(".interna_msg").show(500).delay(5000).hide(500);
	}
	else{
		var data = "";
		var inicio = "";
		$("input.inpG").each(function(){
			if($(this).val() != ""){
				data += inicio + $(this).attr('id')+"="+$(this).val();
				inicio = "&";
			}
		});
		
		var id = $("#id").val();
		if(id != "") data += "&id="+id;
		
		$.ajax({
			url: "../menus/insere.php",
			type: "post",
			dataType: "html",
			data: data,
			cache: false,
			success: function(dataServer){
				if(dataServer == 1){
					Menu(caminho, submenu, menu, limit, 0, busca, 'cadastrado');
					menusAtualiza();
				}
				else{
					if(dataServer == 2){
						Menu(caminho, submenu, menu, limit, 0, busca, 'atualizado');
						menusAtualiza();
					}
					else{
						var retorna = dataServer;
						retorna = retorna.split("|");
						if(retorna[0] == 3){
							$(".interna_msg p").html(retorna[1] + " menu(s) j&aacute; existe(m)!");
							$(".interna_msg").show(500).delay(5000).hide(500);
							for(var i=2; i<retorna.length; i++){
								$("#"+retorna[i]).css("color", "red").click(function(){
									$(this).css("color", "#242B33");
								});
							}
						}						
						else{
							$(".interna_msg p").html("Erro ao salvar!");
							$(".interna_msg").show(500).delay(5000).hide(500);
						}
					}
				}			
			},
			complete: function(){
				$("#loading").hide();
			}
		});
	}
}

function menusAcoes(){
	var acao = $("#acao_lista").val();
	switch(acao){
		case 'excluir':
			menusExcluir('', 1);
			break;
		case 'default':
			break;
	}
}

function menusExcluir(id, select){		
	if(select){
		var checks = $("input:checked").length;
		if($("input:checked").length == 0){
			$(".interna_msg p").html("Nenhum registro selecionado!");
			$(".interna_msg").show(500).delay(5000).hide(500);			
		}
		else{
			var n_id = "";
			var inicio = "";
			$("input:checked").each(function(){
				if($(this).attr("id") != "todos"){
					n_id += inicio + $(this).val();
					inicio = "|";
				}
			});			
			$("#modal_id").val(n_id);
			$.colorbox({ inline:true, href:'#localizacao'});			
		}
	}
	else{
		if(id){
			$("#modal_id").val(id);
			$.colorbox({ inline:true, href:'#localizacao', transition: "fade"});
		}
		
	}	
}

function menusExclui(){
	$("#loading2").show();
	var id = $("#modal_id").val();
	$.ajax({
		url: "../menus/exclui.php",
		type: "post",
		dataType: "html",
		data: "id="+id,
		cache: false,
		success: function(dataServer){
			if(dataServer == 1){				
				$.colorbox.close();
				$(".interna_msg p").html("Registro(s) exclu&iacute;do(s) com sucesso!");
				$(".interna_msg").show(500).delay(5000).hide(500);
			}
			else{			
				$(".msg p").html("Erro ao exluir registro(s)!");
				$(".msg").show(500).delay(5000).hide(500);
			}
		},
		complete: function(){			
			$("#loading2").hide();			
			var n_id = id.split("|");
			n_id = n_id.length;
			var cont = $(".tbl_lista tbody tr").length;
			var limit = cont - n_id;
			menusListar('buscar', limit, 0);
			menusAtualiza();
		}
	});
}

function menusAdicionarCampo(){
	var cont = $("input:text").length;
	//cria um label e um input novo
	$("label.left:first").clone().appendTo("form");	
	$("input.inpG:first").clone().appendTo("form");
	$("input.inpG:last").css("color", "#242B33").val("");
	var nome_id = cont + 1;
	$("input.inpG:last").attr("id", "menu_"+nome_id);
	//realoca o bota de +
	$("#bt_add").insertAfter("label.left:last");	
}
</script>