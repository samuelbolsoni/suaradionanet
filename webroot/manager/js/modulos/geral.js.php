<script type="text/javascript">
function mailvalido(v){

	var mailok = true;

    //var climail = document.getElementById("email");

    var n = v.length;
    var pa = v.indexOf("@");
    var ua = v.lastIndexOf("@");
    if(pa!=ua){
        mailok = false;
    }
    else if(pa==0 || ua==n-1){    	
        mailok = false;
    }
    else{
        var charv = new Array("q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "a", "s", "d", "f", "g", "h", "j", "k", "l", "z", "x", "c", "v", "b", "n", "m", "-", "_", ".", "@");
        var dok = true;
        var uok = true;
        var domi = v.substring(pa+1, n);
        var user = v.substring(0, pa);
        //
        var din = domi.indexOf(".");
        var dinl = domi.lastIndexOf(".");
        if(din == - 1 || din == 0 || dinl == (domi.length-1)){ dok = false; }
        //
        var uip = user.indexOf(".");
        var uipl = user.lastIndexOf(".");
        if(uip == 0 || uipl == (user.length-1)){ uok = false; }
        if(!dok || !uok){        	
            mailok = false;
        }
        //
        for (var i=0;i<n;i++){
            var l = v.charAt(i);
            var charok = false;
            for(var c=0;c<=9;c++){
               if(l == c.toString()){ charok = true; }
            }
            for(var cc=0;cc<charv.length;cc++){
                if(l == charv[cc]){ charok = true; }
            }
            if(!charok){               
               mailok = false;
            }
        }
    }
	return mailok;
}

function Trim(str){
	return str.replace(/^\s+|\s+$/g,"");
}


//url_encode version 1.0 
function url_encode(str) { 
    var hex_chars = "0123456789ABCDEF"; 
    var noEncode = /^([a-zA-Z0-9\_\-\.])$/; 
    var n, strCode, hex1, hex2, strEncode = ""; 

    for(n = 0; n < str.length; n++) { 
        if (noEncode.test(str.charAt(n))) { 
            strEncode += str.charAt(n); 
        } else { 
            strCode = str.charCodeAt(n); 
            hex1 = hex_chars.charAt(Math.floor(strCode / 16)); 
            hex2 = hex_chars.charAt(strCode % 16); 
            strEncode += "%" + (hex1 + hex2); 
        } 
    } 
    return strEncode; 
} 

// url_decode version 1.0 
function url_decode(str) { 
    var n, strCode, strDecode = ""; 

    for (n = 0; n < str.length; n++) { 
        if (str.charAt(n) == "%") { 
            strCode = str.charAt(n + 1) + str.charAt(n + 2); 
            strDecode += String.fromCharCode(parseInt(strCode, 16)); 
            n += 2; 
        } else { 
            strDecode += str.charAt(n); 
        } 
    } 

    return strDecode; 
}

function fazBuscaGeral(event, sub){
	
	var navegador = navigator.appVersion;	
	var navegador = navegador.split(" ");
	var tipo   = navegador[2];
	
	if ( tipo == "MSIE" ) var code = event.keyCode;
	else var code = event.which;
	
	if(code == 13) {
		//var sub = $(".conteudo .titulo span:eq(1)").html(); 
		switch(sub){
			case 'menus':
				if(tipo == "MSIE") event.returnValue = false; 
				else event.preventDefault(); 
				menusListar('buscar');
				break;
			case 'submenus':
				if(tipo == "MSIE") event.returnValue = false; 
				else event.preventDefault();
				submenusListar('buscar');
				break;
			case 'inscricoes':
				if(tipo == "MSIE") event.returnValue = false; 
				else event.preventDefault();
				inscricoesListar('buscar');
				break;
			case 'grade':
				if(tipo == "MSIE") event.returnValue = false; 
				else event.preventDefault();
				gradeListar('buscar');
				break;
			case 'usuarios':
				if(tipo == "MSIE") event.returnValue = false; 
				else event.preventDefault(); 
				usuariosListar('buscar');
				break;
			case 'permissoes':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				permissoesListar('buscar');
				break;
			case 'faq':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				faqListar('buscar');
				break;
			case 'perguntas':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				perguntasListar('buscar');
				break;
            case 'hotel':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				hotelListar('buscar');
				break;
            case 'quemsomos':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				quemsomosListar('buscar');
				break;
            case 'infra':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				infraListar('buscar');
				break;
            case 'clientes':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				clientesListar('buscar');
				break;
            case 'local':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				localListar('buscar');
				break;
            case 'negocios':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				negociosListar('buscar');
				break;
            case 'eventos':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				eventosListar('buscar');
				break;
            case 'eventos_fotos':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				eventos_fotosListar('buscar');
				break;
            case 'hoteis':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				hoteisListar('buscar');
				break;
            case 'hoteis_fotos':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				hoteis_fotosListar('buscar');
				break;
            case 'parceiros':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				parceirosListar('buscar');
				break;
            case 'parceiros_fotos':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				parceiros_fotosListar('buscar');
				break;
            case 'feiras':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				feirasListar('buscar');
				break;
            case 'feiras_fotos':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				feiras_fotosListar('buscar');
				break;
            case 'montagem':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				montagemListar('buscar');
				break;
            case 'alimentacao':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				alimentacaoListar('buscar');
				break;
            case 'guia':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				guiaListar('buscar');
				break;
            case 'noticias':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				noticiasListar('buscar');
				break;
            case 'curriculos':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				curriculosListar('buscar');
				break;
            case 'banqueteria':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				banqueteriaListar('buscar');
				break;
            case 'banqueteria_fotos':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				banqueteria_fotosListar('buscar');
				break;
            case 'hospitalar':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				hospitalarListar('buscar');
				break;
            case 'hospitalar_fotos':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				hospitalar_fotosListar('buscar');
				break;
            case 'hoteleira':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				hoteleiraListar('buscar');
				break;
            case 'hoteleira_fotos':
				if(tipo == "MSIE") event.returnValue = false;
				else event.preventDefault();
				hoteleira_fotosListar('buscar');
				break;
		}
	}
}

function telefoneM(a, e) {
	var tecla=(window.event)?event.keyCode:e.which;
	var navegador = navigator.appVersion;	
	var navegador = navegador.split(" ");
	var tipo   = navegador[2];	
    if((tecla > 47 && tecla < 58) || (tecla == 8) || (tecla == 46) || (tecla == 0)) {
		var telefone = $("#"+a).val();
		if(telefone.length == 0){
			$("#"+a).bind("keypress", function(e){
				if ( tipo == "MSIE" ) var resultado = e.keyCode;
				else var resultado = e.which;
				
				if(resultado!=8 && resultado!=46){
					var telefone = $("#"+a).val();
				    if(telefone.length == 1 && !isNaN(telefone)){
					    $("#"+a).val("(" + telefone);								  
				    }
				    if(telefone.length == 3){
					    $("#"+a).val(telefone + ") ");
				    }
				    if(telefone.length == 9){
					    $("#"+a).val(telefone + "-");
				    }			    
				}
				});															  		
		}
    }
    else{
    	if ( tipo == "MSIE" ) event.returnValue = false;
		else e.preventDefault();
    }
}


function dataM(a, e) {
	var tecla=(window.event)?event.keyCode:e.which;
	var navegador = navigator.appVersion;	
	var navegador = navegador.split(" ");
	var tipo   = navegador[2];	
    if((tecla > 47 && tecla < 58) || (tecla == 8) || (tecla == 46) || (tecla == 0)) {
		var data = $("#"+a).val();
		if(data.length == 0){
			$("#"+a).bind("keypress", function(e){
				if ( tipo == "MSIE" ) var resultado = e.keyCode;
				else var resultado = e.which;
				
				if(resultado!=8 && resultado!=46){
					var data = $("#"+a).val();
				    if(data.length == 2){
					    $("#"+a).val(data + "/");								  
				    }
				    if(data.length == 5){
					    $("#"+a).val(data + "/");
				    }			    
				}
			});															  		
		}
    }
    else{
    	if ( tipo == "MSIE" ) event.returnValue = false;
		else e.preventDefault();
    }
}

function horaM(a, e) {
	var tecla=(window.event)?event.keyCode:e.which;
	var navegador = navigator.appVersion;	
	var navegador = navegador.split(" ");
	var tipo   = navegador[2];	
    if((tecla > 47 && tecla < 58) || (tecla == 8) || (tecla == 46) || (tecla == 0)) {
		var data = $("#"+a).val();
		if(data.length == 0){
			$("#"+a).bind("keypress", function(e){
				if ( tipo == "MSIE" ) var resultado = e.keyCode;
				else var resultado = e.which;
				
				if(resultado!=8 && resultado!=46){
					var data = $("#"+a).val();
				    if(data.length == 2){
					    $("#"+a).val(data + ":");								  
				    }			    
				}
			});															  		
		}
    }
    else{
    	if ( tipo == "MSIE" ) event.returnValue = false;
		else e.preventDefault();
    }
}


function cepM(a, e) {
	var tecla=(window.event)?event.keyCode:e.which;
	var navegador = navigator.appVersion;	
	var navegador = navegador.split(" ");
	var tipo   = navegador[2];	
    if((tecla > 47 && tecla < 58) || (tecla == 8) || (tecla == 46) || (tecla == 0)) {
		var cep = $("#"+a).val();
		if(cep.length == 0){
			$("#"+a).bind("keypress", function(e){
				var navegador = navigator.appVersion;	
				var navegador = navegador.split(" ");
				var tipo   = navegador[2];
				
				if ( tipo == "MSIE" ) var resultado = e.keyCode;
				else var resultado = e.which;
				
				if(resultado!=8 && resultado!=46){
					var cep = $("#"+a).val();
				    if(cep.length == 5){
					    $("#"+a).val(cep + "-");								  
				    }			    
				}
				});															  		
		}
    }
    else{
    	if ( tipo == "MSIE" ) event.returnValue = false;
		else e.preventDefault();
    }
}

function mvalor(id, v, e){
	var tecla=(window.event)?event.keyCode:e.which;
	var navegador = navigator.appVersion;	
	var navegador = navegador.split(" ");
	var tipo   = navegador[2];
    if((tecla > 47 && tecla < 58) || (tecla == 8) || (tecla == 46)) {
	    v=v.replace(/\D/g,"");//Remove tudo o que não é dígito
	    v=v.replace(/(\d)(\d{7})$/,"$1.$2");//coloca o ponto dos milhões
	    v=v.replace(/(\d)(\d{4})$/,"$1.$2");//coloca o ponto dos milhares
	        
	    v=v.replace(/(\d)(\d{1})$/,"$1,$2");//coloca a virgula antes dos 2 últimos dígitos
	    //return v;
	    $("#"+id).val(v);
    }
    else{
    	if ( tipo == "MSIE" ) event.returnValue = false;
		else e.preventDefault();
    }
}
</script>