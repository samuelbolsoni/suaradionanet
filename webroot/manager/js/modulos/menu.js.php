<script type="text/javascript">
//Funcoes para o menu

function Menu(caminho, submenu, menu, limit, offset, busca, mensagem){
	$("#loader").show();
	if(offset || limit){
		var data = "caminho="+caminho+"&submenu="+submenu+"&menu="+menu+"&offset="+offset+"&limit="+limit+"&busca="+busca;
	}
	else{
		var data = "caminho="+caminho+"&submenu="+submenu+"&menu="+menu;
	}
	$.ajax({
		url: "../"+caminho+"/index.php",
		type: "post",
		dataType: "html",
		data: data,
		cache: false,
		success: function(dataServer){
			$(".conteudo").html(dataServer);
		},
		complete: function(){
			if((mensagem) && (mensagem != "")){
				switch(mensagem){
					case 'atualizado':
						$(".interna_msg p").html("Resgistro(s) atualizado(s) com sucesso!");
						$(".interna_msg").show(500).delay(5000).hide(500);
						break;
					case 'cadastrado':
						$(".interna_msg p").html("Resgistro(s) cadastrado(s) com sucesso!");
						$(".interna_msg").show(500).delay(5000).hide(500);
						break;					
				}
			}
			$("#loader").hide();
		}
	});
}

function menusAtualiza(){
	$.ajax({
		url: "../../inc/menu.php",
		type: "post",
		dataType: "html",
		data: "ajax=1",
		cache: false,
		success: function(dataServer){
			if(dataServer == "LOGIN"){
				document.location='../login/index.php';
			}
			else{
				$(".menu").html(dataServer);
			}				
		},
		complete: function(){
			initMenu();
		}
	});
}
</script>