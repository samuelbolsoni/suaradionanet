<script type="text/javascript">
//Funções para o submenu galeria
var menu = "";
var submenu = "";
var caminho = "";
var linhas = "";
var busca = "";
var limit = "";
var offset = "";

function galeriaListar(buscar, limit, offset){
	$("#loading").show(); //Loader
	cont = $(".tbl_lista tbody tr").length; //numero de linhas na tabela
	busca = $(".inpBox").val(); //valor de busca, caso exista
	if(!buscar) { //se a tag 'buscar' nao for ativada, o conteudo recebido sera aclopado as linhas ja existentes
		if(cont < 25) {
			limit = "";
			offset = "";
		}
		else {
			limit = cont + 25;
			offset = cont;
		}
	}
	else{ // caso contrario carregara uma nova tabela
		if(!limit && !offset){ //verifica a existencia de um limit e offset pre-definidos
			limit = "";
			offset = "";
		}
		$("#todos").attr("checked", ""); //ao carregar uma tabela nova, limpa o check de 'todos'
	}
	$.ajax({
		url: "../galeria/listar.php",
		type: "post",
		dataType: "html",
		data: "offset="+offset+"&limit="+limit+"&busca="+busca,
		cache: false,
		success: function(dataServer){
			if(buscar) { $("tbody, .interna_criado").html("");} //se existir a tag buscar, o conteudo de tbody e .interna_criado sera deletado
			var res = dataServer.split("-----SEPARADOR-----"); //organiza a resposta com uma divisao pre-definida no arquivo .php
			$(".conteudo table tbody").append(res[0]); //adiciona as tabelas a uma ja existente

			if((!isNaN(res[1])) && (res[1] > 1)){ // verifica se a divisao da resposta e um numero e se ele e maior que um
				$(".interna_criado").html(res[1]+" registros encontrados."); //resposta no plural
			}
			else{
				if((!isNaN(res[1])) && (res[1] == 1)){ // verifica se a divisao da resposta e um numero e se ele e igual a um
					$(".interna_criado").html(res[1]+" registro encontrado."); //resposta no singular
				}
			}
			var cont2 = $(".tbl_lista tbody tr").length; //calcula a quantidade de linhas atual			
			if((!isNaN(res[1])) && ((res[1] - cont2) > 0)){ //verifica se ha diferenca do numero total de registro com o numero atual de linhas
				$(".interna_voltar").show(); //caso a diferenca exista e seja positiva, a opcao de mostrar os proximos itens estara ativa			
			}
			else{
				$(".interna_voltar").hide(); //caso contrario sera desativada
			}			
		},
		complete: function(){
			$("#loading").hide(); // ao completar a funcao o loader sumira
			$("#loader").hide();
		}
	});
}

function galeriaInserir(id){
	menu = $(".titulo span:eq(0)").html();
	submenu = $(".titulo span:eq(1)").html();
	caminho = $("#caminho").val();
	cont = $(".tbl_lista tbody tr").length;
	busca = $(".inpBox").val();
	if(id){
		var data = "id="+id+"&limit="+cont+"&busca="+busca+"&menu_post="+menu+"&submenu_post="+submenu+"&caminho_post="+caminho;
	}
	else{
		var data = "limit="+cont+"&busca="+busca+"&menu_post="+menu+"&submenu_post="+submenu+"&caminho_post="+caminho;
	}
	$.ajax({
		url: "../galeria/inserir.php",
		type: "post",
		dataType: "html",
		data: data,
		cache: false,
		success: function(dataServer){
			$(".conteudo").html(dataServer);
		},
		complete: function(){
			verticalSliding();
			$(".interna_box_cinza2").sortable().disableSelection();
			
		}
	});
}

function galeriaInsere(){	
	$("#loading").show();
	
	var data = "";
	var inicio = "";
	
	$("#form input[type=text], #form select, #form textarea").each(function(){
		data+= inicio + $(this).attr("id") + "=" + $(this).val();
		inicio = "&";
	});

	//var texto = $("#texto").tinymce().getContent();
	//texto = url_encode(texto);

	//data+= inicio + "texto="+texto;

	var imagem = $("#id_imagem").val();
	data += "&imagem=" + imagem;

	var id = $("#id").val();

	if(id != "") data += "&id="+id;

	$.ajax({
		url: "../galeria/insere.php",
		type: "post",
		dataType: "html",
		data: data,
		cache: false,
		success: function(dataServer){			
			if(dataServer == 1){
				 Menu(caminho, submenu, menu, limit, 0, busca, 'cadastrado');
			}
			else {
				if(dataServer == 2){
					Menu(caminho, submenu, menu, limit, 0, busca, 'atualizado');
				}
				else {
					var teste = dataServer.split("|");
					if(teste[0] == 3){
						$(".interna_msg p").html("Produto já cadastrado!");
						$(".interna_msg").show(500).delay(5000).hide(500);
						$("#titulo").css("color", "red").focus().click(function(){
							$(this).css("color", "#242B33");
						});
					}
					else{
						$(".interna_msg p").html("Erro ao salvar!");
						$(".interna_msg").show(500).delay(5000).hide(500);
					}
				}
			}
		},
		complete: function(){
			$("#loading").hide();
		}
	});
}

function galeriaAcoes(id){
	if(id && id != ""){
		var acao = id;
	}
	else{
		var acao = $("#acao_lista").val();
	}
	switch(acao){
		case 'excluir':
			galeriaExcluir('', 1);
			break;
		case 'default':
			break;
	}
}

function galeriaExcluir(id, select){		
	if(select){
		var checks = $("input:checked").length;
		if($("input:checked").length == 0){
			$(".interna_msg p").html("Nenhum registro selecionado!");
			$(".interna_msg").show(500).delay(5000).hide(500);			
		}
		else{
			var n_id = "";
			var inicio = "";
			$("input:checked").each(function(){
				if($(this).attr("id") != "todos"){
					n_id += inicio + $(this).val();
					inicio = "|";
				}
			});			
			$("#modal_id").val(n_id);
			$.colorbox({ inline:true, href:'#localizacao'});			
		}
	}
	else{
		if(id){
			$("#modal_id").val(id);
			$.colorbox({ inline:true, href:'#localizacao', transition: "fade"});
		}
		
	}	
}

function galeriaExclui(){
	$("#loading2").show();
	var id = $("#modal_id").val();
	$.ajax({
		url: "../galeria/exclui.php",
		type: "post",
		dataType: "html",
		data: "id="+id,
		cache: false,
		success: function(dataServer){
			if(dataServer == 1){				
				$.colorbox.close();
				$(".interna_msg p").html("Registro(s) exclu&iacute;do(s) com sucesso!");
				$(".interna_msg").show(500).delay(5000).hide(500);
			}
			else{			
				$(".msg p").html("Erro ao exluir registro(s)!");
				$(".msg").show(500).delay(5000).hide(500);
			}
		},
		complete: function(){			
			$("#loading2").hide();			
			var n_id = id.split("|");
			n_id = n_id.length;
			var cont = $(".tbl_lista tbody tr").length;
			var limit = cont - n_id;
			galeriaListar('buscar', limit, 0);			
		}
	});
}
//funcoes para upload imagem sem crop
function salvaImagemgaleria(){
		$("#loader").show();
		var crop = {};
		crop["x"] = $("#x").val();
		crop["x2"] = $("#x2").val();
		crop["y"] = $("#y").val();
		crop["y2"] = $("#y2").val();
		crop["w"] = $("#w").val();
		crop["h"] = $("#h").val();
		crop["imagem"] = $("#id_imagem").val();
		
	
		$.ajax({
			url: "../galeria/crop.php",
			type: "post",
			dataType: "html",
			data: { crop: crop },
			cache: false,
			complete: function(dataServer){
				var id_imagem = $("#id_imagem").val();
				var id_imagem_completo = $("#id_imagem_completo").val();
				$("#imagem_crop").html("<img width='120px' height='90px' src='../../../uploads/img/galeria/thumb_"+id_imagem_completo+"' title=''><div class='links' style='width: 202px;'><p><a style='padding: 2px 43px 0;' href='javascript:;' onclick='galeriaFotoExcluir("+id_imagem+");'>Excluir</a></p></div>");
                $("#imagem_crop").show();                
                $("#loader").hide();
                $.colorbox.close();
                
			}
		});
	
}

function showCoords_galeria(c)
{
	$('#x').val(c.x);
	$('#y').val(c.y);
	$('#x2').val(c.x2);
	$('#y2').val(c.y2);
	$('#w').val(c.w);
	$('#h').val(c.h);

	var rx = 200 / c.w;
	var ry = 140 / c.h;	
	//alert("rx=" + Math.round(rx * w));
	//alert("ry=" + Math.round(ry * h)); 
	
	var w = $("#ImagemDiv img").width();
	var h = $("#ImagemDiv img").height();
	
	
	$('#ImagemDivThumb img').css({
		width: Math.round(rx * w) + 'px',
		height: Math.round(ry * h) + 'px',
		marginLeft: '-' + Math.round(rx * c.x) + 'px',
		marginTop: '-' + Math.round(ry * c.y) + 'px'
	});
	
};


//Funcoes para SWFUpload
function dialog_complete_galeria(number, queued, total){	
	var files = swfu.getStats().files_queued;	
	var qerrors = swfu.getStats().queue_errors;
	var uploads = swfu.getStats().successful_uploads;
	var cancelled = swfu.getStats().upload_cancelled;
	var uerrors = swfu.getStats().upload_errors;

	var todos = files + qerrors + uploads + cancelled + uerrors;
	//swfu.getFile(todos-1).name;
	if(files > 0){
		var file = swfu.getFile(todos-1).name;
		$("#getFile").val(file);
		var tit_file = file.split(".");
		$("#title_imagem").val(tit_file[0]);
	}
	//upload_start_cli();
	
}

function file_queue_error_galeria(file_object, error_code, message){
	//alert(error_code);
	switch(error_code){		
		case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
			//$("#msg_upload").text("Escolha um arquivo de no máximo 1 MB!").show(500).delay(2500).hide(500);
			alert("Escolha um arquivo de no máximo 1 MB!");
			break;
	}
}

function upload_start_galeria(){	
	var files = swfu.getStats().files_queued;	
	var qerrors = swfu.getStats().queue_errors;
	var uploads = swfu.getStats().successful_uploads;
	var cancelled = swfu.getStats().upload_cancelled;
	var uerrors = swfu.getStats().upload_errors;

	var todos = files + qerrors + uploads + cancelled  + uerrors;
	//swfu.getFile(todos-1).name;
	if(files > 0){		
		var file = swfu.getFile(todos-1).id;
		var title_imagem = $("#title_imagem").val();
		swfu.addPostParam("title", title_imagem);
		swfu.startUpload(file);		
		$("#fazerUpload").hide();
		$("#loading2").show();
	}	
}

function upload_success_galeria(file_object, server_data, received_response){	
	if(server_data != "") {		
		if(server_data == 0){			
			$(".msg p").text("Imagem pequena demais!");
			$(".msg").show(500).delay(2500).hide(500);
			var files = swfu.getStats().files_queued;	
			var qerrors = swfu.getStats().queue_errors;
			var uploads = swfu.getStats().successful_uploads;
			var cancelled = swfu.getStats().upload_cancelled;
			var uerrors = swfu.getStats().upload_errors;

			var todos = files + qerrors + uploads + cancelled  + uerrors;
			var file = swfu.getFile(todos-1).id;
			swfu.cancelUpload(file);
		}
		else{
			if($("#cropSN").attr("checked") == true){										
				$.colorbox({
					href: "../../../uploads/img/galeria/preview_"+server_data+"?rel=" + Math.random(), 
					scrolling: false, 
					overlayClose: false, 
					escKey: false,
					onLoad: function(){
					$("#cboxClose").hide().css("background", "url('../../img/modal_btn_ok.png') no-repeat scroll 0 0 transparent").css("top", "-30px").show();
					},
					onComplete: function(){						
						$("#cboxPhoto").Jcrop({
							aspectRatio: 200 / 140,
							minSize: [ 200, 140 ],
							setSelect:   [ 0, 0, 200, 140 ],
							onChange: showCoords_galeria,
							onSelect: showCoords_galeria
						});						
					},
					onCleanup: function(){
						$.ajax({
							url: "../galeria/crop.php",
							type: "post",
							dataType: "html",
							data: "x="+ $("#x").val() + "&x2=" + $("#x2").val() + "&y=" + $("#y").val() + "&y2=" + $("#y2").val() + "&w=" + $("#w").val() + "&h=" + $("#h").val() + "&imagem=" + server_data,
							cache: false,
							success: function(dataServer){
								if(dataServer == 1){
									var id = server_data.split("_");
									$("#imagem").val(id[0]);
									$(".conceitual_imagens div").remove();
									$(".conceitual_imagens").prepend("<div class='imagem' style='width: 122px; height: 92px'>"+
																		"<img src='../../../uploads/img/galeria/thumb_"+ server_data +"?rel=" + Math.random() + "' width='120px' height='90px' />"+
																		"<div class='links' style='width: 122px'>"+
																			"<p class='excluir'><a style='padding: 2px 43px 0' href='javascript:;' onclick='galeriaFotoExcluir("+ id[0] +");'>Excluir</a></p>"+
																		"</div>"+		
																	"</div>");
									$("div.imagem").css("background", "url('')");
									$(".links").hide();		
									$('.conceitual_imagens .imagem').hover(function(){
										$(".links", this).stop().animate({height:'20px'},{queue:false,duration:300});
									}, function() {
										$(".links", this).stop().animate({height:'0px'},{queue:false,duration:300});
									});
								}
							}
						});
					}
				});								
			}		
			else{
				var id = server_data.split("_");
				$("#imagem").val(id[0]);
				$(".conceitual_imagens div").remove();
				$(".conceitual_imagens").prepend("<div class='imagem' style='width: 122px; height: 92px'>"+
													"<img src='../../../uploads/img/galeria/thumb_"+ server_data +"?rel=" + Math.random() + "' width='120px' height='90px' />"+
													"<div class='links' style='width: 92px'>"+
														"<p><a style='padding: 2px 43px 0' href='javascript:;' onclick='galeriaFotoExcluir("+ id[0] +");'>Excluir</a></p>"+
													"</div>"+
												"</div>");
				$("div.imagem").css("background", "url('')");
				$(".links").hide();
				$('.conceitual_imagens .imagem').hover(function(){
					$(".links", this).stop().animate({height:'20px'},{queue:false,duration:300});
				}, function() {
					$(".links", this).stop().animate({height:'0px'},{queue:false,duration:300});
				});	
				$.colorbox.close();
			}											
		}
	}
	$("#loading2").hide();
	$("#fazerUpload").show();
	//alert(server_data);
}


function upload_error_galeria(file, error, message){
	alert(error);
	alert(message);
	$("#loading").hide();
	$("#enviar").show();
}
// END Funcoes para SWFUpload

function galeriaFotoExcluir(id){
	$(".links").html("<p style='width: 65px'><a style='padding:2px 0px 0; margin-left: 5px;' href='javascript:;' onclick='galeriaFotoExclui(" + id + ");'>Sim</a></p>" +
					 "<p style='width: 50px'><a style='padding:2px 0px 0; margin-left: 0px;' href='javascript:;' onclick='galeriaFotoNao(" + id + ");'>N&atilde;o</a></p>");	
}

function galeriaFotoNao(id){
	$(".links").html("<p><a style='padding: 2px 83px 0' href='javascript:;' onclick='galeriaFotoExcluir("+ id +");'>Excluir</a></p>");
}

function galeriaFotoExclui(id){
	$("#imagem").val("");
	if((id) && (id != "")){
		$.ajax({
			url: "../galeria/fotoExclui.php",
			type: "post",
			dataType: "html",
			data: "id="+id,
			cache: false,
			success: function(dataServer){
				if(dataServer == 1){
					$(".conceitual_imagens div img").remove();
					$(".conceitual_imagens div").hide();	
				}
			},
			complete: function(){
				$("#imagem").val('');
				$("#id_imagem").val("");
			}
		
		});
	}
}
</script>