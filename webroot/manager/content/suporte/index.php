				<?php require_once "../../inc/inicio.php"; ?>
                	<h1 class="titulo">Geral</h1>
                    <div class="geral_box">
                    	<div>
                        	<h1>Administrador</h1>
                            <img src="../../img/geral_img_1.png" />
                            <p>
                                <a href="javascript:;">Administração geral</a><br />
                                <a href="javascript:;">Administração de usuários</a><br />
                                <a href="javascript:;">Logins e Senhas</a><br />
                                <a href="javascript:;">Relação de usuários</a>
                            </p>
                            <br clear="all" />
                        </div>
                    	<div class="borda_tr">
                        	<h1>Adicionar</h1>
                            <img src="../../img/geral_img_2.png" />
                            <p>
                                <a href="javascript:;">Add usuário</a><br />
                                <a href="javascript:;">Add destaque</a><br />
                                <a href="javascript:;">Add notícia</a><br />
                                <a href="javascript:;">Add nova img de área conceitual</a>
                            </p>
                            <br clear="all" />
                        </div>
                    	<div class="borda_bl">
                        	<h1>Estatísticas</h1>
                            <img src="../../img/geral_img_3.png" />
                            <p>
                                <a href="javascript:;">Ver relatório de estatísticas do site</a><br />
                            </p>
                            <br clear="all" />
                        </div>
                    	<div>
                        	<h1>Mensagens</h1>
                            <img src="../../img/geral_img_4.png" />
                            <p>
                                <a href="javascript:;">Enviar mensagem interna</a><br />
                                <a href="javascript:;">Ver mensagens</a><br />
                            </p>
                            <br clear="all" />
                        </div>
                        <br clear="all" />
                    </div>
				<?php require_once "../../inc/fim.php"; ?>
