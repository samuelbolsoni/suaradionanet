<?php require_once "../../inc/verifica_login.php"; ?>
<?php
	if($_SERVER['REQUEST_METHOD']=='POST') {
		if($_POST['id']) $id = $_POST['id'];
		$limit         = $_POST['limit'];
		$busca         = $_POST['busca'];
		$menu_post     = $_POST['menu_post'];
		$submenu_post  = $_POST['submenu_post'];
		$caminho_post  = $_POST['caminho_post'];

		if(($id) && ($id != "")){
			$RES = mysql_fetch_array(mysql_query("SELECT * FROM site_novidades WHERE id = $id"));
			foreach($RES as $key=>$value){
				if($key != "id"){
					$$key = utf8_encode($value);					
				}
			}
			
			$data = explode(" ", $data);
			$data = $data[0];

			$dt_cad = implode("/", array_reverse(explode("-", $dt_cad)));
			$data = implode("/", array_reverse(explode("-", $data)));
		}
?>
					<h1 class="titulo">
                    	<span><?=$menu_post?></span>
                        <img src="../../img/cp_sep.png" />
                        <span><?=$submenu_post?></span>
                        <img src="../../img/cp_sep.png" />
                        <span>Editar</span>
                    </h1>

                    <div class="interna_box_cinza">
                    	<div class="botao left" onclick="Menu('<?=$caminho_post?>', '<?=$submenu_post?>', '<?=$menu_post?>');">
                        	<p>Listar</p>
                        </div>
                        <br clear="all" />
                    </div>

                    <form id="form" action="#" method="post">
                    	<input type="hidden" value="<?=$id?>" id="id" />

	                    <label for="titulo">Título:</label>
	                    <input type="text" class="inpG" id="titulo" value="<?=$titulo?>" />

	                    <br clear="all" />
	                    
	                    <label for="restrito">Restrição:</label><br/>
	                     <select id="restrito" class="select_3" style="width: 303px; height: 31px; padding: 5px 10px;">										
							<option value="1"<? if($restrito == 1){?> selected="selected"<?php }?>>Nenhuma</option>
							<option value="2"<? if($restrito == 2){?> selected="selected"<?php }?>>Área restrita</option>
						</select>
	                    
	                    <br clear="all" /> 
	                    <label for="texto">Data:</label>
	                    <br clear="all" />
	                    <input type="text" class="inpP" maxlength="10" onkeypress="dataM(this.id, event);" id="data" value="<?=$data?>" />

						<br clear="all" /><br/>
	                    
	                    
	                    <label for="texto">Texto:</label>
	                    <div class="editor">
	                    	<textarea id="texto" name="texto">
	                    		<?=$texto?>
		                    </textarea>
						</div>

						<br clear="all" /><br/>						
	                   						
	                </form>
                    <h2 class="subtitulo left" <?=$style?>>Imagem</h2>
                    <div class="botao left" <?=$style?>>
                    	<p onclick="$.colorbox.remove(); $.colorbox.init(); $.colorbox({ inline: true, href: '#localizacao', minHeigth: 340, minWidth: 790, scrolling: false });">+</p>
                    </div>

                    <br clear="all" />

                    <div class="conceitual_imagens" <?=$style?>>
                    <input type="hidden" id="imagem" value="<?=$imagem?>" />

                    <?php 
						if(($id) && ($id != "") && ($imagem != "0") && ($imagem != "")){																 								
							$RES = mysql_fetch_array(mysql_query("SELECT * FROM manager_imagens WHERE id = $imagem"));
							$foto_tal = $RES['imagem'];
							$title_tal = $RES['title'];
							if(is_file("../../../uploads/img/novidades/view_$foto_tal")) $foto_tal = "view_" . $foto_tal;
							else $foto_tal = "preview_" . $foto_tal;
							?>
						<div class="imagem" style="width: 202px; height: 142px">
                            <img title="<?=$title_tal?>" src="../../../uploads/img/novidades/<?=$foto_tal?>?rel=<? echo rand(0, 100000); ?>" width="200px" height="140px" />
                            <div class="links" style="width: 202px">
                                <p><a style="padding: 2px 83px 0" href="javascript:;" onclick="novidadesFotoExcluir(<?=$imagem?>);">Excluir</a></p>
                            </div>		
                        </div>
						<?php							
						}	                    	
                    ?>
                    </div>
                    <br clear="all" />

                    <div class="botao salvar" onclick="novidadesInsere();">
                    	<p>Salvar</p>
                    </div>
                    <div class="botao cancelar" onclick="Menu('<?=$caminho_post?>', '<?=$submenu_post?>', '<?=$menu_post?>', <?=$limit?>, 0, '<?=$busca?>');">
                    	<p>Cancelar</p>
                    </div>
                    <div class="botao cancelar" id="loading" style="display: none;">
	                    <img src="../../img/loading.gif" style="padding: 10px 5px 0px 5px" />
                    </div>

                    <br clear="all" />

                    <div class="interna_msg" style="display: none;">
                    	<p>Preencha todos os campos para salvar a postagem.</p>
                    </div>

                    <p class="interna_criado"><? if($id) echo $dt_cad; ?></p>
                    <p class="interna_voltar" onclick="Menu('<?=$caminho_post?>', '<?=$submenu_post?>', '<?=$menu_post?>');">Voltar</p>
                    <br clear="all" />

                    <!-- Espaco para colorbox -->
                    <div style='display:none'>
                    	<input type="hidden" id="modal_id" value="" />
                        <div id="localizacao" class="modal" align="center" style="height: 340px; width: 790px">

                        	<h1 style="text-align: left; padding: 25px 0 10px 20px">Upload de imagens:</h1>
                        	<p class="texto">
                        		Ao fazer o upload de imagens certifique-se de que a mesma esteja no formato correto (JPG, GIF, PNG ou BITMAP) e que o tamanho da não ultrapasse o tamanho máximo de <strong>20 MB</strong>.<br/>
								<br/>
Lembre-se que as dimensões recomendadas é no mínimo 195x130 px imagens menores serão rejeitadas pela aplicação.
Caso queira editar sua imagem após o envio ative a ferramenta <strong>Crop</strong> situada logo abaixo.
                        	</p>
                        	<h1 style="text-align: left; padding: 5px 0 2px 20px">Adicione sua Imagem:</h1>
                        	<input id="getFile" type="text" class="inpG" value="" />

                            <div class="botao_modal left" style="width:31px;margin: -1px 5px 36px 6px">
		                      	 <div id="btn_logo"></div>
		                    </div>

                            <div id="fazerUpload" class="botao_modal left" style="margin: -1px 15px 36px 0; width: 148px" onclick="upload_start_novidades();">
                            	<p>Fazer Upload</p>
                            </div>

                            <img src="../../img/loading.gif" id="loading2" style="display: none; padding: 10px 10px 0 5px"/>
                            <br clear="all" />
                            
                            <p class="texto" style="margin:-32px 40px 50px; float: left; height: 1px">Realizar <strong>Crop</strong> ap&oacute;s o envio?</p>
	                        <input id="cropSN" style="float: left; margin: -50px 5px 0 21px" type="checkbox" checked="checked" />                                                        
                            <br clear="all" />
                            
                            <div class="msg" style="display: none; margin: -10px 0px 0px 0px; width: 750px">                            	
                            	<p>Não é possível escluir este registro, contate seu suporte.</p>
                            </div>

                        </div>
                    </div>
                    <!-- inputs para controle de dimensoes do crop -->
                    <form style="display: none">
                    	<input type="hidden" id="x" />
                    	<input type="hidden" id="y" />
                    	<input type="hidden" id="x2" />
                    	<input type="hidden" id="y2" />
                    	<input type="hidden" id="w" />
                    	<input type="hidden" id="h" />
                    </form>
<?php
	}
?>

<script type="text/javascript">
$(document).ready(function(){
	$("div.imagem").css("background", "url('')");
});
var swfu;

var settings = {
	flash_url : "../../inc/swfupload/swfupload.swf",
	upload_url : "../../content/novidades/upload.php",	
	file_post_name : "Filedata",
	post_params:{
		"id" : "<?=$id?>",
		"imagem" : "<?=$imagem?>" 
	},
	prevent_swf_caching : true, 
	preserve_relative_urls : true,	
	file_types : "*.jpg;*.png;*.gif;*.bmp",
	file_types_description: "Arquivos de imagem da Web", 
	file_size_limit : "10MB", 
	button_image_url: "../../img/nome.png",
	button_width: "29",
	button_height: "28",
	button_placeholder_id: "btn_logo",
	button_text: '<p class="theFont"><b>+</b></p>',
    button_text_style: ".theFont { font-family: 'Arial';font-size: 20px; color: #242B33; text-align:center;  }",
	//button_text_left_padding: 10,
	button_text_top_padding: 2,
	button_cursor : SWFUpload.CURSOR.HAND, 
	button_window_mode : SWFUpload.WINDOW_MODE.TRANSPARENT,
	
	file_dialog_complete_handler:  dialog_complete_novidades,
	file_queue_error_handler: file_queue_error_novidades,
	upload_start_handler : upload_start_novidades,
	upload_error_handler: upload_error_novidades,
	upload_success_handler : upload_success_novidades,
    //queue_complete_handler : queueComplete, // Queue plugin event
	debug: false
	
};
swfu = new SWFUpload(settings);		
</script>