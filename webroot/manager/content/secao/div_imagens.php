<div id="conceitual" class="modal" align="center" style="height: 300px; width: 790px">
	<input type="hidden" id="id_imagem_conceitual" />
	<h1 style="text-align: left; padding: 25px 0 10px 20px">Upload de imagens:</h1>
	<p class="texto">
		Ao fazer o upload de imagens certifique-se de que a mesma esteja no formato correto (JPG, GIF, PNG ou BITMAP) e que o tamanho da não ultrapasse o tamanho máximo de <strong>1 MB</strong>.<br/>
		<br/>
	Lembre-se que as dimensões recomendadas são no mínimo 511x305 px. Imagens menores irão quebrar o layout do site.
	Caso queira editar sua imagem após o envio ative a ferramenta <strong>Crop</strong> situada logo abaixo.
	</p>
	<input title="Link da imagem" placeholder="Link da imagem" id="link_conceitual" type="text" class="inpG" value="" style="width: 200px" />
	<input title="Descrição da imagem" placeholder="Descrição da imagem" id="descricao_conceitual" type="text" class="inpG" value="" style="width: 470px" />
	<br clear="all" />
    <div id="conceitualImagemDiv3" style="display: none; float: right; margin: 0px 20px 0px 0px">
       <input type="checkbox" id="check_conceitual" style="float: left; margin: 40px 0 0 5px ;" /><label for="check_conceitual" style="font-family: arial; font-size: 12px; margin: 40px 0 0 5px; float: left; width: 125px">Marque para atualizar a thumb.</label>
   </div>
	<div id="conceitualImagemDiv" style="float: left; margin: 0px 0px 0px 20px;  border: 1px solid #C0C0C0; width: 511px; height: 305px; overflow: hidden; display: none">
	</div>
	<div id="conceitualImagemDivThumb" style="float: left; margin: 0px 0px 0px 20px;  border: 1px solid #C0C0C0; width: 71px; height: 53px; overflow: hidden; display: none">
	</div>
	<div id="conceitualImagemDivThumb2" style="float: left; margin: 0px 0px 0px 20px;  border: 1px solid #C0C0C0; width: 71px; height: 53px; overflow: hidden; display: none">
	</div>
	<br clear="all" />
	<div style="margin: 20px 20px 0 0; float: right">
	    <span style="font-size: 12px; float: left; font-family: arial; width: 255px"> * As imagens devem ter as dimensões: 511x305px</span>
		<div title="Clique para adicionar uma imagem" class="botao_modal left" style="margin: -1px 10px 0px 10px; width: 180px">
	    	<input type="file" name="files" id="fileupload_conceitual" />
	    	<p>Adicionar imagem</p>
	    </div>
	    <div title="Clique para deletar a imagem" class="botao_modal left" style="display: none; margin: -1px 10px 0px 10px;">
	    	<p onclick="deletaImagemConceitual();">Deletar</p>
	    </div>
	    <div title="Clique para salvar" class="botao_modal left" style="margin: -1px 10px 0px 10px;">
	    	<p onclick="salvaImagemConceitual()">Salvar</p>
	    </div>
    </div>
</div>
<div style="display: none" id="conceitual-1">
	<?php
	$conceituais = explode("|", $conceitual);
	foreach($conceituais as $key){
		$RESi = @mysql_fetch_array(mysql_query("SELECT * FROM manager_imagens WHERE id = $key"));
        $img = $RESi['imagem'];
		if($img != "" && $img != 0){
			echo "<input type=\"hidden\" id=\"conceitual1_".$RESi['id']."\" value=\"".$RESi['id']."\" />";
			echo "<input type=\"hidden\" id=\"conceitual2_".$RESi['id']."\" value=\"".$RESi['imagem']."\" />";
			echo "<input type=\"hidden\" id=\"conceitual3_".$RESi['id']."\" value=\"".$RESi['title']."\" />";
			echo "<input type=\"hidden\" id=\"conceitual4_".$RESi['id']."\" value=\"".utf8_encode($RESi['descricao'])."\" />";
		}
	}
	?>
</div>
