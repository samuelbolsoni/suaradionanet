<?php require_once "../../inc/verifica_login.php"; ?>
<?php
	if ($_SERVER['REQUEST_METHOD']=='POST') {
		$submenu_post = $_POST['submenu'];
		$menu_post    = $_POST['menu'];
		$caminho_post = $_POST['caminho'];
		$offset  = $_POST['offset'];
		$limit   = $_POST['limit'];	
		$busca   = $_POST['busca'];	
?>
                	<h1 class="titulo">
                    	<span><?=$menu_post?></span>
                        <img src="../../img/cp_sep.png" />
                        <span><?=$submenu_post?></span>
                        <img src="../../img/cp_sep.png" />
                        <span>Lista</span>
                        <input type="hidden" id="caminho" value="<?=$caminho_post?>" />
                    </h1>

                    <div class="interna_box_cinza">
                    	<div class="botao left" onclick="dicasInserir();">
                        	<p>Inserir</p>
                        </div>
                        <select id="acao_lista" style="width: 301px; border-radius: 5px 5px 5px 5px; margin-left: 240px; height: 30px; padding: 5px;"  onchange="dicasAcoes();">
                        	<option value="">A&ccedil;&otilde;es para &iacute;tens selecionados</option>
                        	<option value="excluir">Excluir</option>
                        </select>
                        
                        <br clear="all" />        
                    </div>
                    
                    <table cellpadding="0" cellspacing="0" border="0" class="tbl_lista">
                    	<thead>
                        	<tr>
                            	<td class="borda_tl">Conte&uacute;do</td>
                            	<td>Inserido/Modificado</td>
                            	<td class="borda_tr" width="90"><span>Edi&ccedil;&atilde;o</span><input id="todos" type="checkbox" /></td>
                            </tr>
                        </thead>
                    	<tbody>
                        	<!-- Espaco para retorno ajax -->
                        </tbody>
                    </table>
                    
                    <div class="interna_msg" style="display:none">
                    	<p>O item selecionado não pode ser excluído pois causará inconsistência no sistema.</p>
                    </div>
                    
                    <p class="interna_criado"></p>
                    <div id="loading" class="botao cancelar" style="display: none">
                    	<img src="../../img/loading.gif" style="padding: 10px 5px 0px 5px" />	
                    </div>
                    <p class="interna_voltar" onclick="dicasListar();" style="display: none">Mostrar mais &iacute;tens</p>
                    <br clear="all" />
                    
                    <div style='display:none'>
                    	<input type="hidden" id="modal_id" value="" />
                        <div id="localizacao" class="modal" align="center">
                        
                        	<h1>Deseja excluir este(s) registro(s)?</h1>
                            
                            <div class="botao_modal" onclick="dicasExclui();">
                            	<p>Sim</p>
                            </div>
                            
                            <div class="botao_modal" onclick="$.colorbox.close();">
                            	<p>Não</p>
                            </div>                                                                                                                
                            
                            <img src="../../img/loading.gif" id="loading2" style="display: none;"/>
                            
                            <div class="msg" style="display: none;">                            	
                            	<p>Não é possível escluir este registro, contate seu suporte.</p>
                            </div>

                        </div>
                    </div>
                    				
				<script language="javascript" type="text/javascript">
					<? 
						if($limit != "" && $offset != ""){
							echo "dicasListar('buscar', $limit, $offset);";	
						} else {										
                			echo "dicasListar();";                 
						}
                	?>
                </script>
				<?php require_once "../../js/modulos/funcoes.js.php"; ?>
<?php 
	}
?>                	