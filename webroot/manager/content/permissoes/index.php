<?php require_once "../../inc/verifica_login.php"; ?>
<?php
	if ($_SERVER['REQUEST_METHOD']=='POST') {
		$submenu_post = $_POST['submenu'];
		$menu_post    = $_POST['menu'];
		$caminho_post = $_POST['caminho'];
		$offset  = $_POST['offset'];
		$limit   = $_POST['limit'];
		$busca   = $_POST['busca'];		
?>				
                	<h1 class="titulo">
                    	<span><?=$menu_post?></span>
                        <img src="../../img/cp_sep.png" />
                        <span><?=$submenu_post?></span>
                        <img src="../../img/cp_sep.png" />
                        <span>Lista</span>
                        <input type="hidden" id="caminho" value="<?=$caminho_post?>" />
                    </h1>
                    
                    <div class="interna_box_cinza">
                    	<input type="text" class="inpBox" value="<?=$busca?>" onkeypress="fazBuscaGeral(event, 'permissoes');" />
                    	<div class="botao left" onclick="permissoesListar('buscar');">
                        	<p>Buscar</p>
                        </div>
                        <br clear="all" />        
                    </div>
                    
                    <div class="interna_box_cinza">
                    	<!-- <div class="botao left" onclick="permissoesInserir();">
                        	<p>Inserir</p>
                        </div> -->
                        <select id="acao_lista" style="width: 301px; border-radius: 5px 5px 5px 5px; margin-left: 315px; height: 30px; padding: 5px;" onchange="permissoesAcoes();">
                        	<option value="">A&ccedil;&otilde;es para itens selecionados</option>
                        	<?php 
                        		$user_temp = $_SESSION['usr_id_'.$id_sys];
			    				$RES_temp = mysql_fetch_array(mysql_query("SELECT * FROM manager_usuarios WHERE id = $user_temp"));
			    				if($RES_temp['tipo_usuario'] == 1){
                        	?>
                        	<option value="editar">Editar</option>
                        	<option value="excluir">Excluir</option>
                        	<?php 
			    				}
                        	?>
                        </select>
                        <br clear="all" />        
                    </div>
                    
                    <table cellpadding="0" cellspacing="0" border="0" class="tbl_lista">
                    	<thead>
                        	<tr>
                            	<td class="borda_tl">Conte&uacute;do</td>
                            	<td>Inserido/Modificado</td>
                            	<td class="borda_tr" width="90"><span>Edi&ccedil;&atilde;o</span><input type="checkbox" id="todos" /></td>
                            </tr>
                        </thead>
                    	<tbody>
                        	<!-- Espaco para retorno ajax -->
                        </tbody>
                    </table>                   
                    
                    <div class="interna_msg" style="display:none">
                    	<p>O item selecionado não pode ser excluído pois causará inconsistência no sistema.</p>
                    </div>
                    
                    <p class="interna_criado">5 registros encontrados.</p>
                    <div id="loading" class="botao cancelar" style="display: none">
                    	<img src="../../img/loading.gif" style="padding: 10px 5px 0px 5px" />
                    </div>
                    <p class="interna_voltar" onclick="permissoesListar();" style="display: none">Mostrar mais &iacute;tens</p>
                    <br clear="all" />
                    
                    <div style='display:none'>
                    	<input type="hidden" id="modal_id" value="" />
                        <div id="localizacao" class="modal" align="center">
                        
                        	<h1>Deseja excluir suas permiss&otilde;es ?</h1>
                            
                            <div class="botao_modal" onclick="permissoesExclui();">
                            	<p>Sim</p>
                            </div>
                            
                            <div class="botao_modal" onclick="$.colorbox.close();">
                            	<p>Não</p>
                            </div>                                                                                                                
                            
                            <img src="../../img/loading.gif" id="loading2" style="display: none;"/>
                            
                            <div class="msg" style="display: none;">                            	
                            	<p>Não é possível escluir este registro, contate seu suporte.</p>
                            </div>

                        </div>
                    </div>                                        
                    
				<script language="javascript" type="text/javascript">
					<? 
						if($limit != "" && $offset != ""){
							echo "permissoesListar('buscar', $limit, $offset);";	
						} else {										
                			echo "permissoesListar();";                 
						}
                	?>
                </script>
				
<?php 
	}
?>