<?php 
	require_once "../../../../connection/connection.php";		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<base href="<?=$url_base2?>/login/" />
        <link rel="stylesheet" type="text/css" href="../../style/style.css" />
        <?php require_once "../../inc/head.php"; ?>
        <script language="javascript" type="text/javascript">
        $(document).ready(function(){
        	var navegador = navigator.appVersion;	
        	var navegador = navegador.split(" ");
        	var tipo   = navegador[2];
        	
        	$("#login").keydown(function(event) {		
        		if ( tipo == "MSIE" ) var code = event.keyCode;
        		else var code = event.which;
        		if(code == 13) { 
        			if(tipo == "MSIE") event.returnValue = false; 
        			else event.preventDefault(); 
        			$("#senha").focus();
        		} 
        	});
        	
        	$("#senha").keydown(function(event){
        		var code = event.which;
        		if(code == 13) { 
        			if(tipo == "MSIE") event.returnValue = false; 
        			else event.preventDefault();
        			logar(); 
        		}
        	});
        	
        	$("#email").keydown(function(event){
        		var code = event.which;
        		if(code == 13) { 
        			if(tipo == "MSIE") event.returnValue = false; 
        			else event.preventDefault();
        			enviar(); }
        	});
        });
        </script>        
    </head>
    <body>
        <div id="sombra_geral" class="sombra_geral"></div>
        <?php require_once "../../inc/ie6.php"; ?>

      	<div class="corpo" align="center">
            <div class="centro" align="left">
            	<div class="pagina_login">
                    <div class="login_esq">
                        <img src="../../img/login_logo_w8s.png" class="w8s" />
                        <img src="../../img/login_sep_logos.png" class="sep" />
                        <img src="../../../uploads/img/logo/<?=$logo_cliente?>" class="cliente" />                        
                        <img src="../../img/login_logo_data_manager.png" class="data_manager" />
                    </div>
                    <div class="login_dir">
                        <div class="box">
                            <div class="login">
                                <label for="login">Login:</label>
                                <input id="login" type="text" class="inp" />
                                <label for="senha">Senha:</label>
                                <input id="senha" type="password" class="inp" />
                                <br clear="all" />
                                <p class="perdeu"><a id="perdeu_senha" href="javascript:;">Perdeu a senha?</a></p>
                                <div id="loading" class="botao logar" style="display: none">    
                                	<img src="../../img/loading.gif" style="padding: 10px 5px 0px 5px" />
                                </div>
                                <div id="botao_logar" onclick="logar();" class="botao logar">
                                    <p>Logar</p>
                                </div>
                                <br clear="all" />
                            </div>
                            <div class="senha" style="display:none;">
                                <label for="email">Digite seu e-mail:</label>
                                <input id="email" type="text" class="inp" />
                                <br clear="all" /> 
                                <p class="voltar"><a id="voltar" href="javascript:;">Voltar</a></p>
                                <div class="botao enviar" style="display: none" id="loading2">
                                	<img src="../../img/loading.gif" style="padding: 10px 5px 0px 5px" />
                                </div>                                                              
                                <div id="login_enviar" onclick="enviar();" class="botao enviar">
                                    <p>Enviar</p>
                                </div>
                                <br clear="all" />
                            </div>
                        </div>
                    </div>
                    <br clear="all" />
                    <div class="login_msg" style="display:none">
                        <p>Seu e-mail n&atilde;o consta em nossos banco de dados, tente novamente com outro e-mail.</p>
                    </div>                    
				</div>                    
            </div>
        </div>
        
        <div class="rodape" align="center">
			<?php require_once "../../inc/rodape.php"; ?>
        </div>
        
    </body>
</html>
