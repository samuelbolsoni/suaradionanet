                <div id="ie6" class="ie6">
                	<div class="left">
                    	<h5 class="ie6_tit_1"><span>ATEN&ccedil;&atilde;O:</span> VOC&ecirc; EST&aacute; USANDO UM NAVEGADOR DESATUALIZADO</h5>
                    	<h5 class="ie6_tit_2">CAMPANHA PARA ACABAR COM O USO DE BROWSERS OBSOLETOS</h5>
                    </div>
                    <div class="right">
                    	<a href="http://www.w8s.com.br" target="_blank"><img src="../../img/ie6_logo_w8s.jpg" class="ie6_w8s" alt="" /></a>
                    </div>
                    <br clear="all" />
                    <div class="ie6_txt_1">
                    	<p>
                            Vers&otilde;es antigas e desatualizadas de navegadores t&ecirc;m problemas de 
                            seguran&ccedil;a e n&atilde;o acompanham os novos padr&otilde;es de desenvolvimento para a 
                            Internet. O Atualize seu navegador faz parte de um movimento internacional 
                            para eliminar os navegadores obsoletos do mercado.                        
                        </p>
                    </div>
                    <div class="ie6_box_navegadores">
                    	<h5 class="tit">ESCOLHA O SEU NAVEGADOR</h5>
                        <table cellpadding="0" cellspacing="0" border="0">
                        <tr valign="baseline">
                            <td>
                            	<a href="http://www.microsoft.com/brasil/windows/internet-explorer/default.aspx" target="_blank">
                            		<img src="../../img/ie6_img_ie.jpg" alt="" />
                                </a>
                            </td>
                            <td>
                            	<a href="http://br.mozdev.org/download/" target="_blank">
                            		<img src="../../img/ie6_img_ff.jpg" alt="" />
                                </a>
                            </td>
                            <td>
                            	<a href="http://www.google.com/chrome/index.html" target="_blank">
                            		<img src="../../img/ie6_img_ch.jpg" alt="" />
                                </a>
                            </td>
                            <td>
                            	<a href="http://www.apple.com/safari/download/" target="_blank">
                            		<img src="../../img/ie6_img_sa.jpg" alt="" />
                                </a>
                            </td>
                            <td>
                            	<a href="http://www.opera.com/download/" target="_blank">
                            		<img src="../../img/ie6_img_op.jpg" alt="" />
                                </a>
                            </td>
                        </tr>
                        </table>
                        <div class="left">
                            <h5 class="tit">LINKS &Uacute;TEIS</h5>
                            <p><a href="http://cartilha.cert.br/" target="_blank">Cartilha de Seguran&ccedil;a para a Internet</a></p>
                            <p><a href="http://www.baboo.com.br/conteudo/modelos/As-pseudo-estatisticas-dos-navegadores_a27093_z0.aspx" target="_blank">Estat&iacute;sticas de Uso de Navegadores</a></p>
                            <p><a href="http://pt.wikipedia.org/wiki/Browsers" target="_blank">Lista de Navegadores da Wikipedia</a></p>
                        </div>
                        <div id="ie6_continuar" class="ie6_continuar">
                        	<h5><a href="javascript:;">DESEJO CONTINUAR NAVEGANDO MESMO ASSIM.</a></h5>
                        </div>
                        <br clear="all" />
                    </div>
                    <div class="ie6_txt_2">
                    	<h5>PORQUE ATUALIZAR</h5>
                    	<p>
                            Navegadores antigos como o Internet Explorer 6 (lan&ccedil;ado em 2001) n&atilde;o 
                            exibem sites dentro dos padr&otilde;es web, s&atilde;o repletos de bugs, n&atilde;o 
                            oferecem as funcionalidades dos browsers atuais como navega&ccedil;&atilde;o por abas ou 
                            gerenciador de downloads e est&atilde;o submetidos a s&eacute;rios problemas de seguran&ccedil;a 
                            como v&iacute;rus e malware.
                            <br />
                            <br />
                            Ainda comprometem a qualidade da exibi&ccedil;&atilde;o de sites e limitam a cria&ccedil;&atilde;o 
                            de desenvolvedores, que precisam perder tempo adaptando o site para rodar nesses navegadores obsoletos.
                        </p>
                    </div>
                    <br clear="all" />
                </div>
