        <!-- <base href="<?=$url_site?>/manager/content/home/index.php"> -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Data Manager</title>
        <link rel="shortcut icon" type="text/css" href="../../img/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="../../style/style.css" />        
        <link rel="stylesheet" type="text/css" href="../../style/ie6.css" />
        <link rel="stylesheet" type="text/css" href="../../style/colorbox.css" />
        <link rel="stylesheet" type="text/css" href="../../style/select.css" />
        <link rel="stylesheet" type="text/css" href="../../style/acordion_lista/jquery.ui.accordion.css"/>
        <link rel="stylesheet" type="text/css" href="../../style/acordion_lista/jquery.ui.theme.css"/>
        <link rel="stylesheet" type="text/css" href="../../style/black-tie/jquery-ui-1.8.6.custom.css"/>
        <link rel="stylesheet" type="text/css" href="../../style/smoothness/jquery-ui-1.8.6.custom.css"/>
        <link rel="stylesheet" type="text/css" href="../../inc/jcrop/css/jquery.Jcrop.css"/>
        <script language="javascript" type="text/javascript" src="../../js/cufon-yui.js"></script>
        <script language="javascript" type="text/javascript" src="../../js/helvetica_neue_condensed_bold_700.font.js"></script>
        <script language="javascript" type="text/javascript" src="../../js/principal.js"></script>
		<script language="javascript" type="text/javascript" src="../../js/jquery-1.7.1.min.js"></script>
		<script language="javascript" type="text/javascript" src="../../js/jquery_ui.js"></script>
		<script language="javascript" type="text/javascript" src="../../js/jquery-ui-1.8.6.progressbar.js"></script>
		<script language="javascript" type="text/javascript" src="../../js/jquery-ui-1.8.6.datepicker.js"></script>
		<script language="javascript" type="text/javascript" src="../../js/jquery.ui.datepicker-pt-BR.js">
			$.datepicker.setDefaults($.datepicker.regional['pt_BR']);
		</script>
		<script language="javascript" type="text/javascript" src="../../js/jquery.colorbox.js"></script>
        <script language="javascript" type="text/javascript" src="../../js/jquery.dd.js"></script>
        <script language="javascript" type="text/javascript" src="../../inc/tiny_mce/jquery.tinymce.js"></script>
        <script language="javascript" type="text/javascript" src="../../inc/swfupload/swfupload.js"></script>
        <script language="javascript" type="text/javascript" src="../../inc/swfupload/swfupload.queue.js"></script>
        
        <script language="javascript" type="text/javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>        
        <script language="javascript" type="text/javascript" src="../../inc/jcrop/js/jquery.Jcrop.js"></script>        
    	<script src="../../inc/file_upload/jquery.iframe-transport.js"></script>
		<script src="../../inc/file_upload/jquery.fileupload.js"></script>
        <script language="javascript" type="text/javascript">Navegador();</script>
		<script language="javascript" type="text/javascript">
		/*Cufon*/
			Cufon.replace('h5', {
                fontFamily: 'helvetica_neue_condensed_bold',
                hover: true
            });
		/*Cufon*/
			
		/*Colorbox*/
			$(document).ready(function() {
				//Examples of how to assign the ColorBox event to elements
				//$(".excluir").colorbox({ inline:true, href:"#localizacao", transition: "none"});
			});
		/*Colorbox*/
			
		/*Select*/
			$(document).ready(function() {
			try {
				  $("#acao_lista").msDropDown({mainCSS:'select_1'});
				  $("#menus").msDropDown({mainCSS:'select_2'});
				  $("#link_1").msDropDown({mainCSS:'select_4'});
				  $("#link_2").msDropDown({mainCSS:'select_4'});
				  $("#link_3").msDropDown({mainCSS:'select_4'});
				  $("#link_4").msDropDown({mainCSS:'select_4'});
				  } catch(e) {
					  alert("Error: "+e.message);
				  }
			})
		/*Select*/
		
		/*Login*/
			$(document).ready(function() {
				$("#perdeu_senha").click(function(){
					$(".login_msg").hide();
					$('.login').fadeOut( 450 , function() {
						$(".senha").stop().fadeIn( 450 );						
					});
				});
				
				$("#voltar").click(function(){
					$(".login_msg").hide();
					$('.senha').fadeOut( 450 , function() {
						$(".login").stop().fadeIn( 450 );			  
					});
				});
			});
		/*Login*/

		/*Vertical Sliding*/
			function verticalSliding(){
				//$(".links").hide();
				$(document).ready(function(){
					$('.conceitual_imagens .imagem').hover(function(){
						$(".links", this).stop().animate({height:'20px'},{queue:false,duration:300});
					}, function() {
						$(".links", this).stop().animate({height:'0px'},{queue:false,duration:300});
					});
				});
			}
		/*Vertical Sliding*/

		/*Over*/
			//Página Inicial
			$(document).ready(function() {
				$(function() { $('.geral_box').delegate('div', 'hover', hoverHandler);});
				function hoverHandler(event) {
					switch(event.type) {
						case 'mouseover':$(this).stop(true).animate({backgroundColor:'#39434F'}, 'slow'); break;
						case 'mouseout':$(this).stop(true, true).animate({backgroundColor:'#242B33'}, 'slow'); break;
					}
				}
			});

			//Tabela
			$(document).ready(function() {
				$(function() { $('table').delegate('tbody .c1', 'hover', hoverHandler);});
				function hoverHandler(event) {
					switch(event.type) {
						case 'mouseover':$(this).stop(true).animate({backgroundColor:'#39434F'}, 'fast'); break;
						case 'mouseout':$(this).stop(true, true).animate({backgroundColor:'#CCCCCC'}, 'fast'); break;
					}
				}
			});
			$(document).ready(function() {
				$(function() { $('table').delegate('tbody .c2', 'hover', hoverHandler);});
				function hoverHandler(event) {
					switch(event.type) {
						case 'mouseover':$(this).stop(true).animate({backgroundColor:'#39434F'}, 'fast'); break;
						case 'mouseout':$(this).stop(true, true).animate({backgroundColor:'#E3E3E3'}, 'fast'); break;
					}
				}
			});
		/*Over*/	
		  
		  $(document).ready(function() {
			$("#accordion").accordion( {
				autoHeight: false
			});
		  });

		/*Menu*/
		<?php 
		if(isset($_SESSION['usr_tipo_usuario_'.$id_sys]) && $_SESSION['usr_tipo_usuario_'.$id_sys] == 1){
		?>
		function initMenu(){
		  	$( "#menu" ).accordion({
			  	change: function(){
			  		var menu = parseInt($(".menu ul").css("height"));
			  		var conteudo = parseInt($(".conteudo").css("height"));

			  		if(menu > conteudo) {
			  		    $(".conteudo").css("min-height", menu + 16);
			  		}
			  	}, 
			  	collapsible: true, 
			  	autoHeight: false }).sortable({
			    update: function(){
				    $("#loader").show();
			        var result = $(this).sortable("toArray");
			        var data = ""; var inicio = "";
			        for(var x = 0; x < $(result).size(); x++){
			            data+= inicio + "var_" + x + "=" + result[x];
			            inicio = "&";
			        }
			        $.ajax({
						url: "../../inc/menu_posicao.php",
						type: "post",
						dataType: "html",
						data: data,
						cache: false,
						success: function(){
							$("#loader").hide();
						}						
				    });
			    }
			});
			$("#menu ul"). sortable({
				update: function(){
					$("#loader").show();
					var result = $(this).sortable("toArray");
			        var data = ""; var inicio = "";
			        for(var x = 0; x < $(result).size(); x++){
			            data+= inicio + "var_" + x + "=" + result[x];
			            inicio = "&";
			        }
			        var id = $(this).parent().attr("id");
			        data+= inicio + "id=" + id;
			        $.ajax({
						url: "../../inc/submenu_posicao.php",
						type: "post",
						dataType: "html",
						data: data,
						cache: false,
						success: function(){
							$("#loader").hide();
						}						
				    });
				}
			});
		  };
		<?php 
		}
		else{
			?>
			function initMenu() {
			  $('#menu ul').hide();
			  //$('#menu ul:first').show();
			  $('#menu li a').click(
				function() {
				  $("#menu li a").css("text-decoration", "");
				  $(this).css("text-decoration","underline");
				  var checkElement = $(this).next();
				  if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
					return false;
					}
				  if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
				    //$(this).prev().css("color","red")
					$('#menu ul:visible').slideUp('fast');
					checkElement.slideDown('fast');
					return false;
					}
				  }
				);
			  }
			<?php
		}
		?>
		$(document).ready(function(){ initMenu(); });

		$(document).ready(function(){
			var menu = parseInt($(".menu ul").css("height"));
			var conteudo = parseInt($(".conteudo").css("height"));

			if(menu > conteudo) {
			    $(".conteudo").css("min-height", menu + 16);
			}
			//Checkboxes
			$("#todos").live({
				"click": function(){
					if($("#todos").attr("checked") == "checked"){
						$("input:checkbox").each(function(){
							if($(this).attr("disabled") == undefined){
								$(this).attr("checked", "checked");
							}
						});			
					}
					else{
						$("input:checked").each(function(){
							if($(this).attr("disabled") == undefined){
								$(this).attr("checked", false);
							}
						});
					}
				}
			});
			//Checkboxes
		});

		/*Menu*/
  		</script>
  		<!-- Carrega modulo de login -->
  		<?php require_once "../../js/modulos/login.js.php";?>
  		<!-- Carrega modulo geral -->
  		<?php require_once "../../js/modulos/geral.js.php"; ?>
  		<!-- Carrega modulo menu -->
  		<?php require_once "../../js/modulos/menu.js.php"; ?>
  		<!-- Carrega modulo funcoes -->
  		<?php //require_once "../../js/modulos/funcoes.js.php"; ?>
  		<!-- Carrega modulo menus -->
  		<?php require_once "../../js/modulos/menus.js.php"; ?>
  		<!-- Carrega modulo submenus -->
  		<?php require_once "../../js/modulos/submenus.js.php"; ?>
  		<!-- Carrega modulo usuarios -->
  		<?php require_once "../../js/modulos/usuarios.js.php"; ?>
  		<!-- Carrega modulo permissoes -->
  		<?php require_once "../../js/modulos/permissoes.js.php"; ?>
  		<!-- Carrega modulo faq -->
  		<?php require_once "../../js/modulos/faq.js.php"; ?>
  		<!-- Carrega modulo configuracoes avancadas -->
  		<?php require_once "../../js/modulos/conf_avancadas.js.php"; ?>
  		<!-- Carrega modulo perguntas -->
  		<?php require_once "../../js/modulos/perguntas.js.php"; ?>
  		<!-- Carrega modulo mudar senha -->
  		<?php require_once "../../js/modulos/mudar_senha.js.php"; ?>
  		<!-- Carrega modulo home -->
  		<?php require_once "../../js/modulos/home.js.php"; ?>
			<!-- Carrega modulo tutoriais -->
  		<?php require_once "../../js/modulos/tutoriais.js.php"; ?>
  		<!-- Carrega modulo tutoriais categoria -->
  		<?php require_once "../../js/modulos/tutoriais_categoria.js.php"; ?>

  		<!-- Carrega modulo noticias -->
  		<?php require_once "../../js/modulos/noticias.js.php"; ?>
  		<!-- Carrega modulo parceiros -->
  		<?php require_once "../../js/modulos/parceiros.js.php"; ?>
  		<!-- Carrega modulo conceitual -->
  		<?php require_once "../../js/modulos/conceitual.js.php"; ?>
  		<!-- Carrega modulo barra home -->
  		<?php require_once "../../js/modulos/barra_home.js.php"; ?>
  		<!-- Carrega modulo dicas -->
  		<?php require_once "../../js/modulos/dicas.js.php"; ?>
  		<!-- Carrega modulo ouvintes -->
  		<?php require_once "../../js/modulos/ouvintes.js.php"; ?>
  		<!-- Carrega modulo qualidade -->
  		<?php require_once "../../js/modulos/qualidade.js.php"; ?>
  		<!-- Carrega modulo streaming -->
  		<?php require_once "../../js/modulos/streaming.js.php"; ?>
  		<!-- Carrega modulo faq_site -->
  		<?php require_once "../../js/modulos/faq_site.js.php"; ?>
  		<!-- Carrega modulo galeria -->
  		<?php require_once "../../js/modulos/galeria.js.php"; ?>
  		<!-- Carrega modulo planos -->
  		<?php require_once "../../js/modulos/planos.js.php"; ?>
  		<!-- Carrega modulo trabalhos -->
  		<?php require_once "../../js/modulos/trabalhos.js.php"; ?>
  		<!-- Carrega modulo videos -->
  		<?php require_once "../../js/modulos/videos.js.php"; ?>
  	
  			