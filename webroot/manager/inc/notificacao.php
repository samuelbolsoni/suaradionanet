<?
$mensagem_qsera_enviada = "
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    <title>W8S</title>
</head>

<body topmargin=\"0\" style=\"margin-top:0px\">
	<div align=\"center\">
    	<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"margin:0;\">
        	<tbody>
            	<tr valign=\"top\">
                	<td><img src=\"$url_site/web/webroot/site/img/lateral_img_esq.jpg\" style=\"display:block;\" alt=\"Lateral Esquerda\" /></td>
                	<td>
                        <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"margin:0;\">
                            <tbody>
                                <tr style=\"background:#22272F;\">
                                    <td>
                                    	<img src=\"$url_site/web/webroot/uploads/img/logo/$logo_cliente\" height=\"59\" style=\"display:block; margin:10px 0 11px 19px;\" alt=\"Topo Esquerda\" />
                                    	<img src=\"$url_site/web/webroot/site/img/topo_img_esq.jpg\" style=\"display:block;\" alt=\"Topo Esquerda\" />
                                    </td>
                                    <td><img src=\"$url_site/web/webroot/site/img/topo_img_dir.jpg\" style=\"display:block;\" alt=\"Topo Direita\" /></td>
                                </tr>
                                <tr valign=\"top\">
                                    <td colspan=\"2\" height=\"343\">
                                        <p style=\"font-family:'Arial'; font-size:15px; color:#21262E; padding:6px 0 0 22px;\"><strong>Olá $nome_user</strong></p>
                                        <p style=\"font-family:'Arial'; font-size:12px; color:#21262E; line-height:19px; padding:4px 0 0 31px; width: 500px\">
                                            Recentemente você foi cadastrado no gerenciador de conteúdo do site <br />
                                            <a style=\"color:#21262E; font-weight:bold;\" href=\"$url_site\" target=\"_blank\">$url_site</a> seus dados de login estão sendo enviados através deste<br /> 
                                            e-mail.<br />
                                            Nunca revele seus dados de acesso para outras pessoas.
                                        </p>                                    
                                        <img src=\"$url_site/web/webroot/site/img/conteudo_img_sep.jpg\" style=\"margin:6px 0 0 26px\" />
                                        <p style=\"font-family:'Arial'; font-size:12px; color:#21262E; padding:10px 0 0 31px;\">
                                            <strong style=\"font-size:17px; padding:0 10px 0 0;\">Seu Login:</strong> $login_user<br />
                                            <strong style=\"font-size:17px; padding:0 10px 0 0;\">Sua senha:</strong> $nova_senha
                                        </p>
                                        <p style=\"font-family:'Arial'; font-size:14px; color:#21262E; padding:46px 0 0 338px;\"><strong>Atenciosamente Equipe W8S</strong></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td><img src=\"$url_site/web/webroot/site/img/rodape_img_esq.jpg\" style=\"display:block;\" alt=\"Rodapé Esquerda\" /></td>
                                    <td><img src=\"$url_site/web/webroot/site/img/rodape_img_dir.jpg\" style=\"display:block;\" alt=\"Rodapé Direita\" /></td>
                                </tr>
                            </tbody>	                
                        </table>
                    </td>
                	<td><img src=\"$url_site/web/webroot/site/img/lateral_img_dir.jpg\" style=\"display:block;\" alt=\"Lateral Direita\" /></td>
                </tr>
			</tbody>	                
        </table>
	</div>
</body>
</html>

";
?>