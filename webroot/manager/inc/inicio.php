<?php require_once "verifica_login.php"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<base href="<?=$url_base2?>/home/" />
        <?php require_once "../../inc/head.php"; ?>
    </head>
    <body>
    	<div id="loader" style="display: none; width: 60px; height: 60px; background-color: #FFFFFF; position: fixed; top: 50%; left: 50%; z-index: 99999; border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; opacity: 0.7"><img src='../../img/loading.gif' style="margin: 25px 0 0 15px" /></div>
        <div id="sombra_geral" class="sombra_geral"></div>
        <?php require_once "../../inc/ie6.php"; ?>
        <div class="corpo" align="center">
            <div class="topo">
				<?php require_once "../../inc/topo.php"; ?>
            </div>
            <div class="conceitual">
				<?php require_once "../../inc/conceitual.php"; ?>
            </div>
            <div class="interna" align="left">
            	<div class="menu">
					<?php require_once "../../inc/menu.php"; ?>
                </div>
            	<div class="conteudo">
