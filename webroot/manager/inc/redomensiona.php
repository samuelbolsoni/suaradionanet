<?php
function redimensiona_imagem($caminho, $foto, $x, $y, $complemento) {
	$diretorio_foto_noticia = $caminho;
	$imagem                 = $foto;
	$imageSize              = GetImageSize ($diretorio_foto_noticia."/".$imagem);
	$img_w                  = $imageSize[0];
	$img_h                  = $imageSize[1];
							
	if ($img_w >= $img_h )   $tipo_imagem="p"; else $tipo_imagem="r";
	if ($tipo_imagem == "p")  { $largura = $x; $altura = $y;  }
	if ($tipo_imagem == "r")  { $largura = $y; $altura = $x;  }
	$complemento = $complemento;
	if ($imagem <> '')     include ($diretorio_foto_noticia."/gera_thumb2.php"); 
}

// DEFINE A IMAGEM DA QUAL SER� GERADA A MINIATURA
   // Lembrar que essa imagem tem que estar no diret�rio do script...
   // .. nenhum teste ser� feito para saber se ela existe
   $imagem2 = $diretorio_foto_noticia."/".$imagem; // Tipo: JPG

// DEFINIR O NOME DO ARQUIVO PARA O THUMBNAIL
   //$thumbnail = explode('.', $imagem2);
   $thumbnail = explode('.', $imagem); 
   $thumbnail = $complemento.$thumbnail[0].".".$thumbnail[1];

// DEFINIR AS DIMENS�ES PARA O THUMBNAIL
   $x = $largura; // Largura
   $y = $altura; // Altura

// L� A IMAGEM DE ORIGEM
     $img_origem = ImageCreateFromJPEG($imagem2);

// PEGA AS DIMENS�ES DA IMAGEM DE ORIGEM
    $origem_x = imagesx($img_origem); // Largura
    $origem_y = imagesy($img_origem); // Altura

// ESCOLHE A LARGURA MAIOR E, BASEADO NELA, GERA A LARGURA MENOR
    if($origem_x > $origem_y) { // Se a largura for maior que a altura
       $final_x = $x; // A largura ser� a do thumbnail
       $final_y = floor($x * $origem_y / $origem_x); // A altura � calculada
       $f_x = 0; // Colar no x = 0
       $f_y = round(($y / 2) - ($final_y / 2)); // Centralizar a imagem no meio y do thumbnail
    } else { // Se a altura for maior ou igual � largura
       $final_x = floor($y * $origem_x / $origem_y); // Calcula a largura
       $final_y = $y; // A altura ser� a do thumbnail
       $f_x = round(($x / 2) - ($final_x / 2)); // Centraliza a imagem no meio x do thumbnail
       $f_y = 0; // Colar no y = 0
    }

// CRIA A IMAGEM FINAL PARA O THUMBNAIL
    $img_final = ImageCreateTrueColor($x,$y);
	//$white = imagecolorallocate($img_final, 255, 255, 255); //deixa o fundo branco
	imagefill($img_final, 0, 0, 0xFFFFFF);

// COPIA A IMAGEM ORIGINAL PARA DENTRO DO THUMBNAIL
    imagecopyresampled($img_final, $img_origem, $f_x, $f_y, 0, 0, $final_x, $final_y, $origem_x, $origem_y);

// SALVA O THUMBNAIL
    ImageJPEG($img_final, $diretorio_foto_noticia."/".$thumbnail);

// LIBERA A MEM�RIA
    ImageDestroy($img_origem);
    ImageDestroy($img_final);
?>