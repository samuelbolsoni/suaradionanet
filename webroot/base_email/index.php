<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Luxo Belle</title>
</head>

<body>

	<table cellpadding="0" cellspacing="0" border="0" style="margin:0 auto;" width="550">
		<thead>
			<tr>
				<td>
					<img src="http://www.w8s.com.br/labs/luxobelle/web/webroot/base_email/images/img001.jpg" style="display:block;" alt="Luxo Belle" />
				</td>
			</tr>
		</thead>
		<tbody>            
			<tr background="http://www.w8s.com.br/labs/luxobelle/web/webroot/base_email/images/img002.jpg" style="display:block;"width="550">
				<td>
					<font face="Arial, Helvetica, sans-serif" style="font-size:20px; text-indent:25px" color="#000000" >
                    	<p>
                        	Lorem Ipsum
                        </p>
                    </font>
                    <font face="Arial, Helvetica, sans-serif" style="font-size:18px;" color="#000000" >
                        <p style="padding:0 25px 0 25px;" editable="true">
                        	Olá fulano de tal
                        </p>
                    </font>
                    <font face="Arial, Helvetica, sans-serif" style="font-size:14px;" color="#000000" >
                        <p style="padding: 0 25px 0 25px;">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque et mi lectus. Nunc lacinia gravida pretium. Proin lacinia odio facilisis lorem mattis eget sodales libero scelerisque. Donec odio mauris, aliquam non aliquam vel, luctus varius felis. Pellentesque id neque sem, in vehicula nibh. Duis ante justo, viverra non euismod a, malesuada et nulla. Donec consequat feugiat eleifend. Pellentesque faucibus lacus non nunc pellentesque id posuere dui bibendum. Donec eleifend diam nec turpis vestibulum elementum. Curabitur scelerisque ligula at eros tempus facilisis. Phasellus ac justo erat, sed lacinia est. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin vehicula sem eget urna tempor aliquet.
                        </p>
                    </font>
                    <br />
                    <br />
                    <br />
				</td>
			</tr>
		</tbody>
		<tfoot>            
			<tr>
				<td>
					<img src="http://www.w8s.com.br/labs/luxobelle/web/webroot/base_email/images/img003.jpg" style="display:block;" alt="Luxo Belle" />
				</td>
			</tr>
		</tfoot>
	</table>

</body>
</html>