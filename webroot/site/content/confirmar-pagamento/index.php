<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml"
      lang="pt-br">
<?php require_once "../../inc/verificastatus.php"; ?>
<head>
  <base href="<?php echo $url_base;?>/confirmar-pagamento/"/>
  <?php include "../../inc/head.php"; ?>
  <link rel="stylesheet" href="../../css/3rd_party/sexy/sexy.css" />
</head>

<body>
  <div id="wrapper" class="internal payment"> <!-- Wrapper -->

    <div id="main"> <!-- Main -->
      
      <?php include "../../inc/header_pagamento.php" ?>

      <div id="content-wrapper"> <!-- Content-Wrapper -->

        <div class="wrapper"> <!-- Class Wrapper -->
          
          <div id="content" class="clearfix"> <!-- Content -->
            
            <div class="content-bottom clearfix">
              <section class="content dropdown-infos"> <!-- corpo de conteudo -->
                
                <form action="javascript:;" method="post">
                  <div class="form-left">
                    <label class="font01" for="nome">Seu Nome:</label>
                    <div class="bars bleft">
                      <div class="bars bright">
                        <div class="bars bmiddle">
                          <input id="nome" maxlength="" class="required" placeholder="Digite seu nome completo" type="text"/>
                        </div>
                      </div>
                    </div>
                    <label class="font01" for="pagamento">Forma de Pagamento:</label>
                    <div class="bars bleft">
                      <div class="bars bright">
                        <div class="bars bmiddle">
                          <select class="select required" id="pagamento" placeholder="Selecione modo de pagamento">
                            <option value=""></option>
                            <option value="Cartao">Cartão</option>
                            <option value="Boleto">Boleto</option>
                            <option value="Dinheiro">Dinheiro</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <label class="font01" for="radio">Nome da Sua Rádio:</label>
                    <div class="bars bleft">
                      <div class="bars bright">
                        <div class="bars bmiddle">
                          <input id="radio" maxlength="" class="required" placeholder="Qual o nome da sua rádio?" type="text"/>
                        </div>
                      </div>
                    </div>
                  </div>  
                  <div class="form-right">
                    <label class="font01" for="email">Seu E-mail:</label>
                    <div class="bars bleft">
                      <div class="bars bright">
                        <div class="bars bmiddle">
                          <input id="email" maxlength="" class="required" placeholder="Digite seu e-mail" type="text"/>
                        </div>
                      </div>
                    </div>
                    <div class="rightmin-01">
                      <label class="font01" for="pago">Valor Pago:</label>
                      <div class="bars bleft">
                        <div class="bars bright">
                          <div class="bars bmiddle-min">
                            <input id="pago" maxlength="" class="required" placeholder="R$ 00,00" type="text" />
                          </div>
                        </div>
                      </div>
                      <label class="font01" for="fone">Telefone:</label>
                      <div class="bars bleft">
                        <div class="bars bright">
                          <div class="bars bmiddle-min">
                            <input id="fone" maxlength="14" class="required" placeholder="(xx) 0000.00000" type="text"/>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="rightmin-02">
                      <label class="font01" for="data">Data e hora do Pag.</label>
                      <div class="bars bleft">
                        <div class="bars bright">
                          <div class="bars bmiddle-min">
                            <input id="data" maxlength="19" class="required" placeholder="dd/mm/aaaa - 00h00m" type="text"/>
                          </div>
                        </div>
                      </div>
                      <label class="font01" for="cel">Celular:</label>
                      <div class="bars bleft">
                        <div class="bars bright">
                          <div class="bars bmiddle-min">
                            <input id="cel" maxlength="14" placeholder="(xx) 0000.00000" type="text" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <label class="font01" for="mensagem">Coloque todos os dados impressos no comprovante de depósito ou transferência:</label>
                  <div class="bars tleft">
                    <div class="bars tright">
                      <div class="bars tmiddle">
                        <textarea id="mensagem" class="required" placeholder="Coloque todos os dados impressos no comprovante de depósito ou transferência."></textarea>
                      </div>
                    </div>
                  </div>
                  
                  <div class="file-wrap">
                  <label class="font01" for="comprovante">Anexar comprovante:</label>
                    <div class="bars bleft">
                      <div class="bars bright">
                        <div class="bars bmiddle comprovante">
                          <input id="comprovante" type="text" placeholder="Selecione o arquivo..."/>
                          <span class="icon-file"></span>
                        </div>
                      </div>
                    </div>
                  <input id="file-comprovante" class="input-file" type="file" />
                  </div>
                  
                  <div class="captcha-wrap">
                  
                    <label class="font01" for="captc">Captcha:</label>
                    <div class="bars bleft">
                      <div class="bars bright">
                        <div class="bars bmiddle">
                          <input id="captc" class="required" type="text"/>
                        </div>
                      </div>
                    </div>
                  
                    <figure class="captcha-img">
                      <a style="float: left; margin: 18px 10px 0px" href="javascript:;" onclick="document.getElementById('captcha').src = '../../inc/securimage/securimage_show.php?' + Math.random(); return false" title="Alterar o captcha">
                        <img src="../../img/reload-radios.png" width="15px" /><!--Carregar novo-->
                      </a>
                      <img id="captcha" src="../../inc/securimage/securimage_show.php?<?php echo rand(0, 1000); ?>" style="float: left; margin-top: 5px; position: relative; border: 1px solid <?php echo $RES_radio['tema']; ?>" alt="CAPTCHA Image" /> 
                    </figure>
                  
                  </div>
                  
                  <div class="send-area">
                    <div class="warning-msg" style="display: none;" ><!-- classe para msg de erro: 'incorrect' -->
                      <p class="font-msg"></p>
                    </div>
                    <input class="btn-send" type="submit" value="" title="Enviar" id="enviar" />
                  </div>
                </form>
                
                <a href="javascript:(window.history.go(-1));" class="bt-back" title="Voltar">
                  <span>Voltar</span>
                </a>
                <div class="loader">
                  <img src="../../img/loader.gif" alt="Loader" title="Loader" />
                </div>
              </section> <!-- corpo de conteudo -->
            </div>
            
          </div> <!-- Content -->

        </div> <!-- END: Class Wrapper -->

      </div> <!-- END: Content-Wrapper -->

    </div> <!-- END: Main -->

  </div><!-- END: Wrapper -->

  <?php
    include "../../inc/footer.php";
    include "../../inc/scripts.php";
    include "../../inc/scripts-internas.shtml";
  ?>
  <script type="text/javascript" src="../../js/pagamento.js"></script>
  <script type="text/javascript" src="../../js/jquery.maskedinput-1.2.2.js" ></script>
  <script type="text/javascript" src="../../js/jquery.price_format.js"></script>
  <script type="text/javascript" src="../../js/jquery.ui.widget.js"></script>
  <script type="text/javascript" src="../../js/jquery.fileupload.js"></script>
  <script>
  $(function(){
  	$('.input-file').change(function() {
        $('#comprovante').val($('.input-file').val());
      });
      
  	$("#file-comprovante").fileupload({
    	 dropZone: null,
         dataType: 'html',
         url: 'uploadcomprovante.php',
         send: function(e, data){
        	$(".loader").show();
        },
        done: function (e, data) {
        	$(".loader").hide();
        	var result = data.result;
        	if(result == false){
        		alert("Não foi possível fazer o upload.");
        	}
        }
    });
  });

  </script>
</body>
</html>