<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml"
      lang="pt-br">
  <?php
  require_once "../../inc/verificastatus.php";
 ?>
<head>
    <base href="<?php echo $url_base; ?>/cadastro/"/>
    <?php
    include "../../inc/head.php";
    ?>
    <link rel="stylesheet" href="../../css/3rd_party/sexy/sexy.css" />
    <link rel="stylesheet" href="../../css/3rd_party/colorbox/colorbox-newmodal.css" />
    <script src="../../js/cadastro.js?<?php echo rand(0, 999); ?>"></script>
  </head>

  <body>
    <div id="wrapper" class="internal cadastro">
      <!-- Wrapper -->

      <div id="main">
        <!-- Main -->

        <?php
        include "../../inc/header_cadastro.php";
        ?>

        <div id="content-wrapper">
          <!-- Content-Wrapper -->

          <div class="wrapper">
            <!-- Class Wrapper -->

            <div id="content" class="clearfix">
              <!-- Content -->

              <div class="content-bottom clearfix">
                <section class="content">
                  <!-- corpo de conteudo -->

                  <?php
                  function addzeros($valorplano) {
                    if (strpos($valorplano, '.')) {
                      $valorplano = $valorplano . "0";
                      $valorplano = str_replace('.', ',', $valorplano);
                    } else {
                      if ($valorplano != NULL) {
                        $valorplano = $valorplano . ",00";
                      }
                    }
                    return $valorplano;
                  }

                  if (isset($_GET['id'])) {
                    $idvalor = $_GET['id'];
                    $query = mysql_query("select * from `site_planos` where id =" . $idvalor);
                    $rs = mysql_fetch_array($query);
                    $valor = $rs['valor'];
                    if ($valor >= 39.6) {
                      $gratuitoadm = "- Grátis no plano selecionado<sup>*</sup>";
                      $gratuitodj = "- Grátis no plano selecionado<sup>*</sup>";
                    } else {
                      if ($valor < 33.5) {
                        $gratuitoadm = "";
                        $gratuitodj = "";
                      } else {
                          $gratuitoadm = "- Grátis no plano selecionado<sup>*</sup>";
                      }
                    }
                    $valor = addzeros($valor);
                    $streaming = $rs['streaming'];
                    $valqualidade = $rs['qualidade'];
                    $valouvintes = $rs['ouvintes'];
                  } else {
                    $valor = "";
                    $gratuitoadm = "- Grátis no plano selecionado<sup>*</sup>";
                    $gratuitodj = "- Grátis no plano selecionado<sup>*</sup>";
                    $streaming = 4;
                    $valqualidade = 13;
                    $valouvintes = 20;
                  }
                  $first = 1;
                  ?>
                  <form action="javascript:;" method="post" id="formCadastro">
                    <div class="form-left">
                      <div class="w162">
                        <label class="font01" for="tipo-streaming">Tipo Selecionado:</label>
                        <div class="bars bleft w162 resetMarginRight">
                          <div class="bars bright">
                            <div class="bars bmiddle" id="streaming-grid">
                              <select class="select" id="tipo-streaming" placeholder="Tipo de Streaming" onchange="valor(); planos(2);">
                                
                                <?php
                                $query = mysql_query("select * from site_streaming order by titulo asc");
                                while ($rs = mysql_fetch_array($query)) {
                                $idstreaming = $rs['id'];
                                $titulostreaming = $rs['titulo'];
                                if ($idstreaming == $streaming) {
                                $streamselected = "selected='selected'";
                                } else {
                                $streamselected = "";
                                }
                                echo "<option " . $streamselected . " value=" . $idstreaming . ">" . $titulostreaming . "</option>";
                                }
                                ?>  
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="w112">
                      <label class="font01" for="ouvintes">Ouvintes:</label>
                      <div class="bars bleft w112 resetMarginRight">
                        <div class="bars bright">
                          <div class="bars bmiddle" id="ouvintes-grid">
                            <select class="select" id="ouvintes" placeholder="0" onchange="valor(); planos();">
                              <?php $query = mysql_query("SELECT p.ouvintes AS id, o.titulo FROM site_planos AS p, site_ouvintes AS o WHERE p.streaming = $streaming AND p.ouvintes = o.id GROUP BY o.titulo ORDER BY o.titulo");
                              while ($rs = mysql_fetch_array($query)) {
                                $idouvintes = $rs['id'];
                                $tituloouvintes = $rs['titulo'];
                                if ($idouvintes == $valouvintes) {
                                  $ouvintesselected = "selected='selected'";
                                } else {
                                  $ouvintesselected = "";
                                }
                                echo "<option " . $ouvintesselected . " value=" . $idouvintes . ">" . $tituloouvintes . "</option>";
                              }
                                ?>  
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="w112 resetMarginRight">
                      <label class="font01" for="qualidade">Qualidade:</label>
                      <div class="bars bleft w112 resetMarginRight" >
                        <div class="bars bright">
                          <div class="bars bmiddle" id="qualidade-grid">
                            <select class="select" id="qualidade" placeholder="0" onchange="valor();">
                              <?php
                              $query = mysql_query("SELECT a.id as id, a.dt_cad as dt_cad , b.titulo as ouvintes, b.id as id_ouvintes, c.id as id_qualidade,
 c.titulo as qualidade, d.titulo as streaming FROM site_planos as a, site_ouvintes as b, site_qualidade as c, site_streaming as d 
 WHERE b.id = a.ouvintes AND c.id = a.qualidade AND d.id = a.streaming AND a.streaming = $streaming and a.ouvintes = $valouvintes order by c.id");
                              while ($rs = mysql_fetch_array($query)) {
                                $idqualidade = $rs['id_qualidade'];
                                $tituloqualidade = $rs['qualidade'];
                                if ($idqualidade == $valqualidade) {
                                  $qualidadeselected = "selected='selected'";
                                } else {
                                  $qualidadeselected = "";
                                }
                                echo "<option " . $qualidadeselected . " value=" . $idqualidade . ">" . $tituloqualidade . "</option>";
                              }
                                ?>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>

                    <label class="font01" for="nome">*Seu Nome:</label>
                    <div class="bars bleft">
                      <div class="bars bright">
                        <div class="bars bmiddle">
                          <input id="nome" maxlength="" placeholder="Digite seu nome completo" type="text" class="required" tabindex="1"/>
                        </div>
                      </div>
                    </div>
                    <label class="font01" for="email">*Seu Email:</label>
                    <div class="bars bleft">
                      <div class="bars bright">
                        <div class="bars bmiddle">
                          <input id="email" maxlength="" placeholder="Digite seu email" type="text" class="required" tabindex="3"/>
                        </div>
                      </div>
                    </div>
                    <label class="font01" for="estado">*Estado:</label>
                    <div class="bars bleft">
                      <div class="bars bright">
                        <div class="bars bmiddle">
                          <select class="select" id="estado" placeholder="Selecione o seu Estado" >
                            <option value="" ></option>
                            <option value="AC">Acre</option>
                            <option value="AL">Alagoas</option>
                            <option value="AP">Amapá</option>
                            <option value="AM">Amazonas</option>
                            <option value="BA">Bahia</option>
                            <option value="CE">Ceará</option>
                            <option value="DF">Distrito Federal</option>
                            <option value="GO">Goiás</option>
                            <option value="ES">Espírito Santo</option>
                            <option value="MA">Maranhão</option>
                            <option value="MT">Mato Grosso</option>
                            <option value="MS">Mato Grosso do Sul</option>
                            <option value="MG">Minas Gerais</option>
                            <option value="PA">Pará</option>
                            <option value="PB">Paraíba</option>
                            <option value="PR">Paraná</option>
                            <option value="PE">Pernambuco</option>
                            <option value="PI">Piauí</option> 
                            <option value="RJ">Rio de Janeiro</option>                    
                            <option value="RN">Rio Grande do Norte</option>
                            <option value="RS">Rio Grande do Sul</option>
                            <option value="RO">Rondônia</option>
                            <option value="RR">Roraima</option>
                            <option value="SP">São Paulo</option>
                            <option value="SC">Santa Catarina</option>
                            <option value="SE">Sergipe</option>
                            <option value="TO">Tocantins</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <label class="font01" for="endereco">*Endereço:</label>
                    <div class="bars bleft">
                      <div class="bars bright">
                        <div class="bars bmiddle">
                          <input id="endereco" maxlength="" placeholder="Digite seu endereço" type="text"/ class="required" tabindex="8">
                        </div>
                      </div>
                    </div>
                    <label class="font01" for="seu_site">*Site:</label>
                    <div class="bars bleft">
                      <div class="bars bright">
                        <div class="bars bmiddle">
                          <input id="seu_site" maxlength="" placeholder="Digite seu site" type="text" class="required" tabindex="11"/>
                        </div>
                      </div>
                    </div>
                    <label for="incluir-adm" class="checkbox font01" >
                      <input type="checkbox" id="incluir-adm" onclick="checkAdm();" tabindex="14" /> Site Administrável <span class="inc-adm" style="display:none;"> <?php echo $gratuitoadm; ?></span>
                    </label>
                    <label for="incluir-dj" class="checkbox font01" style="width: 440px;">
                      <input type="checkbox" id="incluir-dj" onclick="checkDj();" tabindex="15" /> Auto DJ <span class="inc-dj" style="display:none;"> <?php echo $gratuitodj; ?></span>
                    </label>
                    <input id="valorfinal" value="<?=$valor?>" type="hidden" name="valorfinal" placeholder="valorfinal"/>
                  </div>  
                  <div class="form-right">
                    <div id="price" class="font01">
                      <?php
                      if ($idvalor != 'index.php') {
                        echo "R$";
                      } else { 
                        echo "";
                      }
                      if ($idvalor != 'index.php') {
                        echo "<span class=\"price\">".$valor."</span>";
                      } else { 
                        echo "<span class=\"consultar\">Consultar</span>";
                        echo "<span class=\"msg-consultar\">Envie nos um e-mail através deste formulário abaixo com suas dúvidas, será um prazer atende-lo.</span>";
                      }
                      ?>
                      <span class="month"> <?php
                      if ($idvalor != 'index.php')
                        echo "/MÊS";
                      else { echo "";
                      }
                       ?> </span>
                    </div>
                    <label class="font01" for="empresa">*Empresa: (Ou nome da sua Rádio)</label>
                    <div class="bars bleft">
                      <div class="bars bright">
                        <div class="bars bmiddle">
                          <input id="empresa" maxlength="" placeholder="Digite o nome da sua empresa ou da sua rádio" type="text" class="required" tabindex="2"/>
                        </div>
                      </div>
                    </div>

                     <label class="font01" for="cpf_cnpj">*CPF ou CNPJ:</label>
                    <div class="bars bleft">
                      <div class="bars bright">
                        <div class="bars bmiddle">
                          <input id="cpf_cnpj" maxlength="" placeholder="000.000.000-00" type="text" class="required" tabindex="4" />
                        </div>
                      </div>
                    </div>

                    <div class="w192">
                      <label class="font01" for="cidade">*Cidade:</label>
                      <div class="bars bleft w192">
                        <div class="bars bright">
                          <div class="bars bmiddle">
                            <input id="cidade" maxlength="" placeholder="Qual sua cidade?" type="text" class="required" tabindex="6" />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="w192 resetMarginRight">
                      <label class="font01" for="cep">*CEP:</label>
                      <div class="bars bleft w192 resetMarginRight">
                        <div class="bars bright">
                          <div class="bars bmiddle">
                            <input id="cep" maxlength="" placeholder="00000-000" type="text" class="required" tabindex="7" />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="w192">
                      <label class="font01" for="bairro">*Bairro:</label>
                      <div class="bars bleft w192">
                        <div class="bars bright">
                          <div class="bars bmiddle">
                            <input id="bairro" maxlength="" placeholder="Qual seu bairro?" class="required" type="text" tabindex="9"/>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="w192 resetMarginRight">
                      <label class="font01" for="complemento">Complemento:</label>
                      <div class="bars bleft w192 resetMarginRight">
                        <div class="bars bright">
                          <div class="bars bmiddle">
                            <input id="complemento" maxlength="" placeholder="ex: Bloco 3 - 307" type="text" tabindex="10"/>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="w192">
                      <label class="font01" for="telefone">*Telefone:</label>
                      <div class="bars bleft w192">
                        <div class="bars bright">
                          <div class="bars bmiddle">
                            <input id="telefone" maxlength="" placeholder="(xx) 0000.00000" type="text" class="required" tabindex="12"/>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="w192 resetMarginRight">
                      <label class="font01" for="celular">Celular:</label>
                      <div class="bars bleft w192 resetMarginRight">
                        <div class="bars bright">
                          <div class="bars bmiddle">
                            <input id="celular" name ="celular" maxlength="" placeholder="(xx) 0000.00000" type="text" tabindex="13"/>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                  <label class="font01" for="mensagem">Mensagem:</label>
                  <div class="bars tleft">
                    <div class="bars tright">
                      <div class="bars tmiddle">
                        <textarea id="mensagem" placeholder="Digite sua mensagem aqui..." tabindex="16"></textarea>
                      </div>
                    </div>
                  </div>

                  <div class="license-info">
                    <p class="small-info">Os campos com * são obrigatórios. CNPJ obrigatório para empresas.</p>
                    <!-- <label for="license" class="checkbox font01">
                      <input type="checkbox" id="license" class="required"> Li e Aceito os <a class="modal-termos" id="termos" href="javascript:;">Termos de Uso</a>.
                    </label> -->
                  </div>
                  
                 <div class="captcha-wrap">
                  
                    <label class="font01" for="captc">Captcha:</label>
                    <div class="bars bleft">
                      <div class="bars bright">
                        <div class="bars bmiddle">
                          <input id="captc" type="text" class="required" tabindex="17"/>
                        </div>
                      </div>
                    </div>
                  
                    <figure class="captcha-img">
                      <a style="float: left; margin: 18px 10px 0px" href="javascript:;" onclick="document.getElementById('captcha').src = '../../inc/securimage/securimage_show.php?' + Math.random(); return false" title="Alterar o captcha">
                        <img src="../../img/reload-radios.png" width="15px" /><!--Carregar novo-->
                      </a>
                      <img id="captcha" src="../../inc/securimage/securimage_show.php?<?php echo rand(0, 1000); ?>"

 style="float: left; margin-top: 5px; position: relative; border: 1px solid                                                                 <?php echo $RES_radio['tema']; ?>"
                                alt="CAPTCHA Image" />
                                </figure>

                            </div>

                            <div class="send-area">
                              <div class="warning-msg" style="display: none;">
                                <!-- 'correct' -->
                                <p class="font-msg"></p>
                              </div>
                              <input class="btn-send" type="submit" value="" title="Enviar" id="enviar" onclick="cadastrar();" />
                            </div>
                  </form>

                  <a href="javascript:(window.history.go(-1));" class="bt-back" title="Voltar"> <span>Voltar</span> </a>

                </section>
                <!-- corpo de conteudo -->
              </div>

            </div>
            <!-- Content -->

          </div>
          <!-- END: Class Wrapper -->

        </div>
        <!-- END: Content-Wrapper -->

      </div>
      <!-- END: Main -->

    </div><!-- END: Wrapper -->
                
    <?php
    include "../../inc/footer.php";
    include "../../inc/scripts.php";
    include "../../inc/scripts-internas.shtml";
    ?>
    
    <script src="../../js/3rd_party/jquery.colorbox-min.js"></script>
    <script>
      $('#termos').click(function(e) {
        e.preventDefault();
        $.colorbox({
          inline: true,
          href: '#modal-box',
          initialWidth : 50,
          initialHeight : 50,
          maxWidth: '85%',
          maxHeight: '90%',
          previous : "",
          next : "",
          current : ""
        });
      });
    </script>
    <div id="invisible">  
      <div id="modal-box">
        <div id="modal-header">
          <h1>Termos de Uso</h1>
        </div>
        <div id="division">
          <div class="text-box">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            <br /><br />
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            <br /><br />
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            <br /><br />
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            <br /><br />
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            <br /><br />
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            <br /><br />
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </div>
        </div>
      </div>
    </div>

  </body>
</html>