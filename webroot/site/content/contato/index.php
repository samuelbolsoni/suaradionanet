﻿	<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml"      xmlns:og="http://ogp.me/ns#"      xmlns:fb="https://www.facebook.com/2008/fbml"      lang="pt-br">
	<?php require_once "../../inc/verificastatus.php"; ?>
	<head>  
		<base href="<?php echo $url_base;?>/contato/"/> 
	<?php include "../../inc/head.php"; ?>
		<link rel="stylesheet" href="../../css/3rd_party/sexy/sexy.css" />
	</head>

	<body>
	<div id="wrapper" class="internal contact"> <!-- Wrapper -->    
		<div id="main"> <!-- Main -->            
		<?php include "../../inc/header_contato.php"; ?>      
			<div id="content-wrapper"> <!-- Content-Wrapper -->        
				<div class="wrapper"> <!-- Class Wrapper -->                    
					<div id="content" class="clearfix"> <!-- Content -->                        
						<div class="content-bottom clearfix">              
							<section class="content dropdown-infos"> <!-- corpo de conteudo -->                                
								<form action="javascript:;" method="post">                  
									<div class="form-left">                    
										<label class="font01" for="nome">Seu Nome:</label>                    
											<div class="bars bleft">                      
												<div class="bars bright">                        
													<div class="bars bmiddle">                          
														<input id="nome" maxlength="" placeholder="Digite seu nome completo" class="required" type="text"/>                        
													</div>                      
												</div>                    
											</div>                    
											<label class="font01" for="pagamento">Estado:</label>                    
											<div class="bars bleft">                      
												<div class="bars bright">                        
													<div class="bars bmiddle">                          
														<select class="select required" id="estado" placeholder="Selecione o seu Estado">
															<option value=""></option>
															 <option value="AC">Acre</option>
															 <option value="AL">Alagoas</option>
															 <option value="AP">Amapá</option>
															 <option value="AM">Amazonas</option>
															 <option value="BA">Bahia</option>
															 <option value="CE">Ceará</option>
															 <option value="DF">Distrito Federal</option>
															 <option value="GO">Goiás</option>
															 <option value="ES">Espírito Santo</option>
															 <option value="MA">Maranhão</option>
															 <option value="MT">Mato Grosso</option>
															 <option value="MS">Mato Grosso do Sul</option>
															 <option value="MG">Minas Gerais</option>
															 <option value="PA">Pará</option>
															 <option value="PB">Paraíba</option>
															 <option value="PR">Paraná</option>
															 <option value="PE">Pernambuco</option>
															 <option value="PI">Piauí</option>
															 <option value="RJ">Rio de Janeiro</option>
															 <option value="RN">Rio Grande do Norte</option>
															 <option value="RS">Rio Grande do Sul</option>
															 <option value="RO">Rondônia</option>
															 <option value="RR">Roraima</option>
															 <option value="SP">São Paulo</option>
															 <option value="SC">Santa Catarina</option>
															 <option value="SE">Sergipe</option>
															 <option value="TO">Tocantins</option>
														</select>
													</div>
												</div>
											</div>
											<label class="font01" for="radio">Nome da Sua Rádio:</label>
											<div class="bars bleft">
												<div class="bars bright">
													<div class="bars bmiddle">
														<input id="radio" maxlength="" class="required" placeholder="Qual o nome da sua rádio?" type="text"/>
													</div>
												</div>
											</div>
										</div>
										<div class="form-right">
											<label class="font01" for="email">Seu E-mail:</label>
											<div class="bars bleft">
												<div class="bars bright">
													<div class="bars bmiddle">
														<input id="email" maxlength="" class="required" placeholder="Digite seu e-mail" type="text"/>
													</div>
												</div>
											</div>
											<label class="font01" for="cidade">Cidade:</label>
											<div class="bars bleft">
												<div class="bars bright">
													<div class="bars bmiddle">
														<input id="cidade" maxlength="" class="required" placeholder="Digite o nome da sua cidade" type="text"/>
													</div>
												</div>
											</div>
											<label class="font01" for="cont_assunto">Assunto:</label>
											<div class="bars bleft">
												<div class="bars bright">
													<div class="bars bmiddle">
														<select class="select required" id="assunto" name="assunto" placeholder="Selecione o Assunto">
															<option value=""></option>
															 <option value="financeiro@suaradionanet.net">Financeiro/Vendas</option>
															 <option value="suporte@suaradionanet.net">Central de Suporte</option>
															 <option value="falecom@suaradionanet.net">Outro</option>
														</select>
														<!--<input id="cont_assunto" maxlength="" class="required" placeholder="Sobre o que deseja falar?" type="text"/>-->
													</div>
												</div>
											</div>
										</div>
										<label class="font01" for="mensagem">Mensagem:</label>
										<div class="bars tleft">
											<div class="bars tright">
												<div class="bars tmiddle">
													<textarea id="mensagem" class="required" placeholder="Digite sua mensagem aqui..."></textarea>
												</div>
											</div>
										</div>
										<div class="captcha-wrap">
											<label class="font01" for="captc">Captcha:</label>
											<div class="bars bleft">
												<div class="bars bright">
													<div class="bars bmiddle">
														<input id="captc" class="required" type="text"/>
													</div>
												</div>
											</div>
											<figure class="captcha-img">
												<a style="float: left; margin: 18px 10px 0px" href="javascript:;" onclick="document.getElementById('captcha').src = '../../inc/securimage/securimage_show.php?' + Math.random(); return false" title="Alterar o captcha">
													<img src="../../img/reload-radios.png" width="15px" /><!--Carregar novo-->
												</a>
												<img id="captcha" src="../../inc/securimage/securimage_show.php?<?php echo rand(0, 1000); ?>" style="float: left; margin-top: 5px; position: relative; border: 1px solid <?php echo $RES_radio['tema']; ?>" alt="CAPTCHA Image" />
											</figure>
										</div>
										<div class="send-area">
											<div class="warning-msg" style="display: none;"><!-- 'correct' -->
												<p class="font-msg"></p>
											</div>
											<input class="btn-send" type="submit" value="" title="Enviar" id="enviar" />
										</div>
									</form>
									<a href="javascript:(window.history.go(-1));" class="bt-back" title="Voltar"><span>Voltar</span></a>
								</section> <!-- corpo de conteudo -->
							</div>
						</div> <!-- Content -->
					</div> <!-- END: Class Wrapper -->
				</div> <!-- END: Content-Wrapper -->
			</div> <!-- END: Main -->
		</div><!-- END: Wrapper -->

		<?php    
		include "../../inc/footer.php";
		include "../../inc/scripts.php";
		include "../../inc/scripts-internas.shtml";  
		?>  

		<script type="text/javascript" src="../../js/contato.js"></script>
	</body>
	</html>