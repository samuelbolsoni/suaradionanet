<!DOCTYPE html>
<html lang="pt-br"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml">
<?php require_once "../../inc/verificastatus.php"; ?>
<head>
  <base href="<?php echo $url_base;?>/faq/"/>
  <?php include "../../inc/head.php"; ?>
  <script src="../../js/faq.js"></script>
  <style type="text/css">
    .search-wrap {
      margin: 2px 0 0;
    }
    .infos-wrap {
      float: left;
      display: none;
      width: 100%;
    }
    #error-msg-wrap {
      float: left;
      display: none;
      width: 500px;
    }
    #error-msg-wrap > .font-msg {
      color: #03652C;
      margin-top: 1.2em;
    }
    #error-msg-wrap.incorrect > .font-msg {
      color: #AC2525;
    }
  </style>
</head>

<body>
  <div id="wrapper" class="internal faq"> <!-- Wrapper -->

    <div id="main"> <!-- Main -->
      
      <!--#include virtual="inc/header_faq.shtml" -->
      <?php include "../../inc/header_faq.php"; ?>

      <div id="content-wrapper"> <!-- Content-Wrapper -->

        <div class="wrapper"> <!-- Class Wrapper -->
          
          <div id="content" class="clearfix"> <!-- Content -->
            
            <div class="content-bottom clearfix">
              <section class="content dropdown-infos"> <!-- corpo de conteudo -->
                <header class="question-wrapper">
                  <form action="javascript:;" method="post">
                    <div class="textarea-left">
                      <div class="textarea-right">
                        <div class="textarea">
                          <textarea id="question" name="question" placeholder="Caso possua uma dúvida que não esteja listada abaixo, pedimos que digite sua pergunta aqui..."></textarea>
                        </div>
                      </div>
                    </div>
                    <button type="submit" onclick="faq();" class="bt-ask">
                      <span>Perguntar</span>
                    </button>
                    <br class="cAll"/>
                    <br class="cAll"/>
                    <div class="infos-wrap">
                      <div id="error-msg-wrap">
                        <p class="font-msg">
                          
                        </p>
                      </div>
                      <div class="captcha-wrap">
                        <label class="font01" for="captc">Captcha:</label>
                        <div class="bars bleft">
                          <div class="bars bright">
                            <div class="bars bmiddle">
                              <input id="captc" type="text"/>
                            </div>
                          </div>
                        </div>
                      
                        <figure class="captcha-img">
                          <a style="float: left; margin: 18px 10px 0px" href="javascript:;" onclick="document.getElementById('captcha').src = '../../inc/securimage/securimage_show.php?' + Math.random(); return false" title="Alterar o captcha">
                            <img src="../../img/reload-radios.png" width="15px" /><!--Carregar novo-->
                          </a>
                          <img id="captcha" src="../../inc/securimage/securimage_show.php?<?php echo rand(0, 1000); ?>" style="float: left; margin-top: 5px; position: relative; border: 1px solid <?php echo $RES_radio['tema']; ?>" alt="CAPTCHA Image" /> 
                        </figure>
                      
                      </div>

                    </div>
                  </form>
                  <form action="javascript:;" class="search-wrap" id="" method="post">
                     <div class="bars bleft">
                        <div class="bars bright">
                          <div class="bars bmiddle">
                            <input class="input-searcharea" id="faqbusca" maxlength="" placeholder="O que você procura?" type="text" onchange="show();" />
                          </div>
                        </div>
                      </div>
                    <button type="submit" class="bt-search" onclick="faqbuscar();">
                      <span>Buscar</span>
                    </button>
                  </form>  
                </header>
                <div id="section">
                	<?php
                	$open = 0;
                	$query_faq = mysql_query("SELECT * FROM site_faq_site WHERE ativo = 0 ORDER BY posicao LIMIT 0, 10");
                	while($result = mysql_fetch_array($query_faq)){
                		$dt_cad = $result['dt_cad'];
										$date = date("Y-m-d");
										$data = strtotime($date) - strtotime($dt_cad);
										$data = $data / 86400;
										$data = round($data);
                		$titulo = $result['titulo'];
										$texto = $result['texto'];
                		?>
                		<article class="dropdow-container">
		                  <div class="bg-top"></div>
		                  <header>
		                    <a href="javascript:;" <?php if($open == 0){?> class="open" <?php } ?>>
		                      <h1 class="font01">
                            <span class="bt-control plus"></span>
		                        <?php echo $titulo; ?>
		                      </h1>
		                    </a>
		                  </header>
		                  <div class="text-container">
		                    <p class="data">Publicado: <?php if($data > 0){ echo $data; ?> dia(s) atrás<?php }else { echo " hoje";} ?></p>
		                    <div class="text">
		                      <?php echo $texto; ?>
		                    </div>
		                  </div>
		                  <footer>
		                  </footer>
		                </article>
                		<?php
                		$open++;
                	}
                	?>
                </div>
                <input type="hidden" id="num" value="<?php echo $open; ?>"/>
                <a href="javascript:(window.history.go(-1));" class="bt-back" title="Voltar">
                  <span>Voltar</span>
                </a>
                
                <div class="loader">
                 <img src="../../img/loader.gif" alt="Loader" title="Loader" />
               </div>
                
              </section> <!-- corpo de conteudo -->
            </div>
            
          </div> <!-- Content -->

        </div> <!-- END: Class Wrapper -->

      </div> <!-- END: Content-Wrapper -->

    </div> <!-- END: Main -->

  </div><!-- END: Wrapper -->
  <?php
	  include "../../inc/footer.php";
		include "../../inc/scripts.php";
		include "../../inc/scripts-internas.shtml";
	?>
	<script src="../../js/paginador.js"></script>
	<input id='pagina' value="0" type="hidden" />
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('#question').focusin(function() {
        if(!$('.infos-wrap').hasClass('open')) {
          $('.infos-wrap').addClass('open');
          $('.infos-wrap').slideToggle();
        }
      });
    });
  </script>

</body>
</html>