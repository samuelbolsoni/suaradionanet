<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml"
      lang="pt-br">
<?php require_once "../../inc/verificastatus.php"; ?>
<head>
  <base href="<?php echo $url_base;?>/popup/"/>
  <?php include "../../inc/head.php"; ?>
  <style type="text/css">
    body, html {
      background: #066CAA;
      min-width: 0;
    }
    #main-popup {
      margin: 10px auto;
      max-width: 455px;
      min-width: 202px;
    }
    #main-popup .contain-player {
      width: 202px;
      margin: 0 auto;
    }
    #main-popup .contain-player #radio-player {
      margin: 0;
    }
  </style>
</head>
<body>
  <div id="main-popup" class="clearfix">
      <?php 
        $typeStreaming = $_GET['type'];
        switch ($typeStreaming) {
          case 'aacplus':
            $playerURL = "http://suaradio2.dyndns.ws:10256/stream";
            echo "<div class=\"contain-player\">";
            include '../../inc/player/player.php';
            echo "</div>";
            break;
          case 'shoutcast':
            echo "<div class=\"contain-player\">";
            $playerURL = "http://suaradio2.dyndns.ws:10342/stream";
            include '../../inc/player/player.php';
            echo "</div>";
            break;
          case 'wmp':
            $playerURL = "mms://suaradio4.dyndns.ws/10238";
            include '../../inc/player/player_wmp.php';
            break;
          case 'srnn':
            echo "<div class=\"contain-player\">";
            $playerURL = "http://suaradio2.dyndns.ws:10342/stream";
            include '../../inc/player/player.php';
            echo "</div>";
            break;
        }
      ?>
  </div>
  <?php
    include "../../inc/scripts.php";
  ?>
</body>
</html>