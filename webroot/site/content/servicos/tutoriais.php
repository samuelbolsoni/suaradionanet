<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
xmlns:og="http://ogp.me/ns#"
xmlns:fb="https://www.facebook.com/2008/fbml"
lang="pt-br">
	<?php
	require_once "../../inc/verificastatus.php";
	?>
	<head>
		<base href="<?php echo $url_base; ?>/servicos/"/>
		<?php
		include "../../inc/head.php";
		?>
		<link rel="stylesheet" href="../../css/3rd_party/colorbox/colorbox.css" />
	</head>

	<body>
		<div id="wrapper" class="internal services">
			<!-- Wrapper -->

			<div id="main">
				<!-- Main -->

				<?php include "../../inc/header_servicos.php" ?>

				<div id="content-wrapper">
					<!-- Content-Wrapper -->

					<div class="wrapper">
						<!-- Class Wrapper -->

						<div id="content" class="clearfix">
							<!-- Content -->

							<div class="content-bottom clearfix">
								<section class="content dropdown-infos">
									<!-- corpo de conteudo -->

									<div class="content-block">
										<!-- Menu lateral -->
										<?php
										include "menu_servicos.php";
										?>
										<!-- fim - Menu lateral -->
										<!-- Conteúdo -->
										<h3 class="font01 titles-services tutos" data-menu="tutoriais">Tutoriais</h3>
										<div class="text">
											Esta página tem como objetivo auxiliar nossos clientes na configuração dos principais softwares que fazem a geração de som e streaming para os servidores da SUA RÁDIO NA NET. Toda vez que encontrar alguma dificuldade na configuração, recorra aos tutoriais aqui expostos.
											<section class="administraveis-locais">
												<?php
												$query = mysql_query("select id, titulo from site_tutoriais_categoria");
												while ($rs = mysql_fetch_array($query)) {
													$i = 1;
													$idcat = $rs['id'];
													$titcat = $rs['titulo'];
													echo "<header>
                        		<h1 class=\"font01\">" . $titcat . "</h1>
                      		</header>";

													$query2 = mysql_query("select * from site_tutoriais where idcategoria = $idcat");
													$count = mysql_fetch_array(mysql_query("select count(id) as total from site_tutoriais where idcategoria = $idcat"));
													$count = $count['total'];
													while ($rs2 = mysql_fetch_array($query2)) {
														$idtut = $rs2['id'];
														$tittut = $rs2['titulo'];
														$link = $rs2['link'];
														$linktitulo = $rs2['linktitulo'];
														$opt = $rs2['linkoptions'];
														
														if($opt == 0){
															$link = "<a href=\"" . $link . "\" class=\"ytb-admin-v1 play-videos font01\" title=\"" . $linktitulo . "\">";
														}
														else{
															if($opt == 1){
																$link = "<a href=\"" . $link . "\" class=\"play-videos font01\" title=\"" . $linktitulo . "\">";
															}
															else{
																$link = "<a href=\"" . $link . "\" class=\"play-videos font01\" title=\"" . $linktitulo . "\" target=\"_blank\">";
															}
														}

														switch ($i) {
															case 1 :
																echo "<article class=\"only-for-first\">
                        ".$link."
                          <span class=\"icon-play\"></span>
                          " . $tittut . "
                        </a>
                      </article>";
																break;
																case $i == $count :
																echo "<article class=\"only-for-last\"><span class=\"borders\"></span>
                        				".$link."
                          <span class=\"icon-play\"></span>
                          " . $tittut . "
                        </a>
                      </article>
                      </section>
												<section class=\"administraveis-locais mgBottom0\">";
																break;
															default :
																echo "<article><span class=\"borders\"></span><span class=\"borders\"></span>
                        ".$link."
                          <span class=\"icon-play\"></span>
                          " . $tittut . "
                        </a>
                      </article>";
																break;
														}
														$i++;
													}
												}
												?>
											</section>

										</div>
										<!-- fim - Conteúdo -->
									</div>

									<a href="javascript:(window.history.go(-1));" class="bt-back" title="Voltar">
		                <span>Voltar</span>
		              </a>

								</section>
								<!-- corpo de conteudo -->
							</div>

						</div>
						<!-- Content -->

					</div>
					<!-- END: Class Wrapper -->

				</div>
				<!-- END: Content-Wrapper -->

			</div>
			<!-- END: Main -->

		</div><!-- END: Wrapper -->

		<?php
		include "../../inc/footer.php";
		include "../../inc/scripts.php";
		include "../../inc/scripts-internas.shtml";
		?>
		<script src="services-menu-control.js"></script>
		<script src="../../js/3rd_party/jquery.colorbox-min.js"></script>
		<script>
			// Chamada colorbox - youtube
			jQuery("a.ytb-admin-v1").colorbox({
				iframe : true,
				innerWidth : 560,
				innerHeight : 315,
				initialWidth : 50,
				initialHeight : 15,
				previous : "",
				next : "",
				current : ""
			});
		</script>
	</body>
</html>