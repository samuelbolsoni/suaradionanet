<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml"
      lang="pt-br">
<?php require_once "../../inc/verificastatus.php"; ?>
<head>
  <base href="<?php echo $url_base;?>/servicos/"/>
  <?php include "../../inc/head.php"; ?>
</head>

<body>
  <div id="wrapper" class="internal services"> <!-- Wrapper -->

    <div id="main"> <!-- Main -->
      
      <?php include "../../inc/header_servicos.php" ?>

      <div id="content-wrapper"> <!-- Content-Wrapper -->

        <div class="wrapper"> <!-- Class Wrapper -->
          
          <div id="content" class="clearfix"> <!-- Content -->
            
            <div class="content-bottom clearfix">
              <section class="content dropdown-infos"> <!-- corpo de conteudo -->
                
                <div class="content-block">
                  <!-- Menu lateral -->
                  <?php include "menu_servicos.php"; ?>
                  <!-- fim - Menu lateral -->
                  <!-- Conteúdo -->
                  <h3 class="font01 titles-services how-works" data-menu="como-funciona">Como Funciona</h3>
                  <div class="text">
                      A cada minuto a internet está mais atuante no mundo. A integração dela com o cotidiano se tornou indispensável. O streaming para rádios se popularizou e chega também a ambientes não corporativos. Muitas pessoas gostariam de se tornar um <strong>DJ</strong>, mas encontram dificuldades para mostrar se trabalho devido a falta de espaço no mercado. A internet possibilitou isso, com uma conexão BANDA LARGA em sua casa ou local de trabalho, é possível ter sua própria rádio. Portais, sites pessoais e organizações não governamentais, cada vez mais usam a internet para transmitir uma programação diferenciada e segmentada aos ouvintes do ciberespaço. Tenha sua própria emissora de maneira legal, sem ortoga da <strong>ANATEL</strong>, órgão governamental responsável pela radiodifusão no Brasil. Web rádio é legal e não precisa de autorização do governo. E o que é melhor, todos os softwares para a geração da web rádio são free.
                      <br /><br />
                      Veja nosso <a style="color: #494949;" href="../../../../../faq"><strong><u>FAQ</u></strong></a> e saiba como gerar uma programação via internet. Temos planos especiais para pessoas físicas que vão possibilitar a difusão de sua programação na web com um custo muito baixo.
                      <br /><br />
                      Veja como é fácil montar uma rádio profissional, veja nossos <a style="color: #494949;" href="../../../../../tutoriais.htm"><strong><u>tutoriais</u></strong></a>
                  </div>
                  <section class="varied-information">
                    <header class="font01 sub-titles">
                      O que você precisa ter e o que garantimos para colocar sua RÁDIO NA INTERNET? 
                    </header>
                    <article>
                      A operação da web rádio é feita através de seu pc, ao vivo ou gravada.
                    </article>
                    <article>
                      Para a operação é preciso um software de automatização (Zara Rádio) e outro para a enviar o sinal até nosso servidor. Após, enviamos um link com o som da emissora que deve ser colocado no site, blog ou distribuir para os ouvintes através de qualquer página na web. Caso assine um plano com site, o player já estará integrado ao site.
                    </article>
                    <article>
                      O PC da web rádio deve ter no mínimo 2GB de memória e processador de 1 GHZ.
                    </article>
                    <article>
                      Indispensável conexão de banda larga de no mínimo 300k  de upload para enviar em 32k ou superior. Se sua internet for via rádio (antena), não garantimos a qualidade, pois em 80% dos casos são instáveis e não possuem largura de banda suficiente para o streaming.
                    </article>
                    <article>
                      Garantimos 99,99% de uptime, ou seja, permanência de sua rádio no ar por mês mês.
                    </article>
                    <article>
                      A instalação é fácil com tutoriais passo a passo via vídeo. Caso não consiga instalar, fazemos a instalação em seu pc via acesso remoto.
                    </article>
                    <article>
                      Temos suporte via chat de segunda a sexta-feira das 8h às 24 horas. 
                    </article>
                    <article>
                      Quanto custa o serviço? O valor do streaming, som na web, depende da capacidade de ouvintes conectados ao mesmo tempo e velocidade de transmissão e pode ser acessado neste link: <a href="../../../../../valores_radionanet.htm"><strong><u>PLANOS</u></strong></a>
                    </article>
                  </section>
                    
                  <!-- fim - Conteúdo -->
                </div>
                
                <a href="javascript:(window.history.go(-1));" class="bt-back" title="Voltar">
                  <span>Voltar</span>
                </a>
                
              </section> <!-- corpo de conteudo -->
            </div>
            
          </div> <!-- Content -->

        </div> <!-- END: Class Wrapper -->

      </div> <!-- END: Content-Wrapper -->

    </div> <!-- END: Main -->

  </div><!-- END: Wrapper -->

  <?php
    include "../../inc/footer.php";
    include "../../inc/scripts.php";
    include "../../inc/scripts-internas.shtml";
  ?>
  <script src="services-menu-control.js"></script>
</body>
</html>