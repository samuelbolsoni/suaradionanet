<!DOCTYPE html>
<html lang="pt-br">
	<?php
	require_once "../../inc/verificastatus.php";
	?>
	<head>
		<base href="<?php echo $url_base; ?>/servicos/"/>
		<?php
		include "../../inc/head.php";
		?>
	</head>

	<body>
		<div id="wrapper" class="internal services">
			<!-- Wrapper -->
			<div id="main">
				<!-- Main -->

				<?php include "../../inc/header_servicos.php" ?>

				<div id="content-wrapper">
					<!-- Content-Wrapper -->

					<div class="wrapper">
						<!-- Class Wrapper -->

						<div id="content" class="clearfix">
							<!-- Content -->

							<div class="content-bottom clearfix">
								<section class="content dropdown-infos">
									<!-- corpo de conteudo -->

									<div class="content-block">
										<!-- Menu lateral -->
										<?php
										include "menu_servicos.php";
										?>
										<!-- fim - Menu lateral -->
										<!-- Conteúdo -->
										<h3 class="font01 titles-services planosRadios" data-menu="planos-radios">Planos Radios</h3>
										<div class="text">
											Atendemos todo o Brasil. Ligue agora para a nossa central de vendas, entre em contato através do MSN ou assine um dos planos pelo formulário abaixo.
											<br />
											<br />
											*Cobrimos oferta da Concorrência (sujeito a análise).
											<br />
											<br />
											Em todos os planos você terá estatísticas de ouvintes e tráfego ilimitado.
											<br />
											<br />
											<br />
											<br />
                      <br />
                      <br />
                      <br />
											<span class="uppercase font01">CONHEÇA OS NOSSOS NOVOS PLANOS</span>
											<a href="" class="fRight mgTopLess10"> <img src="../../img/misc/duvida-streaming.jpg" alt="Não sabe que tipo de streaming assinar? Clique aqui e conheça as diferenças."> </a>
											<br class="cAll" />
											<div class="table-wrapper planosAccShout">
												<table class="table">
													<thead>
														<tr>
															<th><img src="../../img/misc/acc-plus-shoutcast.png" class="table-img-top" /></th>
															<th>Plano 1</th>
															<th>Plano 2</th>
															<th>Plano 3</th>
															<th>Plano 4</th>
															<th>Plano 5</th>
															<th>Plano 6</th>
															<th>Plano 7</th>
															<th>Plano 8</th>
															<th>Plano 9</th>
															<th class="last">Plano 10</th>
														</tr>
														<tr>
															<th colspan="11" class="containSubtable">
															<table class="subtable">
																<tr>
																	<td width="222" class="first">Quantidade de Ouvintes Simultâneos</td>
																	<?php
																	function addzeros($valorplano) {
																		if (strpos($valorplano, '.')) {
																			$valorplano = $valorplano . "0";
																			$valorplano = str_replace('.', ',', $valorplano);
																		} else {
																			if ($valorplano != NULL) {
																				$valorplano = $valorplano . ",00";
																			}
																		}
																		return $valorplano;
																	}

																	$query1 = mysql_query("SELECT p.ouvintes AS numOuvintes, o.titulo FROM site_planos AS p, site_ouvintes AS o WHERE p.streaming = 4 AND p.ouvintes = o.id GROUP BY o.titulo ORDER BY o.titulo ASC");
																	$i = 1;
																	// $count = mysql_query("SELECT count(p.id) as count FROM site_planos AS p, site_ouvintes AS o WHERE p.streaming = 4 AND p.ouvintes = o.id GROUP BY o.titulo ORDER BY o.titulo ASC");
																	// $count = mysql_fetch_array($count);
																	// $count = $count['count'];
																	$count = 10;


																	while ($rs = mysql_fetch_array($query1)) {
																		$idouvintes = $rs['id'];
																		$tituloouvintes = $rs['titulo'];

																		switch ($i) {
																			case 1 :
																				echo "<td width=\"56\">" . $tituloouvintes . "</td>";
																				break;
																			case 2 :
																			case 3 :
																			case 4 :
																				echo "<td width=\"60\">" . $tituloouvintes . "</td>";
																				break;
																			case 5 :
																			case 6 :
																			case 7 :
																				echo "<td width=\"68\">" . $tituloouvintes . "</td>";
																				break;
																			case 8 :
																			case 9 :
																				echo "<td width=\"75\">" . $tituloouvintes . "</td>";
																				break;
																			case $i == $count :
																				echo "<td width=\"76\" class=\"last\" >" . $tituloouvintes . "</td>";
																				echo "</tr>
																	</table>
																	</th>
																	</tr>
																	</thead>
																	<tbody>";
																				break;
																		}

																		$i++;

																	}

																	$query2 = mysql_query("SELECT a.id as id, a.dt_cad as dt_cad , b.titulo as ouvintes, b.id as id_ouvintes, c.id as id_qualidade,
 c.titulo as qualidade, d.titulo as streaming FROM site_planos as a, site_ouvintes as b, site_qualidade as c, site_streaming as d 
 WHERE b.id = a.ouvintes AND c.id = a.qualidade AND d.id = a.streaming AND a.streaming = 4 and b.id= 20 order by c.id limit 10");
																	$i = 1;

																	while ($rs2 = mysql_fetch_array($query2)) {
																		$idqualidade = $rs2['id_qualidade'];
																		$tituloqualidade = $rs2['qualidade'];

																		echo "<tr>";
																		echo "<td class=\"first\">Qualidade " . $tituloqualidade . "</td>";
																		$query3 = mysql_query("SELECT id,valor from `site_planos` where qualidade='$idqualidade' AND streaming=4 order by ISNULL(valor), valor asc limit 10");
																		while ($rs3 = mysql_fetch_array($query3)) {

																			$idplano = $rs3['id'];
																			$valorplano = $rs3['valor'];

																			//echo $valorplano. "<br/>" ;
																			if ($valorplano >= 33.50) {
																				$valorplano = addzeros($valorplano);
																				echo "<td><a href=\"../../../../../cadastro.htm/" . $idplano . "\">" . $valorplano . "<sup>*</sup></a></td>";
																			} else {
																				$valorplano = addzeros($valorplano);
																				if ($valorplano == NULL) {
																					echo "<td class=\"consult\"><a href=\"../../../../../cadastro.htm/" . $idplano . "\">Consultar</a></td>";
																				} else {
																					echo "<td><a href=\"../../../../../cadastro.htm/" . $idplano . "\">" . $valorplano . "</a></td>";
																				}
																			}

																		}
																		echo "</tr>";
																	}
																	?> 
																	</tbody>
															</table>
											</div><!-- END: table-wrapper -->
											<br class="cAll"/>
											<br />
											<br />
											<a href="../../../../../auto_dj.htm" class="image-1"> <img src="../../img/misc/planos_radios-auto-dj.jpg" /> </a>
											<a href="../../../../../site_administravel.htm" class="image-2"> <img src="../../img/misc/planos_radios-site1.jpg" /> </a>
											<a href="../../../../../site_administravel.htm" class="image-3"> <img src="../../img/misc/planos_radios-site2.jpg" /> </a>
											
											<br class="cAll" />
											
											<p class="not-AD">*Planos com 16k de qualidade não possuem auto-DJ</p>
											
											<br class="cAll" />
											<br />
											
											<div class="table-wrapper planosAccShout planosWma">
												<table class="table">
													<thead>
														<tr>
															<th width="" class=""><img src="../../img/misc/planos_radios-wma.png" style="margin-left: 40px; float: left" /></th>
															<th>Plano 1</th>
															<th>Plano 2</th>
															<th>Plano 3</th>
															<th>Plano 4</th>
															<th>Plano 5</th>
															<th>Plano 6</th>
															<th>Plano 7</th>
															<th class="last">Plano 8</th>
														</tr>
														<tr>
															<th colspan="9" class="containSubtable">
															<table class="subtable">
																<tr>
																	<td class="first">Quantidade de Ouvintes Simultâneos</td>
																	<?php
																	$query1 = mysql_query("SELECT p.ouvintes AS numOuvintes, o.titulo FROM site_planos AS p, site_ouvintes AS o WHERE p.streaming = 2 AND p.ouvintes = o.id GROUP BY o.titulo ORDER BY o.titulo ASC");
																	$i = 1;
																	// $count = mysql_query("SELECT count(p.id) as count FROM site_planos AS p, site_ouvintes AS o WHERE p.streaming = 4 AND p.ouvintes = o.id GROUP BY o.titulo ORDER BY o.titulo ASC");
																	// $count = mysql_fetch_array($count);
																	// $count = $count['count'];
																	$count = 8;


																	while ($rs = mysql_fetch_array($query1)) {
																		$idouvintes = $rs['numOuvintes'];
																		$tituloouvintes = $rs['titulo'];

																		switch ($i) {
																			case 1 :
																				echo "<td width=\"75\">" . $tituloouvintes . "</td>";
																				break;
																			case 2 :
																			case 3 :
																			case 4 :
																				echo "<td width=\"77\">" . $tituloouvintes . "</td>";
																				break;
																			case 5 :
																			case 6 :
																				echo "<td width=\"78\">" . $tituloouvintes . "</td>";
																				break;
																			case 7 :
																				echo "<td width=\"84\">" . $tituloouvintes . "</td>";
																				break;
																			case $i == $count :
																				echo "<td width=\"84\" class=\"last\" >" . $tituloouvintes . "</td>";
																				echo "</tr>
																	</table>
																	</th>
																	</tr>
																	</thead>
																	<tbody>";
																				break;
																		}

																		$i++;

																	}
																	?>
														<tr class="lastItem">
															<?
															$query2 = mysql_query("SELECT a.id as id, a.dt_cad as dt_cad , b.titulo as ouvintes, b.id as id_ouvintes, c.id as id_qualidade,
 c.titulo as qualidade, d.titulo as streaming FROM site_planos as a, site_ouvintes as b, site_qualidade as c, site_streaming as d 
 WHERE b.id = a.ouvintes AND c.id = a.qualidade AND d.id = a.streaming AND a.streaming = 2 and b.id= 20 order by c.id limit 10");
																	$i = 1;

																	while ($rs2 = mysql_fetch_array($query2)) {
																		$idqualidade = $rs2['id_qualidade'];
																		$tituloqualidade = $rs2['qualidade'];

																		echo "<tr>";
																		echo "<td class=\"first\">Qualidade " . $tituloqualidade . "</td>";
																		$query3 = mysql_query("SELECT id,valor from `site_planos` where qualidade='$idqualidade' AND streaming=2 order by ISNULL(valor), valor asc limit 8");
																		while ($rs3 = mysql_fetch_array($query3)) {

																			$idplano = $rs3['id'];
																			$valorplano = $rs3['valor'];

																			//echo $valorplano. "<br/>" ;
																			if ($valorplano >= 33.50) {
																				$valorplano = addzeros($valorplano);
																				echo "<td><a href=\"../../../../../cadastro.htm/" . $idplano . "\">" . $valorplano . "<sup>*</sup></a></td>";
																			} else {
																				$valorplano = addzeros($valorplano);
																				if ($valorplano == NULL) {
																					echo "<td class=\"consult\"><a href=\"../../../../../cadastro.htm/" . $idplano . "\">Consultar</a></td>";
																				} else {
																					echo "<td><a href=\"../../../../../cadastro.htm/" . $idplano . "\">" . $valorplano . "</a></td>";
																				}
																			}

																		}
																		echo "</tr>";
																	}
																	?> 
															</tr>
													</tbody>
												</table>
											</div><!-- END: table-wrapper -->
											<br class="cAll"/>
											<br />
											<br />
											<a href="../../../../../site_administravel.htm" class="image-4"> <img src="../../img/misc/planos_radios-site1.jpg" /> </a>
											<a href="../../../../../site_administravel.htm" class="image-5"> <img src="../../img/misc/planos_radios-site2.jpg" /> </a>
										</div>
										<!-- fim - Conteúdo -->
									</div>

									<a href="javascript:(window.history.go(-1));" class="bt-back" title="Voltar">
	                  <span>Voltar</span>
	                </a>

								</section>
								<!-- corpo de conteudo -->
							</div>

						</div>
						<!-- Content -->

					</div>
					<!-- END: Class Wrapper -->

				</div>
				<!-- END: Content-Wrapper -->

			</div>
			<!-- END: Main -->

		</div><!-- END: Wrapper -->

		<?php
		include "../../inc/footer.php";
		include "../../inc/scripts.php";
		include "../../inc/scripts-internas.shtml";
		?>
		<script src="services-menu-control.js"></script>
		<script src="services-control-hover-table.js"></script>
		<script type="text/javascript">
			$('.table td:last-child').css({
				background : 'none'
			})
		</script>
	</body>
</html>