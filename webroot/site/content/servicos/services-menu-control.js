$(document).ready(function($) {
  
  var activeThis = $('h3[data-menu]').attr('data-menu');
  $('#menu-lateral a[data-menu]').removeClass('active');
  $('#menu-lateral a[data-menu]').each(function() {
    if($(this).attr('data-menu') == activeThis) {
      $(this).addClass('active');
    }
  });

});
