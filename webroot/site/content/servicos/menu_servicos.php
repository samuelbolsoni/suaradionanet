<div class="ml-top">
  <div class="ml-bottom">
    <nav class="ml-content">
      <ul id="menu-lateral">
        <li>
          <a class="font01 icon-documents" href="../../../../../valores_radionanet.htm" data-menu="planos-radios">Planos Radios</a>
        </li>
        <li>
          <div class="border-top"> </div>
          <a class="font01 icon-screw" href="../../../../../como_montar_uma_web_radio.htm" data-menu="monte-radio">Monte sua Rádio</a>
        </li>
        <li>
          <div class="border-top"> </div>
          <a class="font01 icon-fire" href="../../../../../site_administravel.htm" data-menu="site-administravel">Site Administrável</a>
        </li>
        <li>
          <div class="border-top"> </div>
          <a class="font01 icon-volume" href="../../../../../tipos_streaming.htm" data-menu="tipos-streaming">Tipos de Streaming</a>
        </li>
        <li>
          <div class="border-top"> </div>
          <a class="font01 icon-fone" href="../../../../../auto_dj.htm" data-menu="auto-dj">Auto-DJ</a>
        </li>
        <li>
          <div class="border-top"> </div>
          <a class="font01 icon-tv" href="../../../../../como_montar_uma_tv_online.htm" data-menu="tv-web-online">TV Web Online</a>
        </li>
        <li>
          <div class="border-top"> </div>
          <a class="font01 icon-light" href="../../../../../radio_on_line.htm" data-menu="como-funciona">Como Funciona</a>
        </li>
        <li>
          <div class="border-top"> </div>
          <a class="font01 icon-tuto" href="../../../../../tutoriais.htm" data-menu="tutoriais">Tutoriais</a>
        </li>
      </ul>
    </nav>
  </div>
</div>