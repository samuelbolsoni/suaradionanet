<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml"
      lang="pt-br">
<?php require_once "../../inc/verificastatus.php"; ?>
<head>
  <base href="<?php echo $url_base;?>/servicos/"/>
  <?php include "../../inc/head.php"; ?>
</head>

<body>
  <div id="wrapper" class="internal services"> <!-- Wrapper -->

    <div id="main"> <!-- Main -->
      
      <?php include "../../inc/header_servicos.php" ?>

      <div id="content-wrapper"> <!-- Content-Wrapper -->

        <div class="wrapper"> <!-- Class Wrapper -->
          
          <div id="content" class="clearfix"> <!-- Content -->
            
            <div class="content-bottom clearfix">
              <section class="content"> <!-- corpo de conteudo -->
                
                <div class="content-block">
                  <!-- Menu lateral -->
                  <?php include "menu_servicos.php"; ?>
                  <!-- fim - Menu lateral -->
                  <!-- Conteúdo -->
                  <h3 class="font01 titles-services tipoStreaming" data-menu="tipos-streaming">Tipos de Streaming</h3>
                  <br />
                  <div class="text">
                    A Sua Rádio na Net trabalha com três formatos de streaming para melhor atender o cliente: MP3, WMA e AACPLUS +HD ÁUDIO.<br />
                    <br />
                    Para que sua emissora tenha um produto satisfatório, vamos explicar nesta página a diferença entre os três tipos de streaming, vantagens e desvantagens de cada. Antes de contratar um de nossos planos, tenha certeza de que ele é o mais adequado à suas necessidades.<br />
                    <br />
                    <img src="../../img/misc/tipo_streaming-acc-plus.jpg" class="fLeft" /> <br />
                    <br />
                    Podemos dizer que o AACPLUS é a revolução do streaming na internet. A qualidade de som é insuperável, com apenas 32 kbps é possível escutar um som de alta fidelidade com separação em estéreo perfeita. Se você é conhecedor de tecnologias para rádios vai entender bem o que estamos explicando aqui. <br />
                    <br />
                    O AACPLUS é produzido pela ORBAN, uma das empresas líder no mundo em processadores de áudio para emissoras de rádio. Toda rádio que opera em AM ou FM possui um diferencial em relação as suas concorrentes na qualidade de som. E isso não é diferente na internet. O AACPLUS possui um equalizador interno paramétrico que faz uma diferença muito grande na qualidade do áudio gerado via web. <br />
                    <br />
                    Entre as vantagens do AACPLUS podemos citar: Qualidade de som insuperável até o momento e Softwares de streaming totalmente free. Antigamente para escutar a qualidade em alta definição o internauta precisa ter em seu computador instalado o winamp 5.3 ou superior ou instalar um plugin para que seja possível escutar no média player. Caso contrário, não existia maneira de ser ouvindo. Hoje, com a técnologia Flash qualquer computador pode escutar o som do AAC sem ter que instalar plugins ou softwares na máquina do ouvinte.<br />
                    <br />
                    Para quem possui internet de baixa velocidade até 300k, sugerimos que assine o plano de 16k em AAC HD ÁUDIO. A qualidade serásuperior ao 32k do MP3 e do Windows Média Áudio. Pode acontecer uma variação de qualidade dependendo de sua placa de som.<br />
                    <br />
                    <img src="../../img/misc/tipo_streaming-mp3-layer3.jpg" class="fLeft" /> <br />
                    <br />
                    O MP3 PRO é um tecnologia que surgiu para tentar brigar com WMA (Microsoft), mas perdeu a guerra. O formato permite reduzir a velocidade de streaming e melhor a qualidade em ate 33%, sendo que a qualidade sonora superior ao antigo MP3. Tudo indicava, quando do surgimento do formato em 2001, que o MP3 PRO chegava para ficar, pois não possuía nenhum concorrente a altura. Sua emissora operando na internet em Mp3 PRO com 32 kbps terá som superior ao do rádio FM, podendo ser comparado a qualidade de CD. A desvantagem do MP3PRO é que para o internauta escutar o som com melhor definição precisa instalar o plugin Mp3Pro no seu player, para Winamp ou Média Player. E se o ouvinte não instalar o plugin? Neste caso o internauta não deixa de escutar o som de sua emissora, mas vai escutar com a qualidade do MP3 normal. Outra desvantagem do MP3PRO é que existe apenas um software que faz a codificação do streaming para o PRO, o Simplecast, que é um software pago e custa mais de U$ 100,00 (cem dólares). <br />
                    <br />
                    <img src="../../img/misc/tipo_streaming-wma.jpg" class="fLeft" /><br />
                    <br />
                    Windows Media Audio (WMA) é um formato produzido pela Microsoft que tem grande compatibilidade com o Windows Media Player.<br />
                    <br />
                    Entretanto, pode ser reproduzido pelo Winamp e outros reprodutores de áudio, com exceção do iTunes, que oferece serviço de codificação de WMA ao AAC. Oferece qualidade de áudio superior ao MP3 para streaming. Além de ter uma tamanho levemente reduzido em relação a uma música no formato mp3.<br />
                    <br />
                    O primeiro codec WMA era baseado no trabalho anterior de Henrique Malvar e de sua equipe. De acordo com o artigo publicado, a tecnologia foi transferida para a equipe do Windows Media Team da Microsoft.<br />
                    <br />
                    <div class="table-wrapper tipoStreaming">
                      <table class="table font01">
                        <thead>
                          <tr>
                            <th width="150">Formato</th>
                            <th width="140">Qualidade</th>
                            <th width="210">Players</th>
                            <th>Precisa de Plugin</th>
                            <th class="last">Número de Ouvites</th>
                          </tr> 
                          <tr>
                            <th colspan="5" class="bg-color">Tabela que mostra o diferencial entre formatos</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>AACPLUS HD AUDIO</td>
                            <td>EXCELENTE</td>
                            <td class="fSize12">Winamp 5.3 ou superior, Flash Player que roda em todos os computadores.</td>
                            <td>NÃO</td>
                            <td class="last">EXCELENTE</td>
                          </tr>
                          <tr>
                            <td>WMA</td>
                            <td>ÓTIMA, APARTIR DE 64K</td>
                            <td class="fSize12">Média Player, Winamp, Real Player, Outros.</td>
                            <td>NÃO</td>
                            <td class="last">MUITO BOM</td>
                          </tr>
                          <tr>
                            <td>MP3</td>
                            <td>Boa, apartir de 48k</td>
                            <td class="fSize12">Média Player, Winamp, Real Player, Outros.</td>
                            <td>Não</td>
                            <td class="last">BOM</td>
                          </tr>
                          <tr>
                            <td>MP3 PRO</td>
                            <td>Muito Boa</td>
                            <td class="fSize12">Média Player, Winamp.</td>
                            <td>SIM, (EM PRO)</td>
                            <td class="last">BAIXO EM PRO</td>
                          </tr>
                          <tr class="lastItem">
                            <td colspan="5" class="last fSize12">Na tabela acima é possível ver bem o diferencial dos três formatos. Dependendo do uso que será dado para o streaming, se webrádio, rádio comercial ou rádio loja, o cliente terá que escolher o formato.</td>
                          </tr>
                        </tbody>
                      </table>
                    </div><!-- END: table-wrapper -->
                    <br class="cAll" />
                    <br />
                    <h3 class="font01 subtitle icon-aviao"> <span class="icon"></span> VELOCIDADE DA INTERNET X QUALIDADE</h3><br />
                    Se sua internet não for via rádio (antena) e for acima de 500k de velocidade, poderá assinar qualquer plano que oferecemos a partir de 32k. Se sua internet for via rádio (antena) e sua velocidade foi abaixo de 300k sugerimos para maior estabilidade usar o plano que oferecemos em 16k AAC+ HD ÁUDIO . Terá uma boa qualidade de som e sua rádio não vai correr o risco de ter falhas ou cortes no som devido a falta de velocidade de sua internet.
                  </div>
                  <!-- fim - Conteúdo -->
                </div>
                
                <a href="javascript:(window.history.go(-1));" class="bt-back" title="Voltar">
                  <span>Voltar</span>
                </a>
                
              </section> <!-- corpo de conteudo -->
            </div>
            
          </div> <!-- Content -->

        </div> <!-- END: Class Wrapper -->

      </div> <!-- END: Content-Wrapper -->

    </div> <!-- END: Main -->

  </div><!-- END: Wrapper -->

  <?php
    include "../../inc/footer.php";
    include "../../inc/scripts.php";
    include "../../inc/scripts-internas.shtml";
  ?>
  <script src="services-menu-control.js"></script>
</body>
</html>