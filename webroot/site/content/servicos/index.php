<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml"
      lang="pt-br">
<?php require_once "../../inc/verificastatus.php"; ?>
<head>
  <base href="<?php echo $url_base;?>/servicos/"/>
  <?php include "../../inc/head.php"; ?>
</head>

<body>
  <div id="wrapper" class="internal services"> <!-- Wrapper -->

    <div id="main"> <!-- Main -->
      
      <?php include "../../inc/header_servicos.php" ?>

      <div id="content-wrapper"> <!-- Content-Wrapper -->

        <div class="wrapper"> <!-- Class Wrapper -->
          
          <div id="content" class="clearfix"> <!-- Content -->
            
            <div class="content-bottom clearfix">
              <section class="content dropdown-infos"> <!-- corpo de conteudo -->
                
                <div class="content-block">
                  <!-- Menu lateral -->
                  <?php include "menu_servicos.php"; ?>
                  <!-- fim - Menu lateral -->
                  <!-- Conteúdo -->
                    <div class="text">
                      A Sua Rádio na Net  oferece o que há de mais moderno em Streaming de rádio e TV,  sites e softwares de automatização como o Auto-DJ.
                      <br /><br />
                      Navegue no menu lateral e conheça mais dos serviços que prestamos.  Tire suas dúvidas  e escolha o pacote ideal para as necessidades de sua emissora. 
                      <br /><br />
                      Em caso de dúvida fale com nossa equipe de atendimento via <strong>chat ao vivo</strong> ou por um dos telefones informados na parte superior de nosso site. NA Sua Rádio na Net sua emissora terá qualidade de som  e site com ótima navegabilidade.
                    </div>
                    <div class="bg-colors blue auto-dj">
                      <div class="containerBox">
                        <div class="text-container">
                          <p class="title font01">
                            Auto Dj Grátis
                          </p>
                          <p class="text">
                            Assine um plano de streaming a partir de R$ 39,60 e ganhe totalmente grátis o sistema de auto-dj para sua rádio.
                          </p>
                        </div>
                      </div>  
                      <a class="bt-know" href="../../../../../auto_dj.htm" title="Conheça"></a>
                      <span class="icon"></span>
                      
                    </div>
                    <div class="bg-colors green administravel">
                      <div class="containerBox">
                        <div class="text-container">
                          <p class="title font01">
                            Site Administrável Grátis
                          </p>
                          <p class="text">
                            Assine um plano de streaming a partir de R$ 33,50 e ganhe totalmente grátis um site administrável para sua rádio.
                          </p>
                        </div>
                      </div>  
                      <a class="bt-know" href="../../../../../site_administravel.htm" title="Conheça"></a>
                      <span class="icon"></span>
                    </div>
                  <!-- fim - Conteúdo -->
                </div>
                
                <a href="javascript:(window.history.go(-1));" class="bt-back" title="Voltar">
                  <span>Voltar</span>
                </a>
                
              </section> <!-- corpo de conteudo -->
            </div>
            
          </div> <!-- Content -->

        </div> <!-- END: Class Wrapper -->

      </div> <!-- END: Content-Wrapper -->

    </div> <!-- END: Main -->

  </div><!-- END: Wrapper -->

  <?php
    include "../../inc/footer.php";
    include "../../inc/scripts.php";
    include "../../inc/scripts-internas.shtml";
  ?>
</body>
</html>