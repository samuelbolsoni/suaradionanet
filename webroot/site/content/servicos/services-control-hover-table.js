jQuery(document).ready(function($) {
  
  $('.table > tbody tr td').live({
    mouseenter : function (e) {
      $('.table > tbody tr td').removeClass('hover-td');
      if(!$(this).hasClass('first')) {
        $(this).addClass('hover-td');
      }
      if($('#bt-assinar-agora').html() != undefined) {
        $('#bt-assinar-agora').remove();
      }
    },
    mouseleave : function (e) {
        if($(this).hasClass('first')) {
          $('#bt-assinar-agora').hide();
        }
        if($('#bt-assinar-agora').html() == undefined) {
          $(this).removeClass('hover-td'); 
        }
    },
    click : function (e) {
      var pos = $(this).offset();
      var link = $(this).children('a').attr('href');
      if($('#bt-assinar-agora').html() == undefined) {
        $('<a href="'+link+'" id="bt-assinar-agora"></a>').appendTo('body');
      }
      if($('#bt-assinar-agora').hasClass('open')) {
        $('#bt-assinar-agora').remove();
      } else {
        $('#bt-assinar-agora')
          .css({
            top  : pos.top - 10,
            left : pos.left + 50
          })
          .addClass('open')
          .show();
      }
      return false;
    }
  });

  $('.table > tbody tr td a').live({
    click : function () {
      var pos = $(this).offset();
      var link = $(this).attr('href');
      if($('#bt-assinar-agora').html() == undefined) {
        $('<a href="'+link+'" id="bt-assinar-agora"></a>').appendTo('body');
      }
      if($('#bt-assinar-agora').hasClass('open')) {
        $('#bt-assinar-agora').remove();
      } else {
        $('#bt-assinar-agora')
          .css({
            top  : pos.top - 10,
            left : pos.left + 50
          })
          .addClass('open')
          .show();
      }
      return false;
    }
  });

});
