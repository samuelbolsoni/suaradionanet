<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml"
      lang="pt-br">
<?php require_once "../../inc/verificastatus.php"; ?>
<head>
  <base href="<?php echo $url_base;?>/servicos/"/>
  <?php include "../../inc/head.php"; ?>
</head>

<body>
  <div id="wrapper" class="internal services"> <!-- Wrapper -->

    <div id="main"> <!-- Main -->

      <?php include "../../inc/header_servicos.php" ?>

      <div id="content-wrapper"> <!-- Content-Wrapper -->

        <div class="wrapper"> <!-- Class Wrapper -->

          <div id="content" class="clearfix"> <!-- Content -->

            <div class="content-bottom clearfix">
              <section class="content dropdown-infos"> <!-- corpo de conteudo -->

                <div class="content-block">
                  <!-- Menu lateral -->
                  <?php include "menu_servicos.php"; ?>
                  <!-- fim - Menu lateral -->
                  <!-- Conteúdo -->
                  <h3 class="font01 titles-services dj-fone" data-menu="auto-dj">Auto-DJ</h3>
                  <div class="text">
                    É a  possibilidade de poder fazer sua programação musical sem a necessidade de seu computador ligado. Com o Auto-DJ é possível criar blocos comerciais, vinhetas e programas musicais que irão rodar no  dia e na hora que  você escolher.
                    <br /><br />
                    <!-- A Sua Rádio na Net oferece essa ferramenta nos planos a partir de R$ 39,60/mês com um espaço mínimo de 10GB para você colocar suas músicas.  -->
                    Agora o Auto-Dj é a partir de R$ 31,90 e tem duas capacidades, Auto-DJ com 5 GB de espaço. E A partir de R$ 43,90 Auto-DJ com 25 GB de espaço.
                    <br /><br />
                    O auto-DJ é uma das opções mais usada por muitos internautas que querem ter uma rádio online, mas não possuem tempo e internet disponível para manter uma rádio 24 horas no ar. É uma rádio 100% virtual rodando da maneira que você quer diretamente do nosso servidor.
                    <br /><br />
                    A Sua Rádio na Net oferece o Auto-DJ com dois tipos de  painel  de controle: CENTOVA ou SONIC.
                    <br /><br />
                    O Auto-DJ do Centova é mais robusto e com mais ferramentas. O que possibilita a criação de sua playlist de maneira profissional.
                    <br /><br />
                    O  Auto-DJ do SONIC não possui tantos recursos. É possível fazer playlist seqüencial ou randômica. Mas possui uma porta extra para DJ. Com ela é possível fazer a troca do Auto-DJ para o Ao Vivo sem o ouvinte notar, com  uma mixagem perfeita. Além disso, caso sua rádio tenha muitos locutores de diversos lugares do Brasil, você pode gerar uma senha para cada locutor e manter o controle total sobre seu painel de controle.
                    <br /><br />

                    <section class="varied-information">
                      <header class="font01 sub-titles">
                        Vantagens WHMSonic:
                      </header>
                      <article>
                        Você terá uma porta exclusiva que pode ser usada por vários locutores em PCs diferentes sem perder nenhum ouvinte na troca de DJs.
                      </article>
                      <article>
                        AutoDJ com encoder AAC + ou MP3P
                      </article>
                      <article>
                        Fácil operação, com playlists seqüenciais ou randômicas.
                      </article>
                    </section>

                    <section class="varied-information">
                      <header class="font01 sub-titles">
                        Vantagens do CentovaCast:
                      </header>
                      <article>
                        Possibilidade de criar vários Playlists sendo programação musical, vinhetas, comerciais.
                      </article>
                      <article>
                        Programação do AutoDJ por horários e dias determinados. Você pode criar uma programação complete, totalmente profissional, sem precisar ficar com eu PC ligado.
                      </article>
                      <article>
                        Fácil operação, com uma infinidade de possibilidade de criação de playlists.
                      </article>
                    </section>

                    O Auto-DJ deixará sua rádio 100% profissional. Fale com um atendente online e peça já o seu pacote de rádio com Auto-DJ.
                  </div>
                  <figure class="dj-images">
                    <img src="../../img/misc/auto-dj.jpg" alt="Auto-DJ" title="Auto-DJ"/>
                    <img class="img-two" src="../../img/misc/fones2.jpg" alt="Fones" title="Auto-DJ"/>
                  </figure>
                  <!-- fim - Conteúdo -->
                </div>

                <a href="javascript:(window.history.go(-1));" class="bt-back" title="Voltar">
                  <span>Voltar</span>
                </a>

              </section> <!-- corpo de conteudo -->
            </div>

          </div> <!-- Content -->

        </div> <!-- END: Class Wrapper -->

      </div> <!-- END: Content-Wrapper -->

    </div> <!-- END: Main -->

  </div><!-- END: Wrapper -->

  <?php
    include "../../inc/footer.php";
    include "../../inc/scripts.php";
    include "../../inc/scripts-internas.shtml";
  ?>
  <script src="services-menu-control.js"></script>

</body>
</html>