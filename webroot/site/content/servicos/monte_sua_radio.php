<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml"
      lang="pt-br">
<?php require_once "../../inc/verificastatus.php"; ?>
<head>
  <base href="<?php echo $url_base;?>/servicos/"/>
  <?php include "../../inc/head.php"; ?>
</head>

<body>
  <div id="wrapper" class="internal services"> <!-- Wrapper -->

    <div id="main"> <!-- Main -->
      
      <?php include "../../inc/header_servicos.php" ?>

      <div id="content-wrapper"> <!-- Content-Wrapper -->

        <div class="wrapper"> <!-- Class Wrapper -->
          
          <div id="content" class="clearfix"> <!-- Content -->
            
            <div class="content-bottom clearfix">
              <section class="content dropdown-infos"> <!-- corpo de conteudo -->
                
                <div class="content-block">
                  <!-- Menu lateral -->
                  <?php include "menu_servicos.php"; ?>
                  <!-- fim - Menu lateral -->
                  <!-- Conteúdo -->
                  <h3 class="font01 titles-services screw" data-menu="monte-radio">Monte Sua Rádio</h3>
                  <div class="text">
                    Como colocar sua rádio na web? Existem suas maneiras de gerar som em tempo real via internet. A primeira é
                    usando seu próprio computador como servidor, o que não recomendamos pelos seguintes motivos:
                    <br /><br />
                    <strong>1 -</strong> Os internautas irão se conectar diretamente em seu pc. nas primeiras conexões nem você ou
                    o (s) internauta (s) ira notar problemas. Mas conforme for aumentando o número de ouvintes, sua internet irá se
                    tornando lenta e o som dos ouvintes começa a ter falhas e picotes.
                    <br /><br />
                    <strong>2 -</strong> Não estando seu computador protegido e adequadamente configurado, sua rede corre o risco
                    de ser atacada por Hackers.
                    <br /><br />
                    A segunda opção é mais adequada, não só pela segurança, mas pela grande capacidade que possui nossos servidores,
                    que irá fazer a distribuição do streaming. Os internautas irão se conectar nele e não no computador que está
                    gerando o som (o seu). Isso possibilita que seu pc fique estável não sobrecarregando a rede local.                       
                  </div>
                  <figure class="our-radio">
                    <img src="../../img/misc/misc002.png" class="ourradio-img" alt="O que não é recomendando para seu computador" title="Não Recomendado"/>
                    <img src="../../img/misc/misc003.png" class="second-ourradio-img" alt="O que é recomendado para seu computador" title="Recomendado"/>
                  </figure>
                  <!-- fim - Conteúdo -->
                </div>
                
                <a href="javascript:(window.history.go(-1));" class="bt-back" title="Voltar">
                  <span>Voltar</span>
                </a>
                
              </section> <!-- corpo de conteudo -->
            </div>
            
          </div> <!-- Content -->

        </div> <!-- END: Class Wrapper -->

      </div> <!-- END: Content-Wrapper -->

    </div> <!-- END: Main -->

  </div><!-- END: Wrapper -->

  <?php
    include "../../inc/footer.php";
    include "../../inc/scripts.php";
    include "../../inc/scripts-internas.shtml";
  ?>
  <script src="services-menu-control.js"></script>
</body>
</html>