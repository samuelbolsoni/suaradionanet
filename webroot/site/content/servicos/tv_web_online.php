<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml"
      lang="pt-br">
	<?php
	require_once "../../inc/verificastatus.php";
 ?>
<head>
		<base href="<?php echo $url_base; ?>/servicos/"/>
		<link rel="stylesheet" type="text/css" href="planos_radios.css">
		<?php
			include "../../inc/head.php";
 ?>
	</head>

	<body>
		<div id="wrapper" class="internal services">
			<!-- Wrapper -->

			<div id="main">
				<!-- Main -->

				<?php include "../../inc/header_servicos.php" ?>

				<div id="content-wrapper">
					<!-- Content-Wrapper -->

					<div class="wrapper">
						<!-- Class Wrapper -->

						<div id="content" class="clearfix">
							<!-- Content -->

							<div class="content-bottom clearfix">
								<section class="content dropdown-infos">
									<!-- corpo de conteudo -->

									<div class="content-block">
										<!-- Menu lateral -->
										<?php
											include "menu_servicos.php";
										?>
										<!-- fim - Menu lateral -->
										<!-- Conteúdo -->
										<h3 class="font01 titles-services tv-online" data-menu="tv-web-online">TV Web Online</h3>
										<div class="text">
											A cada minuto a internet está atuante no mundo. A integração dela com o cotidiano se tornou indispensável. O
											streaming para TV PELA WEB se popularizou e chega também a ambientes não corporativos. Muitas pessoas gostariam
											de ter uma WEB TV ONLINE, mas encontram dificuldades para mostrar seu trabalho devido a falta de espaço no mercado.
											A internet possibilitou isso, com uma conexão BANDA LARGA em sua casa ou local de trabalho, é possível ter sua própria
											TV ONLINE. Portais, sites pessoais e organizações não governamentais, cada vez mais usam a internet para transmitir uma
											programação diferenciada e segmentada aos ouvintes de ciberespaço. Tenha sua própria TV WEB sem ortoga da ANATEL, órgão
											governamental responsável pela radiodifusão no Brasil. Transmissão de TV pela WEB é legal e não precisa de autorização
											do governo. Aprenda a montar uma TV AO VIVO pela internet com um custo baixo e com total suporte da SUA RÁDIO NA NET.
											<br />
											<br />
											Em todos os planos você terá gratuitamente estatísticas de ouvintes e tráfego ilimitado.
											<div class="cAll"></div>
											<h3 class="font01 sub-titles">SISTEMA WINDOWS MEDIA AUDIO</h3>
											<div class="table-wrapper planosAccShout planosWonline">
												<table class="table">
													<thead>
														<tr>
															<th width="258"><img src="../../img/misc/planos_radios-wma.png" style="margin-left: 40px; float: left" /></th>
															<th>Plano 1</th>
															<th>Plano 2</th>
															<th>Plano 3</th>
															<th>Plano 4</th>
															<th>Plano 5</th>
															<th>Plano 6</th>
															<th>Plano 7</th>
															<th class="last">Plano 8</th>
														</tr>
														<tr>
															<th colspan="9" class="containSubtable">
															<table class="subtable">
																<tr>
																	<td width="170" class="first">Quantidade de Ouvintes Simultâneos</td>
																	<?php
																	function addzeros($valorplano) {
																		if (strpos($valorplano, '.')) {
																			$valorplano = $valorplano . "0";
																			$valorplano = str_replace('.', ',', $valorplano);
																		} else {
																			if ($valorplano != NULL) {
																				$valorplano = $valorplano . ",00";
																			}
																		}
																		return $valorplano;
																	}
																	
																	$query1 = mysql_query("SELECT p.ouvintes AS numOuvintes, o.titulo FROM site_planos AS p, site_ouvintes AS o WHERE p.streaming = 3 AND p.ouvintes = o.id GROUP BY o.titulo ORDER BY o.titulo ASC");
																	$i = 1;
																	// $count = mysql_query("SELECT count(p.id) as count FROM site_planos AS p, site_ouvintes AS o WHERE p.streaming = 4 AND p.ouvintes = o.id GROUP BY o.titulo ORDER BY o.titulo ASC");
																	// $count = mysql_fetch_array($count);
																	// $count = $count['count'];
																	$count = 8;


																	while ($rs = mysql_fetch_array($query1)) {
																		$idouvintes = $rs['numOuvintes'];
																		$tituloouvintes = $rs['titulo'];

																		switch ($i) {
																			case 1 :
																				echo "<td width=\"54\">" . $tituloouvintes . "</td>";
																				break;
																			case 2 :
																			case 4 :
																			case 5 :
																			case 7 :
																				echo "<td width=\"53\">" . $tituloouvintes . "</td>";
																				break;
																			case 3 :
																				echo "<td width=\"51\">" . $tituloouvintes . "</td>";
																				break;
																			case 6 :
																				echo "<td width=\"57\">" . $tituloouvintes . "</td>";
																				break;
																			case $i == $count :
																				echo "<td width=\"63\" class=\"last\" >" . $tituloouvintes . "</td>";
																				echo "</tr>
																	</table>
																	</th>
																	</tr>
																	</thead>
																	<tbody>";
																				break;
																		}

																		$i++;

																	}
																	?>
														<tr class="lastItem">
															<?
															$query2 = mysql_query("SELECT a.id as id, a.dt_cad as dt_cad , b.titulo as ouvintes, b.id as id_ouvintes, c.id as id_qualidade,
 c.titulo as qualidade, d.titulo as streaming FROM site_planos as a, site_ouvintes as b, site_qualidade as c, site_streaming as d 
 WHERE b.id = a.ouvintes AND c.id = a.qualidade AND d.id = a.streaming AND a.streaming = 3 and b.id= 22 order by c.id limit 10");
																	$i = 1;

																	while ($rs2 = mysql_fetch_array($query2)) {
																		$idqualidade = $rs2['id_qualidade'];
																		$tituloqualidade = $rs2['qualidade'];

																		echo "<tr>";
																		echo "<td class=\"first\">Qualidade " . $tituloqualidade . "</td>";
																		$query3 = mysql_query("SELECT pl.id,pl.valor,pl.ouvintes,ou.titulo from `site_planos` pl, `site_ouvintes` ou where qualidade='$idqualidade' AND streaming=3 and pl.ouvintes = ou.id order by ISNULL(valor), ou.titulo asc limit 10");
																		while ($rs3 = mysql_fetch_array($query3)) {

																			$idplano = $rs3['id'];
																			$valorplano = $rs3['valor'];

																			//echo $valorplano. "<br/>" ;
																			if ($valorplano >= 41.90) {
																				$valorplano = addzeros($valorplano);
																				echo "<td><a href=\"../../../../../cadastro.htm/" . $idplano . "\">" . $valorplano . "<sup>*</sup></a></td>";
																			} else {
																				$valorplano = addzeros($valorplano);
																				if ($valorplano == NULL) {
																					echo "<td class=\"consult\"><a href=\"../../../../../cadastro.htm/" . $idplano . "\">Consultar</a></td>";
																				} else {
																					echo "<td><a href=\"../../../../../cadastro.htm/" . $idplano . "\">" . $valorplano . "</a></td>";
																				}
																			}

																		}
																		echo "</tr>";
																	}
																	?> 
																	</tbody>
															</table>
											</div><!-- END: table-wrapper -->
											<figure class="webonline-imgs">
												<a href="../../../../../site_administravel.htm">
												  <img src="../../img/misc/planos_radios-site2.jpg" alt="Site Administravel 2.0" title="Site Administrável 2.0" />
												</a>
												<a href="../../../../../cadastro.htm">
												  <img class="webonline-02" src="../../img/misc/oferta-coberta.jpg" alt="Cobrimos a oferta da concorrência" title="Cobrimos a oferta da concorrência" />
												</a>
											</figure>
											<section class="varied-information">
												<header class="font01 sub-titles">
													O que você precisa ter e o que garantimos para colocar sua TV ONLINE?
												</header>
												<article>
													A operação da Web TV é feita através de seu pc, ao vivo ou gravada.
												</article>
												<article>
													O PC da TV Web deve ter no mínimo 2 GB de memória e processador acima de 1 GHZ. Será necessário a instalação do Média Encoder para enviar a imagem de sua Web Can ou placa de captura.
												</article>
												<article>
													Indispensável conexão de banda larga de no mínimo 300k  de upload para enviar em 109k ou mais.
												</article>
												<article>
													Garantimos 99,99% de uptime, ou seja, permanência de sua TV no ar por mês.
												</article>
												<article>
													A instalação é fácil com tutoriais passo a passo para a instalação dos softwares. Caso não consiga instalar, fazemos a instalação em seu pc via acesso remoto.
												</article>
												<article>
													Temos suporte via chat de segunda a sexta-feira das 8h às 24 horas. 
												</article>
											</section>
										</div>
										<!-- fim - Conteúdo -->
									</div>

									<a href="javascript:(window.history.go(-1));" class="bt-back" title="Voltar">
	                  <span>Voltar</span>
	                </a>

								</section>
								<!-- corpo de conteudo -->
							</div>

						</div>
						<!-- Content -->

					</div>
					<!-- END: Class Wrapper -->

				</div>
				<!-- END: Content-Wrapper -->

			</div>
			<!-- END: Main -->

		</div><!-- END: Wrapper -->

		<?php
		include "../../inc/footer.php";
		include "../../inc/scripts.php";
		include "../../inc/scripts-internas.shtml";
		?>
		<script src="services-menu-control.js"></script>
		<script src="services-control-hover-table.js"></script>
		<script type="text/javascript">
      $('.table td:last-child').css({
        background : 'none'
      })
    </script>
	</body>
</html>