<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml"
      lang="pt-br">
<?php require_once "../../inc/verificastatus.php"; ?>
<head>
  <base href="<?php echo $url_base;?>/servicos/"/>
  <?php include "../../inc/head.php"; ?>
</head>

<body>
  <div id="wrapper" class="internal services"> <!-- Wrapper -->

    <div id="main"> <!-- Main -->

      <?php include "../../inc/header_servicos.php" ?>

      <div id="content-wrapper"> <!-- Content-Wrapper -->

        <div class="wrapper"> <!-- Class Wrapper -->

          <div id="content" class="clearfix"> <!-- Content -->

            <div class="content-bottom clearfix">
              <section class="content dropdown-infos"> <!-- corpo de conteudo -->

                <div class="content-block">
                  <!-- Menu lateral -->
                  <?php include "menu_servicos.php"; ?>
                  <!-- fim - Menu lateral -->
                  <!-- Conteúdo -->
                  <h3 class="font01 titles-services fire" data-menu="site-administravel">Site Administrável</h3>
                  <div class="text">
                    O site administrável grátis com hospedagem grátis é uma opção que a Sua Rádio na Net oferece em todos os planos de streaming a partir de R$ 35,10/mês.
                    <br /><br />
                    Se sua emissora ainda não está na internet ou possui um site que não atende as necessidades de seus ouvintes, temos as ferramentas certas para interação com os internautas.
                    <br /><br />
                    Conheça as duas versões disponíveis:
                    <p class="font01 compare">COMPARE AS VERSÕES</p>
                    <div class="cAll"></div>
                    <div class="table-siteadmin">
                      <table id="siteAdmin-1">

                        <thead>
                          <tr class="first-line" height="85">
                            <th class="first-th-admin" width="245">NÍVEL DAS FUNCIONALIDADES</th>
                            <th width="65">NORMAL</th>
                            <th width="65">AVANÇADO</th>
                          </tr>
                          <tr class="siteAdmin-bg">
                            <th width="375" height="31" colspan="3">
                              <span class="site-administravel">SITE ADMINISTRÁVEL 1.0</span>
                            </th>
                          </tr>
                        </thead>

                        <tbody class="body-administ">
                          <tr height="50">
                            <td width="245">PROGRAMAÇÃO MUSICAL</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                            <td class="space-td">-</td>
                          </tr>
                          <tr height="50">
                            <td width="245">SUPORTE A BANNERS</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                            <td class="space-td">-</td>
                          </tr>
                          <tr height="50">
                            <td width="245">BANNERS EXTRAS (LATERAIS)</td>
                            <td><img class="icon-lack" src="../../img/icons/check-no.png" alt="não possui" title="não possui"/></td>
                            <td class="space-td">-</td>
                          </tr>
                          <tr height="50">
                            <td width="245">NOTÍCIAS</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                            <td class="space-td">-</td>
                          </tr>
                          <tr height="50">
                            <td width="245">NOTÍCIAS AUTOMÁTICAS RSS</td>
                            <td><img class="icon-lack" src="../../img/icons/check-no.png" alt="não possui" title="não possui"/></td>
                            <td class="space-td">-</td>
                          </tr>
                          <tr height="50">
                            <td width="245">GALERIA DE FOTOS</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                            <td class="space-td">-</td>
                          </tr>
                          <tr height="50">
                            <td width="245">GALERIA DE VÍDEOS</td>
                            <td><img class="icon-lack" src="../../img/icons/check-no.png" alt="não possui" title="não possui"/></td>
                            <td class="space-td">-</td>
                          </tr>
                          <tr height="50">
                            <td width="245">PÁGINA DE EVENTOS</td>
                            <td><img class="icon-lack" src="../../img/icons/check-no.png" alt="não possui" title="não possui"/></td>
                            <td class="space-td">-</td>
                          </tr>
                          <tr height="50">
                            <td width="245">PÁGINA DE PROMOÇÕES</td>
                            <td><img class="icon-lack" src="../../img/icons/check-no.png" alt="não possui" title="não possui"/></td>
                            <td class="space-td">-</td>
                          </tr>
                          <tr height="50">
                            <td width="245">HORÓSCOPO DO AUTOMÁTICO</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                            <td class="space-td">-</td>
                          </tr>
                          <tr height="50">
                            <td width="245">PREVISÃO DO TEMPO</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                            <td class="space-td">-</td>
                          </tr>
                          <tr height="50">
                            <td width="245">SUPORTE REDES SOCIAIS</td>
                            <td><img class="icon-lack" src="../../img/icons/check-no.png" alt="não possui" title="não possui"/></td>
                            <td class="space-td">-</td>
                          </tr>
                          <tr height="50">
                            <td width="245">ENQUETE</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                            <td class="space-td">-</td>
                          </tr>
                          <tr height="50">
                            <td width="245">TOP 10 MÚSICA</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                            <td class="space-td">-</td>
                          </tr>
                          <tr height="50">
                            <td width="245">TOP VÍDEOS (VIMEO/YOUTUBE)</td>
                            <td><img class="icon-lack" src="../../img/icons/check-no.png" alt="não possui" title="não possui"/></td>
                            <td class="space-td">-</td>
                          </tr>
                          <tr height="50">
                            <td width="245">PEDIDO MUSICAL</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                            <td class="space-td">-</td>
                          </tr>
                          <tr height="50">
                            <td width="245">MURAL DE RECADOS</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                            <td class="space-td">-</td>
                          </tr>
                          <tr height="50">
                            <td width="245">CONTADOR DE OUVINTES</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                            <td class="space-td">-</td>
                          </tr>
                        </tbody>

                      </table>
                      <a href="../../../../../cadastro.htm/" class="bt-info-assinar"></a>
                    </div>

                    <div class="compare-wrap">
                      <button class="bt-compare" title="Compare">COMPARE</button>
                    </div>

                     <div class="table-siteadmin">
                      <table id="siteAdmin-2">

                        <thead>
                          <tr class="first-line" height="85">
                            <th class="first-th-admin" width="245">NÍVEL DAS FUNCIONALIDADES</th>
                            <th width="65">NORMAL</th>
                            <th width="65">AVANÇADO</th>
                          </tr>
                          <tr class="siteAdmin-bg">
                            <th width="375" height="31" colspan="3">
                              <span class="site-administravel">SITE ADMINISTRÁVEL 2.0</span>
                            </th>
                          </tr>
                        </thead>

                        <tbody class="body-administ">
                          <tr height="50">
                            <td width="245">PROGRAMAÇÃO MUSICAL</td>
                            <td class="space-td">-</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                          </tr>
                          <tr height="50">
                            <td width="245">SUPORTE A BANNERS</td>
                            <td class="space-td">-</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                          </tr>
                          <tr height="50">
                            <td width="245">BANNERS EXTRAS (LATERAIS)</td>
                            <td class="space-td">-</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                          </tr>
                          <tr height="50">
                            <td width="245">NOTÍCIAS</td>
                            <td class="space-td">-</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                          </tr>
                          <tr height="50">
                            <td width="245">NOTÍCIAS AUTOMÁTICAS RSS</td>
                            <td class="space-td">-</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                          </tr>
                          <tr height="50">
                            <td width="245">GALERIA DE FOTOS</td>
                            <td class="space-td">-</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                          </tr>
                          <tr height="50">
                            <td width="245">GALERIA DE VÍDEOS</td>
                            <td class="space-td">-</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                          </tr>
                          <tr height="50">
                            <td width="245">PÁGINA DE EVENTOS</td>
                            <td class="space-td">-</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                          </tr>
                          <tr height="50">
                            <td width="245">PÁGINA DE PROMOÇÕES</td>
                            <td class="space-td">-</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                          </tr>
                          <tr height="50">
                            <td width="245">HORÓSCOPO DO AUTOMÁTICO</td>
                            <td class="space-td">-</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                          </tr>
                          <tr height="50">
                            <td width="245">PREVISÃO DO TEMPO</td>
                            <td class="space-td">-</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                          </tr>
                          <tr height="50">
                            <td width="245">SUPORTE REDES SOCIAIS</td>
                            <td class="space-td">-</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                          </tr>
                          <tr height="50">
                            <td width="245">ENQUETE</td>
                            <td class="space-td">-</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                          </tr>
                          <tr height="50">
                            <td width="245">TOP 10 MÚSICA</td>
                            <td class="space-td">-</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                          </tr>
                          <tr height="50">
                            <td width="245">TOP VÍDEOS (VIMEO/YOUTUBE)</td>
                            <td class="space-td">-</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                          </tr>
                          <tr height="50">
                            <td width="245">PEDIDO MUSICAL</td>
                            <td class="space-td">-</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                          </tr>
                          <tr height="50">
                            <td width="245">MURAL DE RECADOS</td>
                            <td class="space-td">-</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                          </tr>
                          <tr height="50">
                            <td width="245">CONTADOR DE OUVINTES</td>
                            <td class="space-td">-</td>
                            <td><img class="icon-have" src="../../img/icons/check.png" alt="possui" title="possui"/></td>
                          </tr>
                        </tbody>

                      </table>
                      <a href="../../../../../cadastro.htm/" class="bt-info-assinar"></a>
                    </div>

                  </div>
                  <!-- fim - Conteúdo -->
                </div>

                <a href="javascript:(window.history.go(-1));" class="bt-back" title="Voltar">
                  <span>Voltar</span>
                </a>

              </section> <!-- corpo de conteudo -->
            </div>

          </div> <!-- Content -->

        </div> <!-- END: Class Wrapper -->

      </div> <!-- END: Content-Wrapper -->

    </div> <!-- END: Main -->

  </div><!-- END: Wrapper -->

  <?php
    include "../../inc/footer.php";
    include "../../inc/scripts.php";
    include "../../inc/scripts-internas.shtml";
  ?>
  <script src="services-menu-control.js"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      var winHeight = window.innerHeight;
      var halfWinHeight = winHeight / 2;
      $('.bt-compare').css({
        position : 'absolute',
        top: 0
      });
      var btPosTop = $('.bt-compare').offset().top - 10;
      var limitPos = $('.compare-wrap').height() + btPosTop - $('.bt-compare').height();
      if(window.pageYOffset) {
        var vscroll = window.pageYOffset;
      } else {
        var vscroll = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
      }
      var diff = vscroll - btPosTop;
      if(vscroll >= btPosTop && vscroll <= limitPos) {
        $('.bt-compare').css({
          position : 'absolute',
          top: diff
        });
      }
      $(window).scroll(function(e) {
        if(window.pageYOffset) {
          var vscroll = window.pageYOffset;
        } else {
          var vscroll = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
        }
        var diff = vscroll - btPosTop;
        if(vscroll >= btPosTop && vscroll <= limitPos) {
          $('.bt-compare').css({
            position : 'absolute',
            top: diff
          });
        }
        if(vscroll < btPosTop) {
          $('.bt-compare').css({
            top: 0
          });
        }
      });

    });

  </script>
</body>
</html>