<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml"
      lang="pt-br">
<?php require_once "../../inc/verificastatus.php"; ?>
<head>
  <base href="<?php echo $url_base;?>/empresa/"/>
  <?php include "../../inc/head.php"; ?>
</head>

<body>
  <div id="wrapper" class="internal servers"> <!-- Wrapper -->

    <div id="main"> <!-- Main -->
      
      <?php include "../../inc/header_servidores.php"; ?>

      <div id="content-wrapper"> <!-- Content-Wrapper -->

        <div class="wrapper"> <!-- Class Wrapper -->
          
          <div id="content" class="clearfix"> <!-- Content -->
            
            <div class="content-bottom clearfix">
              <section class="content dropdown-infos"> <!-- corpo de conteudo -->
                
                <div class="text">
                  <figure class="illustration">
                    <img alt="servidores" class="image" src="../../img/temp/temp002.jpg" title="Servidores" /><!-- Largura estática 252 e algura dinâmica -->
                  </figure>
                  Sem servidores de alta performance é impossível gerar som de qualidade e estável na internet. Muitas empresas de streaming oferecem planos mirabolantes tentando atrair novos clientes. Tenha cuidado com armadinhas, planos de ouvintes ilimitados ou com capacidade para milhares de ouvintes por um valor baixo não existem. Muitas empresas utilizam essa artimanha por saberem que dificilmente a audiência de uma rádio web ira ultrapassar em sua grande maioria, os 500 ouvintes conectados simultâneamente.
                  <br /><br />
                  Pensando em qualidade para o cliente é que a <strong><u>SUA RÁDIO NA NET</u></strong> possui servidores nos Estados Unidos nos maiores  Data Center do mundo. Com diversos servidores garantimos a qualidade de nossas conexões, mantendo sua emissora on-line 99,99%. *Além disso nossas conexões não possuem picotes ou falhas na transmissão. Faça parte de um grupo seleto de clientes que investiram na <strong><u>SUA RÁDIO NA NET</u></strong>.
                  <br /><br />
                  Entre em <a style="color: #494949;" href="../../../../../contato.html"><strong><u>contato</u></strong></a> e faça um teste sem compromisso.
                  <br /><br />
                  <i>* Internautas em conexões discadas de baixa qualidade ou ADSL com rendimento abaixo do oferecido pela prestadora, podem ter picotes ou falhas no som. Tais problemas não estão relacionados aos nossos servidores, mas a falta de condições técnicas das prestadoras locais.</i>
                </div>
                
                <a href="javascript:(window.history.go(-1));" class="bt-back" title="Voltar">
                  <span>Voltar</span>
                </a>
                
              </section> <!-- corpo de conteudo -->
            </div>
            
          </div> <!-- Content -->

        </div> <!-- END: Class Wrapper -->

      </div> <!-- END: Content-Wrapper -->

    </div> <!-- END: Main -->

  </div><!-- END: Wrapper -->

 <?php
    include "../../inc/footer.php";
    include "../../inc/scripts.php";
    include "../../inc/scripts-internas.shtml";
  ?>

</body>
</html>