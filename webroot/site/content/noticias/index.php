<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml"
      lang="pt-br">
<?php
	require_once "../../inc/verificastatus.php";
?>
<head>
	<base href="<?php echo $url_base;?>/noticias/"/>
  <meta charset="utf-8"/>
  <title>Radios</title>
  <link rel="stylesheet" href="../../css/3rd_party/normalize.css" />
  <link rel="stylesheet" href="../../css/3rd_party/sexy/sexy.css" />
  <link rel="stylesheet" href="../../css/3rd_party/colorbox/colorbox.css" />
  <link rel="stylesheet" href="../../css/core/style.css" />
  <script src="../../js/core/crossbrowser/crossbrowser-0.2.js"></script>
  
  <!--[if lt IE 9]>
    <script src="../../js/3rd_party/html5shiv.js"></script>
  <![endif]-->

</head>

<body>
  <div id="wrapper" class="internal news"> <!-- Wrapper -->

    <div id="main"> <!-- Main -->
      
      <!--#include virtual="../../inc/header_noticias.shtml" -->
      <?php include "../../inc/header_noticias.php"; ?>

      <div id="content-wrapper"> <!-- Content-Wrapper -->

        <div class="wrapper"> <!-- Class Wrapper -->
          
          <div id="content" class="clearfix"> <!-- Content -->
            
            <div class="content-bottom clearfix">
              <section  class="content dropdown-infos"> <!-- corpo de conteudo -->
                <div id="section">
                <?php
                $query_noticias = mysql_query("SELECT *, 
                															DATE_FORMAT(data, '%d/%m/%Y') AS data2,
                															DATE_FORMAT(hora, '%Hh%im') AS hora2
                															FROM site_noticias ORDER BY data DESC, hora DESC LIMIT 0, 10");
                $open = 0;
                while($result = mysql_fetch_array($query_noticias)){
                  $newsId = $result['id'];
                	//$dt_cad = $result['dt_cad'];
									$dt_cad = date("Y-m-d");
									$data = strtotime($dt_cad) - strtotime($result['data']);
									$data = round( $data / 86400 );
									$hora = $result['hora2'];
                	$texto = $result['texto'];
                	$titulo = $result['nome'];
                	$img = $result['imagem'];
                	$imagem_final = "";
									if($img != "" && $img != 0){
										$query_imagem = mysql_fetch_array(mysql_query("SELECT * FROM manager_imagens WHERE id = $img"));
										$imagem_final = $query_imagem['imagem'];
									}
                	?>
                	<article id="<?php echo $newsId; ?>" class="dropdow-container">
                  <div class="bg-top"></div>
                  <header>
                    <a href="javascript:;" <?php if($open == 0){?> class="open" <?php } ?> >
                      <h1 class="font01">
                        <span class="bt-control plus"></span>
                        <?php echo $titulo; ?>
                      </h1>
                    </a>
                  </header>
                  <div class="text-container">
                    <p class="data">Publicado: <?php if($data > 0){ echo $data; ?> dia(s) atrás <?php }else { echo " hoje";} ?> às <?php echo $hora; ?></p>
                    <div class="text">
                      <?php
                  				if($imagem_final != "" && file_exists("../../../uploads/img/noticias/thumb_".$imagem_final)){
                  			?>
                      <div class="img-wrapper">
                        <!-- a imagem deve ter 250 x 250 -->
                        <!-- <img src="" /> -->
                        	<img src="../../../uploads/img/noticias/thumb_<?php echo $imagem_final; ?>" />

                      </div>
                     <?php
                     }
                     	echo $texto;
                     ?>
                    </div>
                  </div>
                  <footer>
                  </footer>
                </article>
                	<?php
                	$open++;
                }
                ?>
               </div>
                <input type="hidden" id="num" value="<?php echo $open; ?>"/>
                <a href="javascript:(window.history.go(-1));" class="bt-back" title="Voltar">
                  <span>Voltar</span>
                </a>
                
                <div class="loader">
                  <img src="../../img/loader.gif" alt="Loader" title="Loader" />
                </div>
                
              </section> <!-- corpo de conteudo -->
            </div>
            
          </div> <!-- Content -->

        </div> <!-- END: Class Wrapper -->

      </div> <!-- END: Content-Wrapper -->

    </div> <!-- END: Main -->

  </div><!-- END: Wrapper -->
  
  <?php
	  include "../../inc/footer.php";
		include "../../inc/scripts.php";
		include "../../inc/scripts-internas.shtml";
	?>
	<script src="../../js/paginador.js"></script>
  <!--#include virtual="../../inc/footer.shtml" -->
  <!-- Include de Javascripts -->
  <!--#include virtual="../../inc/scripts.shtml" -->
  <!-- Include de scripts especificos para as internas -->
  <!--#include virtual="../../inc/scripts-internas.shtml" -->
	<input id='pagina' value="0" type="hidden" />
</body>
</html>