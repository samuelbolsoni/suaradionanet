<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml"
      lang="pt-br">
<?php require_once "../../inc/verificastatus.php"; ?>
<head>
  <base href="<?php echo $url_base;?>/empresa/"/>
  <?php include "../../inc/head.php"; ?>
</head>

<body>
  <div id="wrapper" class="internal enterprise"> <!-- Wrapper -->

    <div id="main"> <!-- Main -->
      
      <?php include "../../inc/header_empresa.php"; ?>

      <div id="content-wrapper"> <!-- Content-Wrapper -->

        <div class="wrapper"> <!-- Class Wrapper -->
          
          <div id="content" class="clearfix"> <!-- Content -->
            
            <div class="content-bottom clearfix">
              <section class="content dropdown-infos"> <!-- corpo de conteudo -->
                
                <div class="text">
                  <figure class="illustration">
                    <img alt="empresa" class="image" src="../../img/misc/empresa.jpg" title="Empresa" />
                  </figure><!-- Largura estática 252 e algura dinâmica-->
                  Somos uma empresa com experiência no mercado de rádios e TV on-line. Nossa equipe é formada por profissionais do rádio e TI com mais de 20 anos de experiência e atua de maneira eficiente, buscando atender as necessidades de cada emissora em particular. A internet e o avanço da tecnologia possibilitam que a <strong><u>SUA RÁDIO NA NET</u></strong> coloque a serviço de sua empresa o que existe de mais moderno em brodcasting profissional. Nós não apenas ampliamos a capacidade de ouvintes e levamos o som de sua emissora para o mundo globalizado, mas também possibilitamos a captação de novos anunciantes.
                  <br /><br />
                  Oferecemos para nossos clientes: Competência, planejamento e qualidade em streaming. Não basta apenas colocar o som de uma rádio ou a imagem de sua TV na internet, é preciso garantir ela 99,99% on-line.
                  <br /><br />
                  A <strong><u>SUA RÁDIO NA NET</u></strong> investe a cada ano em novos servidores e em mão de obra qualificada, ampliando a atuação no meio cibernético. Somos uma empresa legal, com CNPJ e nota fiscal, apta a participar de licitações públicas ou privadas.
                  <br /><br />
                  Coloque SUA RÁDIO na internet e entre para um mundo de novos negócios.
                  <br /><br /> 
                  Nós possibilitamos isso para você. <strong><u>SUA RÁDIO NA NET</u></strong>, uma empresa que ouve o que você faz.
                </div>
                
                <a href="javascript:(window.history.go(-1));" class="bt-back" title="Voltar">
                  <span>Voltar</span>
                </a>
                
              </section> <!-- corpo de conteudo -->
            </div>
            
          </div> <!-- Content -->

        </div> <!-- END: Class Wrapper -->

      </div> <!-- END: Content-Wrapper -->

    </div> <!-- END: Main -->

  </div><!-- END: Wrapper -->

  <?php
    include "../../inc/footer.php";
    include "../../inc/scripts.php";
    include "../../inc/scripts-internas.shtml";
  ?>

</body>
</html>