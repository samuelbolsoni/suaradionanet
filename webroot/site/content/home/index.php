<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml"

      xmlns:og="http://ogp.me/ns#"

      xmlns:fb="https://www.facebook.com/2008/fbml"

      lang="pt-br">

<?php require_once "../../inc/verificastatus.php"; ?>

<head>

	<base href="<?php echo $url_base;?>/home/"/>

  <?php include "../../inc/head.php"; ?>

  <link rel="stylesheet" href="../../css/3rd_party/sexy/sexy.css" />

  <link rel="stylesheet" href="../../css/3rd_party/colorbox/colorbox.css" />

</head>



<body>

  <div id="wrapper"> <!-- Wrapper -->



    <div id="main"> <!-- Main -->



      <?php

      include "../../inc/header.php";

			include "../../inc/carousel.php";

      ?>



      <div id="content-wrapper"> <!-- Content-Wrapper -->



        <div id="menu-infos" class="clearfix"> <!-- Menu Infos -->

          <ul>

            <li class="first"><a href="javascript:;" class="conheca" data-rel="conheca"><span>Conheça</span></a></li>

            <li><a href="javascript:;" class="planos" data-rel="planos"><span>Planos</span></a></li>

            <li><a href="javascript:;" class="site1" data-rel="site1"><span>Site 1.0</span></a></li>

            <li><a href="javascript:;" class="site2" data-rel="site2"><span>Site 2.0</span></a></li>

            <li><a href="javascript:;" class="assinar" data-rel="assinar"><span>Assinar</span></a></li>

          </ul>

        </div> <!-- END: Menu Infos -->



        <div id="content-infos" class=""> <!-- Content Infos -->

          <div class="content clearfix" data-rel="conheca">

            <!--#include virtual="inc/infos-home/conheca.shtml" -->

            <?php include "../../inc/infos-home/conheca.shtml" ?>

          </div>

          <div class="content clearfix" data-rel="planos">

            <!--#include virtual="inc/infos-home/planos.shtml" -->

            <?php include "../../inc/infos-home/planos.php" ?>

          </div>

          <div class="content clearfix" data-rel="site1">

            <!--#include virtual="inc/infos-home/site1.shtml" -->

            <?php include "../../inc/infos-home/site1.shtml" ?>

          </div>

          <div class="content clearfix" data-rel="site2">

            <!--#include virtual="inc/infos-home/site2.shtml" -->

            <?php include "../../inc/infos-home/site2.shtml" ?>

          </div>

          <div class="content clearfix" data-rel="assinar">

            <!--#include virtual="inc/infos-home/assinar.shtml" -->

            <?php include "../../inc/infos-home/assinar.shtml" ?>

          </div>

          <a href="" class="btn-close">

            <span>fechar</span>

          </a>

        </div> <!-- END: Content Infos -->



        <div class="wrapper"> <!-- Class Wrapper -->



          <div id="content" class="clearfix"> <!-- Content -->



            <div class="grid3">

              <div class="bg-novidades">

                <section id="novidades">

                  <header>

                    <h3 class="font01">

                      Novidades

                    </h3>

                  </header>

                  <?php

                  $count = 0;

                  $limit = 5;

                  $query_noticias = mysql_query("SELECT *,

                  														DATE_FORMAT(data, '%d/%m/%Y') AS data2

                															FROM site_noticias ORDER BY data DESC, hora DESC LIMIT 0,$limit");

                  while($result = mysql_fetch_array($query_noticias)){

                    $class = "";

                    if($count == 0) {

                      $class = "class=\"first\"";

                    }

                    $count++;

                    if($count == $limit) {

                      $class = "class=\"last\"";

                    }

                    $newsId = $result['id'];

                  	$data = $result['data'];

                    $texto = $result['nome'];

										$texto_curto = mb_substr($texto, 0, 62, "UTF-8");

										$d = new DOMDocument;

                  	@$d->loadHTML(utf8_decode($texto_curto));

				   					$texto_curto = preg_replace('~<(?:!DOCTYPE|/?(?:html|body|head|p))[^>]*>\s*~i', '', $d->saveHTML());

				   					$texto_curto = utf8_decode($texto_curto);

										$str = strlen($texto);

										?>

										<article <?php echo $class; ?>>

                      <a href="../../../../../noticias.htm#<?php echo $newsId ?>" class="link-news">

  	                    <span class="data">

  	                      <?php echo strftime("%d %b %Y", strtotime($data)); ?>

  	                    </span>

  	                     - <?php echo  $texto_curto; if($str > 62){ echo "..."; } ?>

                      </a>

	                  </article>

										<?php

                  }

                  ?>

                </section>

              </div>



              <div class="bg-qualidade">

                <div class="text-container">

                  <p class="title font01">

                    COMPROVE A QUALIDADE

                  </p>

                  <p class="text">

                    Seus ouvintes terão acesso a qualidade de áudio em alta definição (HD).

                  </p>

                  <a href="<?php echo $url_site; ?>/qualidade.htm/shoutcast" class="bt-player bt-shout" rel="shoutcast"></a>

                  <a href="<?php echo $url_site; ?>/qualidade.htm/wmp" class="bt-player bt-wmp" rel="wmp"></a>

                  <a href="<?php echo $url_site; ?>/qualidade.htm/aacplus" class="bt-player bt-aac" rel="aacplus"></a>

                </div>

              </div>



            </div>



            <div class="grid3">

              <div class="text-container config">

                <p class="title font01">

                  Veja Como é Fácil Configurar

                </p>

                <p class="text">

                  Veja como é fácil configurar sua rádio na net, em poucos minutos você poderá ter uma rádio com a sua cara e a sua programação no ar, 24 horas por dia.<br />

<br />

                  Flexibiliade e facilidade no Gerenciador de Conteúdo para manter a sua rádio totalmente atualizada e garantir muitos acessos.

                </p>

                <a href="../../../../../tutoriais.htm" class="video" alt="Veja Como é Fácil Configurar" title="Veja Como é Fácil Configurar">

                  <img src="../../img/misc/video.png" />

                </a>

              </div>



              <div class="bg-colors blue auto-dj">

                <a href="../../../../../auto_dj.htm" class="containerBox">

                  <div class="text-container">

                    <p class="title font01">

                      Auto Dj Grátis

                    </p>

                    <p class="text">

                      Assine um plano de streaming a partir de R$ 31,90 e ganhe totalmente grátis o sistema de auto-dj para sua rádio.

                    </p>

                  </div>

                </a>

                <span class="icon"></span>

              </div>



              <div class="bg-colors red duvidas">

                <a href="../../../../../faq_streaming.htm" class="containerBox">

                  <div class="text-container">

                    <p class="title font01">

                      Dúvidas Ainda?

                    </p>

                    <p class="text">

                      Como configurar uma rádio web? Neste espaço reservamos todas as informações de como usar softwares e irradiar sua emissora via internet facilmente.

                    </p>

                  </div>

                </a>

                <span class="icon"></span>

              </div>

            </div>



            <div class="grid3">

              <div class="text-container chat">

                <p class="title font01">

                  Chat ao Vivo

                </p>

                <p class="text">

                  Tire suas dúvidas através de nosso suporte via chat, em tempo real. Nossa equipe está pronta para lhe atender e resolver rapidamente sua dúvida e/ou problema.

                </p>

                <a class="bt-chat-status" href="javascript:;" onclick="chat_online();">

                	<img src="http://www.suaradionanet.com/atendimento_aovivo/image.php?id=02&type=inlay" width="53" height="22" border="0" alt="LiveZilla Live Help" />

                </a>

                <!-- <a href="javascript:;" class="bt-chat-status offline"></a> -->

              </div>



              <div class="text-container atendimento">

                <p class="title font01">

                  Atendimento Direto

                </p>

                <p class="text">

                  Segunda à Sexta de 9h às 17h30<br />

                  <strong>SP:</strong> (11) 4063 8601 - São Paulo<br />

                  <strong>RS:</strong> (51) 4063 8108 - Porto Alegre<br />

                  <strong>BA:</strong> (71) 4062 9270 - Bahia <br />

                </p>

              </div>



              <div class="bg-colors green administravel">

                <a href="../../../../../site_administravel.htm" class="containerBox">

                  <div class="text-container">

                    <p class="title font01">

                      Site Administrável Grátis

                    </p>

                    <p class="text">

                      Assine um plano de streaming a partir de R$ 35,10 e ganhe totalmente grátis um site administrável para sua rádio.

                    </p>

                  </div>

                </a>

                <span class="icon"></span>

              </div>



              <div class="bg-colors orange visit">

                <a href="<?php echo $url_site; ?>/srnn.htm/srnn" rel="srnn" class="bt-play-srnn containerBox">

                  <div class="text-container">

                    <p class="title font01">

                      Ouça a Rádio SRNN

                    </p>

                    <p class="text">

                      Site da Rádio SRNN em construção, enquanto o site não é lançado aproveite para ouvir a rádio, clique aqui!

                    </p>

                  </div>

                </a>

                <span class="icon"></span>

              </div>

            </div>



          </div> <!-- Content -->



        </div> <!-- END: Class Wrapper -->



      </div> <!-- END: Content-Wrapper -->



    </div> <!-- END: Main -->



  </div><!-- END: Wrapper -->



<?php

	include "../../inc/footer.php";

	include "../../inc/scripts.php";

	include "../../inc/scripts-home.shtml";

?>

  <script src="../../js/home.js"></script>

</body>

</html>