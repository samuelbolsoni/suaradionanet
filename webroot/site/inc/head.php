  <meta charset="utf-8"/>
  <title>Sua Rádio Na Net</title>
  <meta property="og:title" content="Sua Rádio Na Net"/>
  <meta property="og:url" content="http://www.suaradionanet.com.br/"/>
  <meta property="og:image" content="../../img/logos/logo-radios.png"/>
  <meta property="og:site_name" content="Sua Rádio Na Net"/>
  <!-- <meta property="fb:admins" content="USER_ID"/> -->
  <meta property="og:description"
        content="Sua Rádio na Net é uma empresa especializada em colocar sua radio na internet, fazendo o melhor streaming para web rádios com servidores dedicados"/>
  <meta http-equiv="content-language" content="pt-br" />
  <meta name="robots" content="ALL" />
  <meta name="title" content="SUA RÁDIO NA NET - STREAMING DE QUALIDADE PARA SUA RADIO WEB" />
  <meta name="description" content="Sua Rádio na Net é uma empresa especializada em colocar sua radio na internet, fazendo o melhor streaming para web rádios com servidores dedicados" />
  <meta name="keywords" content="streaming, sua radio, radio online, radio web, broadcast, shoutcast, simplecast, manual simplecast, simplecast video aula, aovivo, hospedagem, hospedagem radios" />
  <meta name="url" content="http://www.suaradionanet.net" />
  <link rel="shortcut icon" href="../../img/favicon-radios.ico" type="image/x-icon" />
  <link rel="stylesheet" href="../../css/3rd_party/normalize.css" />
  <link rel="stylesheet" href="../../css/core/style.css" />
  <script src="../../js/core/crossbrowser/crossbrowser-0.2.js"></script>
  <!--[if lt IE 9]>
    <script src="../../js/3rd_party/html5shiv.js"></script>
  <![endif]-->