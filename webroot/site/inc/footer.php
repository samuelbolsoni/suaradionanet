<footer id="footer-wrapper">
  <div id="footer">
    <section id="section-footer">
      <article>
        <header class="instTitle" title="Institucional">INSTITUCIONAL</header>
        <a class="font04 inst-links" href="../../../../../empresa.htm">A EMPRESA</a>
        <a class="font04 inst-links" href="../../../../../servidores.htm">SERVIDORES</a>
        <a class="font04 inst-links" href="../../../../../tutoriais.htm">TUTORIAIS</a>
        <a class="font04 inst-links" href="../../../../../como_montar_uma_web_radio.htm">MONTE SUA WEB RADIO</a>
      </article>
      <div class="footer-separator"></div>
      <article>
        <header class="solucTitle" title="Soluções">SOLUÇÕES</header>
        <a class="font04 inst-links" href="../../../../../valores_radionanet.htm">STREAMING SHOUTCAST</a>
        <a class="font04 inst-links" href="../../../../../valores_radionanet.htm">STREAMING WINDOWS MEDIA</a>
        <a class="font04 inst-links" href="../../../../../tipos_streaming.htm">STREAMING AAC PLUS HD AUDIO</a>
        <a class="font04 inst-links" href="../../../../../site_administravel.htm">SITE ADMINISTRAVEL GRÁTIS</a>
      </article>
      <div class="footer-separator"></div>
      <article>
        <header class="atendTitle" title="Atendimento">ATENDIMENTO</header>
        <a class="font04 inst-links" href="../../../../../faq_streaming.htm">DÚVIDAS (FAQ)</a>
        <a class="font04 inst-links" href="javascript:chat_online();">CHAT AO VIVO</a>
        <a class="font04 inst-links" href="../../../../../contato.html">CONTATO</a>
        <a class="font04 inst-links" href="../../../../../confirme.htm">CONFIRMAR PAGAMENTO</a>
      </article>
      <div class="footer-separator"></div>
      <article class="last-column">
        <p class="font04 inst-links">SP: (11) 4063 8601 - SÃO PAULO</p>
        <p class="font04 inst-links">RS: (51) 4063 8108 - PORTO ALEGRE</p>
        <p class="font04 inst-links">BA: (71) 4062 9270 - BAHIA</p>
      </article>
    </section>
    <div id="w8s-group">
      <p>TODOS OS DIREITOS RESERVADOS - 2012</p>
      <a href="http://www.w8s.com.br/" class="w8slogo" target="_blank">
        <img src="../../img/logos/w8s.png" alt="W8S Simply Creative" title="W8S Simply Creative" />
      </a>
    </div>
  </div>
  <!-- Google Code for Compras Conversion Page -->
  <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 990151531;
    var google_conversion_language = "pt";
    var google_conversion_format = "2";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "HmdACI29tgYQ64aS2AM";
    var google_conversion_value = 0;
    /* ]]> */
  </script>
  <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
  </script>
  <noscript>
    <div style="display:inline;">
      <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/990151531/?value=0&amp;label=HmdACI29tgYQ64aS2AM&amp;guid=ON&amp;script=0"/>
    </div>
  </noscript>
</footer>

<?php mysql_close(); ?>