<script src="../../js/3rd_party/jquery-1.8.0.min.js"></script>
<!--<script src="../../js/3rd_party/contato.js"></script>-->
<script src="../../js/3rd_party/jquery.easing.1.3.js"></script>
<script src="../../js/3rd_party/modernizr.js"></script>
<!--[if lt IE 10]>
  <script src="../../js/3rd_party/cufon-yui.js"></script>
  <script src="../../js/3rd_party/AvantGarde_LT_CondMedium_500-AvantGarde_LT_CondMedium_700.font.js"></script>
  <script src="../../js/3rd_party/AvantGarde_LT_CondBook_400.font.js"></script>
  <script src="../../js/core/start-cufon/start-cufon.js"></script>
<![endif]-->
<?php
if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 10') > 0) : ?>
  <script src="../../js/3rd_party/cufon-yui.js"></script>
  <script src="../../js/3rd_party/AvantGarde_LT_CondMedium_500-AvantGarde_LT_CondMedium_700.font.js"></script>
  <script src="../../js/3rd_party/AvantGarde_LT_CondBook_400.font.js"></script>
  <script src="../../js/core/start-cufon/start-cufon.js"></script>
<?php endif;  ?>
<script src="../../js/core/menu-controller/menu-controller.js"></script>
<script src="../../js/3rd_party/player/swfobject.js"></script>
<script src="../../js/bootstrap.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function($) {
    
    var dataText = "Estou ouvindo a rádio";
    var dataShare = 'https://twitter.com/share?url=http://www.suaradionanet.com.br/&lang=pt&text='+dataText; 
    $('.bt-redes-sociais.twitter').bind('click', function(e) {
      e.preventDefault();
      window.open(dataShare, 'Compartilhar no Twitter', 'fullscren=no, height=400, width=500');
    }); 
    jQuery('.bt-redes-sociais.facebook').bind('click', function(e) {
      e.preventDefault();
      window.open("https://www.facebook.com/sharer/sharer.php?u=http://"+window.location.hostname+"/labs/radios_site/", 'Compartilhar no Twitter', 'fullscren=no, height=400, width=500');
    });
  });
</script>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>
<script type="text/javascript">
  _uacct = "UA-522813-3";urchinTracker();
</script>