jQuery(document).ready(function() {

	$("#fone").mask("(99) 9999.9999?9");
	$("#cel").mask("(99) 9999.9999?9");
	$("#data").mask("99/99/9999 - 99h99m");

	$('#pago').priceFormat({
		prefix : 'R$ ',
		centsSeparator : ',',
		thousandsSeparator : '.'
	});

	$("#enviar").click(function() {
		
				
		var data = {};
		var cont_ = 0;
		var email = jQuery("#email").val();
		var mensagem = jQuery("#mensagem").val();

		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		$('.error').removeClass('error');
		jQuery('.required').each(function() {
			console.log(this);
			if (($(this).val() == "" || $(this).val() == $(this).attr('placeholder')) && $(this).attr("id") != undefined) {
				cont_++;
				if ($(this).hasClass('select')) {
					$(this).parent().parent().parent().addClass("error");
				} else {
					if (!$(this).hasClass('input-file')) {
						$(this).parent().parent().parent().addClass("error");
					}
				}
			} else {
				data[$(this).attr("id")] = $(this).val();
			}
		});

		if (!emailReg.test(email)) {
			cont_++;
			$("#email").parent().parent().parent().addClass("error");
		}

		if (cont_ == 0) {

	    $.ajax({
				url : '../confirmar-pagamento/email.php',
				dataType : 'html',
				data : data,
				type : 'POST',
				success : function(dataServer) {
					var retorno = dataServer;

					if (retorno.indexOf("SUCESSO") > 0) {

						$("input[type=text],textarea,select").each(function() {
							$(this).val("");
						});

						$('.warning-msg').removeClass('incorrect').addClass('correct');
						$('.warning-msg').find('.font-msg').html('Seus dados foram enviados com sucesso');
						$('.warning-msg').fadeIn();
						// /document.getElementById('captcha').src = '../../inc/securimage/securimage_show.php?' + Math.random();
						$("#captcha").attr('src', '../../inc/securimage/securimage_show.php?' + Math.random());
					}
					if (retorno.indexOf("CAPTCHA") > 0) {
						$("#captc").parent().parent().parent().addClass('error');
						$('.warning-msg').removeClass('correct').addClass('incorrect');
						$('.warning-msg').find('.font-msg').html('Por favor preencha o Captcha corretamente');
						$('.warning-msg').fadeIn();
					}
				}
			});

		} else {
			$('.warning-msg').removeClass('correct').addClass('incorrect');
			$('.warning-msg').find('.font-msg').html('Por Favor preencha corretamente todos os dados');
			$('.warning-msg').fadeIn();
		}
	});

});

