 jQuery(document).ready(function() {
  $("#enviar").click(function() {
      //var data = {};
      var cont_ = 0;
      var email = $("#email").val();
      var mensagem = $("mensagem").val();

      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      
      $("input[type=\"text\"],textarea,select").each(function(){
          if($(this).val() == ""){
            cont_++;
            if($(this).hasClass('select')) {
              jQuery(this).parent().parent().parent().parent().addClass("error");
            } else {
              jQuery(this).parent().parent().parent().addClass("error");
            }
          }
        });
      if(!emailReg.test(email)) {
        cont_++;
        jQuery(this).parent().parent().parent().addClass("error");
      }
      //console.log(cont_)
      if(cont_ == 0) {

         alert("ok");

        jQuery("input[type=text],textarea,select").each(function(){
            data[$(this).attr("id")] = $(this).val();
          });
        
        $.ajax({
            url: '../contato/email.php',
            dataType: 'html',
            data: data,
            type: 'POST',
            success: function(dataServer) {
              var retorno = dataServer;
              if(retorno.indexOf( "SUCESSO" ) > 0){
                  $('.warning-msg').removeClass('incorrect').addClass('correct');
                  $('.warning-msg').find('.font-msg').html('Seus dados foram enviados com sucesso');
                  $('.warning-msg').fadeIn();
                  jQuery("input[type=text],textarea,select").each(function(){
                    jQuery(this).val( "" );
              });
              } else {
                $('.warning-msg').removeClass('correct').addClass('incorrect');
                $('.warning-msg').find('.font-msg').html('Por Favor preencha corretamente todos os dados');
                $('.warning-msg').fadeIn()

              }          
            }
          }); 
      
      } else {
        $('.warning-msg').removeClass('correct').addClass('incorrect');
        $('.warning-msg').find('.font-msg').html('Por Favor preencha corretamente todos os dados');
        $('.warning-msg').fadeIn()
      }
  });
  
});

