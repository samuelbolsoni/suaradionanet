function faq() {
	var pergunta = $("#question").val();
	var captc = $("#captc").val();
	if (pergunta == "") {
		$('#error-msg-wrap').addClass('incorrect');
		$('#error-msg-wrap').find('.font-msg').html('Digite uma pergunta!');
		$('#error-msg-wrap').fadeIn();
		return false;
	} else {
		if ($('#captc').val() == "") {
			$('#error-msg-wrap').addClass('incorrect');
			$('#error-msg-wrap').find('.font-msg').html('Preencha o Captcha!');
			$('#error-msg-wrap').fadeIn();
			return false;
		}
		$.ajax({
			url : "../faq/insere.php",
			type : "post",
			dataType : "html",
			data : "pergunta=" + pergunta + "&captc=" + captc,
			cache : false,
			success : function(dataServer) {
				if (dataServer.indexOf("CAPTCHA") > 0) {

					$('#error-msg-wrap').addClass('incorrect');
					$('#error-msg-wrap').find('.font-msg').html('Preencha o Captcha corretamente!');
					$('#error-msg-wrap').fadeIn();
					return false;
				} else {
					$("#retorno").html(dataServer);
					$('#error-msg-wrap').removeClass('incorrect');
					$('#error-msg-wrap').find('.font-msg').html('Pergunta enviada com sucesso!');
					$('#error-msg-wrap').fadeIn();
					$(".loader").hide();
					document.getElementById('captcha').src = '../../inc/securimage/securimage_show.php?' + Math.random();
					$("#captcha").attr('src', '../../inc/securimage/securimage_show.php?' + Math.random());
					$("#question").val("");
					$("#captc").val("");
					setTimeout(function() {
						$('.infos-wrap').removeClass('open');
						$('.infos-wrap').slideUp();
						$('#error-msg-wrap').fadeOut();
					}, 1500);

				}
			}
		});
	}
}

function faqbuscar() {
	var busca = $("#faqbusca").val();
	jQuery.ajax({
		url : "../faq/busca.php",
		type : "post",
		dataType : "html",
		data : "busca=" + busca,
		cache : false,
		success : function(dataServer) {
			$("#section").html(dataServer);
		},
		complete : function() {
			$(".loader").hide();
		}
	});
}

function show() {
	var busca = $("#faqbusca").val();
	jQuery.ajax({
		url : "../faq/busca.php",
		type : "post",
		dataType : "html",
		data : "busca=" + busca,
		cache : false,
		success : function(dataServer) {
			$("#section").html(dataServer);
		},
		complete : function() {
			$(".loader").hide();
		}
	});
}
