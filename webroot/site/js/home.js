var select_pr = 0;
function planos(val) {
	var streaming = $("#streaming").val();
	if (!val) {
		var ouvintes = $("#ouvintes").val();
	}
	//var placeholder = $("#streaming").attr("data-placeholder");
	//alert( placeholder );
	if (streaming != 0 && select_pr == 0) {
		select_pr = 1;
		$.ajax({
			url : "planos.php",
			type : "post",
			dataType : "json",
			data : {
				streaming : streaming,
				ouvintes : ouvintes
			},
			cache : false,
			success : function(json) {
				$("#grid1").empty();
				if (val) {
					$("#grid2").empty();
					$("#grid2").html(json.ouvintes);
					$('#grid2 .select-home').sexyCombo({
						skin : 'sel-home',
						showListCallback : function() {
							var self = this;
							var calc = 1;
							if (self.listItems.length > 1 && self.listItems.length <= 5) {
								calc = self.listItems.length;
							}
							if (self.listItems.length > 5) {
								calc = 5;
							}
							jQuery(this.listWrapper).css({
								height : 24.3 * calc,
								width : jQuery(self.wrapper).width() + 1
							});
						}
					});
				}
				$("#grid3").empty();
				$("#grid1").html(json.streaming);
				$("#grid3").html(json.qualidade);
				$('#grid1 .select-home, #grid3 .select-home').sexyCombo({
					skin : 'sel-home',
					showListCallback : function() {
						var self = this;
						var calc = 1;
						if (self.listItems.length > 1 && self.listItems.length <= 5) {
							calc = self.listItems.length;
						}
						if (self.listItems.length > 5) {
							calc = 5;
						}
						jQuery(this.listWrapper).css({
							height : 24.3 * calc,
							width : jQuery(self.wrapper).width() + 1
						});
					}
				});
				select_pr = 0;
			}
		})
	} else {

	}
}

function valor() {
	var ouvintes = $("#ouvintes").val();
	var streaming = $("#streaming").val();
	var qualidade = $("#qualidade").val();

	$.ajax({
		url : "valor.php",
		type : "post",
		dataType : "json",
		data : {
			streaming : streaming,
			qualidade : qualidade,
			ouvintes : ouvintes
		},
		cache : false,
		success : function(json) {
			var link = '../../../../../cadastro.htm/' + json.plano;
			$('#bt-make-plan').attr('href', link);
			if (!json.value) {
				$(".fire-mini").hide();
				$(".fone-mini").hide();
			} else {
				if (json.value >= 39.6) {
					$(".fone-mini").show();
					$(".fire-mini").show();
				} else {
					if (json.value < 33.5) {
						$(".fire-mini").hide();
						$(".fone-mini").hide();
					} else {
						$(".fire-mini").show();
						$(".fone-mini").hide();
					}
				}
			}
			$("#valor").html(json.valor);
		}
	})
}

var newPopup = false;

jQuery(document).ready(function($) {
	$('.bt-player, .bt-play-srnn').live({
		click : function(e) {
			e.preventDefault();
			e.stopPropagation();
			var link = $(this).attr('href');
			var rel = $(this).attr('rel');
			var w = 485, h = 297;
			if (rel == 'aacplus' || rel == 'shoutcast' || rel == 'srnn') {
				w = 300, h = 85;
			}
			createPopup(link, w, h, rel);
		}
	});
	function createPopup(url, strWidth, strHeight) {
		var tools = "resizable,toolbar=no,location=no,scrollbars=no,width=" + strWidth + ",height=" + strHeight + ",left=20,top=40";
		var resize = false;
		if (newPopup) {
			resize = true;
		}
		newPopup = window.open(url, 'newWin', tools);
		if (resize) {
			strHeight += 70;
			newPopup.resizeTo(strWidth, strHeight);
		}
		newPopup.focus();
	}

});
