var StartCufon = function (settings) {
  this.settings = settings;
  this.init();
};

StartCufon.prototype.init = function () {
  for(key in this.settings) {
    this.cufonThis(this.settings[key].classReplace, this.settings[key].fontFamily);
  }
};

StartCufon.prototype.cufonThis = function (element, family) {
  Cufon.replace(element, {
    fontFamily: family,
    hover: true
  })
};