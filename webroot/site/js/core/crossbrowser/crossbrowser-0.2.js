// W8 Simply Creative Cross Browser Script
// Created by André Luzzardi
// Version: 0.2
// This script verify the browser and automatically
// create and add at the end of head tag the css for 
// specified browser.
// You can pass an object contening this information below
// var W8CrossSettings = {
//    arrRegex : [/Safari/, /Chrome/, /MSIE 7/, /MSIE 8/, /MSIE 9/],
//    html5    : true
// };
var W8_CrossBrowser = function (settings) {
  this.arrRegex = settings.arrRegex;
  this.html5 = false;
  if(settings.html5 != undefined) {
    this.html5 = true;
  }
  this.browser;
  this.os;
  this.init();
};

W8_CrossBrowser.prototype.init = function () {
  this.verifyBrowser();
};

W8_CrossBrowser.prototype.verifyBrowser = function () {
  var b = navigator.userAgent;
  if(this.arrRegex == undefined) {
    this.arrRegex = [/Safari/,/Chrome/,/Opera/,/MSIE 7/,/MSIE 8/,/MSIE 9/,/MSIE 10/,/Firefox/];
  }
  this.osRegex = [/Linux/, /Mac/, /Windows/];
  for(var i = 0; i < this.arrRegex.length; i++) {
    if(this.arrRegex[i].test(b)) {
      var string = this.arrRegex[i].toString();
      this.browser = string.substring(1,string.length-1);
    }
  }

  for(var i = 0; i < this.osRegex.length; i++) {
    if(this.osRegex[i].test(b)) {
      var string = this.osRegex[i].toString();
      this.os = string.substring(1,string.length-1);
    }
  }

  
  this.getArchiveName(this.browser, this.os);
};

W8_CrossBrowser.prototype.getArchiveName = function (browser, os) {
  this.archiveName;
  if(browser != undefined) {
    switch(browser) {
      case 'Safari' : this.archiveName = 'chrome.css';
                      break;
      case 'Chrome' : this.archiveName = 'chrome.css';
                      break;
      case 'Opera'  : this.archiveName = 'opera.css';
                      break;
      case 'MSIE 7' : this.archiveName = 'ie7.css';
                      if(this.html5) {
                        this.startHtmlShiv();
                      }
                      this.adjustPNGImages();
                      break;
      case 'MSIE 8' : this.archiveName = 'ie8.css';
                      if(this.html5) {
                        this.startHtmlShiv();
                      }
                      this.adjustPNGImages();
                      break;
      case 'MSIE 9' : this.archiveName = 'ie9.css';
                      break;
      case 'MSIE 10' : this.archiveName = 'ie10.css';
                      break;
      case 'Firefox': this.archiveName = 'firefox.css';
                      break;
    }
    this.createLinkCss(this.archiveName);
    if(/\s/.test(browser)) {
      browser = browser.replace(' ', '-');
    }
    browser = " "+browser;
  } else {
    browser = "";
  }

  document.getElementsByTagName('html')[0].setAttribute('class', os.toLowerCase() + browser.toLowerCase());
  
};

W8_CrossBrowser.prototype.adjustPNGImages = function () {
  var i;
  for (i in document.images) {
      if (document.images[i].src) {
          var imgSrc = document.images[i].src;
          if (imgSrc.substr(imgSrc.length-4) === '.png' || imgSrc.substr(imgSrc.length-4) === '.PNG') {
              document.images[i].style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled='true',sizingMethod='crop',src='" + imgSrc + "')";
          }
      }
  }
};

W8_CrossBrowser.prototype.createLinkCss = function (archiveName) {
  var css = document.createElement('link');
  css.setAttribute('rel','stylesheet');
  css.setAttribute('href','../../css/core/'+archiveName);
  document.getElementsByTagName('head')[0].appendChild(css);
};

W8_CrossBrowser.prototype.startHtmlShiv = function () {
  var js = document.createElement('script');
  js.setAttribute('src','../../js/3rd_party/html5shiv.js');
  document.getElementsByTagName('head')[0].appendChild(js);
};

var W8CrossSettings = {
   arrRegex : [/Safari/, /Chrome/, /MSIE 7/, /MSIE 8/, /MSIE 9/, /MSIE 10/, /Opera/]
};

var W8startCSS = new W8_CrossBrowser(W8CrossSettings);