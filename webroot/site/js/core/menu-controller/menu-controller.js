var MenuController = function () {
  this.selector = '.page-title';
  this.menuSelector = '#nav li a';
  this.menuActive = "";
  
  this.init();
  
};

MenuController.prototype.init = function () {
  this.menuActive = $(this.selector).attr('data-active');
  this.controlActiveMenu();
};

MenuController.prototype.controlActiveMenu = function () {
  var self = this;
  $(this.menuSelector).removeClass('active');
  $(this.menuSelector+'[data-active="'+this.menuActive+'"]').addClass('active');
};