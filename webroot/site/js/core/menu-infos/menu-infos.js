var MenuInfos = function () {
  this.wrapperElement = '#content-infos';
  this.buttonClose = '.btn-close';
  this.containerElement = '#content-wrapper';
  this.clicked = "";
  this.init();
};

MenuInfos.prototype.init = function () {
  if($('html').hasClass('no-csstransitions')) {
    var js = document.createElement('script');
    js.setAttribute('src','../../js/core/menu-infos/animate-menu-infos.js');
    document.getElementsByTagName('body')[0].appendChild(js);
  }
  this.bindEvents();
};

MenuInfos.prototype.bindEvents = function () {
  var self = this;
  $('#menu-infos > ul > li > a').on({
    click : function (e) {
      e.preventDefault();
      self.newClick = $(this);
      self.oldClick = $('#menu-infos ul li a.active');
      if($(this).hasClass('active')) {
          self.closeInfo();
      } else {
          if(!$(self.wrapperElement).hasClass('opened')) {
            $(this).addClass('active');
            self.openInfo($(this));
          } else {
            $(this).addClass('active');
            self.closeInfo($(this), function() {
              self.openInfo(self.newClick);
            });
          }
      }  
    }
  })
  
  $(this.buttonClose).on('click', function(e) {
    e.preventDefault();
    self.oldClick = $('#menu-infos ul li a.active');
    self.closeInfo();
  })
};

MenuInfos.prototype.closeInfo = function (clicked, callback) {
  var self = this;
  if(clicked == undefined) {
    if($('html').hasClass('no-csstransitions')) {
      W8animateMenuInfos.animateOn(self.oldClick, true);
    } else {
      $(self.oldClick).removeClass('active');
    }
     $(self.buttonClose).hide();
      $(self.wrapperElement)
        .slideUp(function() {
          $(this).removeAttr('class');
          $(self.wrapperElement+' > div').hide();
        });
      $(self.wrapperElement).next().delay(300).removeClass('open');
      setTimeout(function() {

      }, 300)
  } else {
    if($('html').hasClass('no-csstransitions')) {
      W8animateMenuInfos.animateOn(self.oldClick, true);
    } else {
      $(self.oldClick).removeClass('active');
    }
    var rel = $(clicked).attr('data-rel');
    $(self.buttonClose).hide();
    $(self.wrapperElement)
      .slideUp(function() {
        $(this).removeAttr('class');
        $(self.wrapperElement+' > div').hide();
        if(typeof callback === 'function') {
          callback();
        }
      });
    
  }
};

MenuInfos.prototype.openInfo = function (clicked) {
  var self = this;
  var rel = $(clicked).attr('data-rel');
  $(self.wrapperElement+' > div[data-rel="'+rel+'"]').show();
  $(self.wrapperElement)
    .addClass(rel)
    .addClass('opened')
    .slideDown(function() {
      $(self.buttonClose).fadeIn(100);
    });
  $(self.wrapperElement).next().addClass('open');
}