var AnimateMenuInfos = function () {
  this.objCss = {
    default1: {
      height: 44,
      marginTop: 11
    },
    default2: {
      height: 53,
      marginTop: 18
    }
  }
  this.init();
};

AnimateMenuInfos.prototype.init = function () {
  var self = this;
  this.bindEvents();
  jQuery('#menu-infos > ul > li > a').css({
    height: self.objCss.default1['height']
  });
  jQuery('#menu-infos > ul > li > a').children('span').css({
    marginTop: self.objCss.default1['marginTop']
  });
};

AnimateMenuInfos.prototype.bindEvents = function () {
  var self = this;
  jQuery('#menu-infos > ul > li > a').hover(
    function(e) {
      e.preventDefault();
      if(!$(this).hasClass('active')) {
        self.animateOff($(this));
      }
    },
    function(e) {
      e.preventDefault();
      if(!$(this).hasClass('active')) {
        self.animateOn($(this));
      }
    }
  );
};

AnimateMenuInfos.prototype.animateOn = function (element, removeActive, callback) {
  var self = this;
  if(removeActive == true) {
    $(element).removeClass('active');
  }
  $(element).animate({
    height: self.objCss.default1['height']
  }, {duration: 200, queue: false});
  $(element).children('span').animate({
    marginTop: self.objCss.default1['marginTop']
  }, {duration: 200, queue: false}, function() {
    if(typeof callback === 'function') {
      callback();
    }
  });
};

AnimateMenuInfos.prototype.animateOff = function (element, removeActive, callback) {
  var self = this;
  $(element).animate({
    height : self.objCss.default2['height']
  }, {duration: 200, queue: false});
  $(element).children('span').animate({
    marginTop : self.objCss.default2['marginTop']
  }, {duration: 200, queue: false});
  
};

var W8animateMenuInfos = new AnimateMenuInfos();