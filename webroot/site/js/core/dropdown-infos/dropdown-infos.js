var DropdownInfos = function () {
  if(jQuery('.dropdown-infos').html() != undefined) {
    this.allInfosOpen = [];
    this.inDrop = false;
    this.init();
  }
};

DropdownInfos.prototype.init = function () {
  var self = this;
  jQuery('.dropdown-infos article header a').each(function() {
    if(jQuery(this).hasClass('open')) {
      jQuery(this)
        .parent()
        .parent()
        .addClass('opened');
      jQuery(this)
        .parent()
        .next()
        .show();
      jQuery(this)
        .children('h1')
        .children('span')
        .removeClass('plus')
        .addClass('minus');
    } else {
      jQuery(this).addClass('close');
      jQuery(this)
        .children('h1')
        .children('span')
        .removeClass('minus')
        .addClass('plus');
    }
  });
  if(window.location.hash) {
      var hash = window.location.hash.substring(1);
      self.openThis = jQuery('#'+hash).children('header').children('a');
      console.log(self.openThis);
      jQuery('.dropdown-infos article header a.open').each(function() {
        self.allInfosOpen.push(jQuery(this))
      });
      self.closeAllInfoAndOpen();
  }
  this.bindEvents();
};

DropdownInfos.prototype.bindEvents = function () {
  var self = this;
  jQuery('.dropdown-infos article header a').live({
    click : function (e) {
      e.preventDefault();
      if(!self.inDrop) {
        self.inDrop = true;
        if(jQuery(this).hasClass('open')) {
          self.closeInfo(jQuery(this));
        } else {
          self.openThis = jQuery(this);
          jQuery('.dropdown-infos article header a.open').each(function() {
            self.allInfosOpen.push(jQuery(this))
          });
          self.closeAllInfoAndOpen();
        }
      }
    }
  })
};

DropdownInfos.prototype.closeInfo = function (elem) {
  var self = this;
  jQuery(elem)
    .removeClass('open')
    .addClass('close')
    .children('h1')
    .children('span')
      .removeClass('minus')
      .addClass('plus');
  jQuery(elem)
    .parent()
    .parent()
    .removeClass('opened');
  jQuery(elem)
    .parent()
    .next()
    .slideUp(function() {
      self.inDrop = false;
    });
};

DropdownInfos.prototype.closeAllInfoAndOpen = function () {
  var self = this;
  var closeThis = self.allInfosOpen.pop();
  if(closeThis != undefined) {
    jQuery(closeThis)
      .removeClass('open')
      .addClass('close')
      .children('h1')
      .children('span')
        .removeClass('minus')
        .addClass('plus');
    jQuery(closeThis)
      .parent()
      .parent()
      .removeClass('opened');
    jQuery(closeThis)
      .parent()
      .next()
      .slideUp(function() {
        self.closeAllInfoAndOpen();
      });
  } else {
    this.openInfo()
  }
}

DropdownInfos.prototype.openInfo = function () {
  var self = this;
  jQuery(this.openThis)
    .removeClass('close')
    .addClass('open')
    .children('h1')
    .children('span')
      .removeClass('plus')
      .addClass('minus')
  jQuery(this.openThis)
      .parent()
      .parent()
      .addClass('opened');
  jQuery(this.openThis)
    .parent()
    .next()
    .slideDown(function() {
      self.inDrop = false;
    });
};