jQuery(document).ready(function() {

	jQuery("#assunto").on('change', function() {
		if (this.value == "suporte@suaradionanet.net") {
			alert ("AVISO! Para suporte imediato entre em contato sempre pelo chat online. Caso não consiga resolver seu problema use este formulário. Seu pedido será encaminhado para a central de suporte e respondido em até 12 horas, podendo ser a qualquer momento, exceto sábados, domingos e feriados, quando não temos expediente.");
		}
	  //alert( this.value ); // or $(this).val()
	});

	jQuery("#enviar").click(function() {
		var data = {};
		var cont_ = 0;

		var email = jQuery("#email").val();
		var mensagem = jQuery("#mensagem").val();

		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

		jQuery(".required").each(function() {
			if ((jQuery(this).val() == "" || jQuery(this).val() == jQuery(this).attr('placeholder')) && jQuery(this).attr("id") != undefined) {
				cont_++;
				if (jQuery(this).hasClass('select')) {
					jQuery(this).parent().parent().parent().parent().addClass("error");
				} else {
					jQuery(this).parent().parent().parent().addClass("error");
				}
			}
		});
		if (!emailReg.test(email)) {
			cont_++;
			jQuery("#email").parent().parent().parent().addClass("error");
		}



		if (cont_ == 0) {

			jQuery(".required").each(function() {
				if (jQuery(this).attr("id") != undefined) {
					data[jQuery(this).attr("id")] = jQuery(this).val();

					if (jQuery(this).hasClass('select')) {

						jQuery(this).parent().parent().parent().parent().removeClass("error");

					} else {
						jQuery(this).parent().parent().parent().removeClass("error");
					}
				}

			});

			jQuery.ajax({
				url : '../contato/email.php',
				dataType : 'html',
				data : data,
				type : 'POST',
				success : function(dataServer) {
					var retorno = dataServer;

					if (retorno.indexOf("SUCESSO") > 0) {

						jQuery("input[type=text],textarea,select").each(function() {
							jQuery(this).val("");
						});

						jQuery('.warning-msg').removeClass('incorrect').addClass('correct');
						jQuery('.warning-msg').find('.font-msg').html('Seus dados foram enviados com sucesso');
						jQuery('.warning-msg').fadeIn();
						document.getElementById('captcha').src = '../../inc/securimage/securimage_show.php?' + Math.random();
						$("#captcha").attr('src', '../../inc/securimage/securimage_show.php?' + Math.random());
					}
					if (retorno.indexOf("CAPTCHA") > 0) {

						jQuery('.warning-msg').removeClass('correct').addClass('incorrect');
						jQuery('.warning-msg').find('.font-msg').html('Por favor preencha o Captcha corretamente');
						//jQuery('.warning-msg').find('.font-msg').html('Por Favor preencha corretamente todos os dados');
						jQuery('.warning-msg').fadeIn()
					}
				}
			});

		} else {
			jQuery('.warning-msg').removeClass('correct').addClass('incorrect');
			jQuery('.warning-msg').find('.font-msg').html('Por Favor preencha corretamente todos os dados');
			jQuery('.warning-msg').fadeIn()
		}
	});

});

