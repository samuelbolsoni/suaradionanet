jQuery(document).ready(function() {
  
  if($('#cidade').html() != undefined) {
    tabChange('#estado', '#cidade', 5);
  }

	var menuController = new MenuController();

	setTimeout(function() {
		swfobject.registerObject("main-player");
		var swfObj = obj = swfobject.getObjectById("main-player");
		$.post("../../js/checkplayersession.php", function(data) {
				if (data == 'playing' || data == 0) {
					try {
						swfObj.playpause();
						$("#playerbutton").addClass('play');
						//$(this).addClass('play');
					} catch (e) {

					}
			}
		});
	}, 1000);

	// Fonts Internet Explorer
	if (navigator.userAgent.indexOf('MSIE') != -1) {

		var cufonObj = new StartCufon({
			replaceAvantGardeMedium : {
				classReplace : '.font01',
				fontFamily : 'AvantGarde LT CondMedium'
			},
			replaceAvantGardeBold : {
				classReplace : '.font02',
				fontFamily : 'AvantGarde LT CondBold'
			},
			replaceAvantGardeCondBook : {
				classReplace : '.font04',
				fontFamily : 'AvantGarde LT CondBook'
			}
		});
		
		setTimeout(function() {
		  Cufon.refresh();
		}, 10);

	}

	swfobject.registerObject("main-player");
	var swfObj = obj = swfobject.getObjectById("main-player");
	$('.bt-radio').live({
		click : function() {
			if ($(this).hasClass('play')) {
				$(this).removeClass('play');
				try {
					swfObj.playpause();
				} catch (e) {
					console.log(e);
				}
				$.post("../../js/playersession.php", {
					session : "paused"
				}, function(data) {
				});
			} else {
				try {
					swfObj.playpause();
				} catch (e) {
					console.log(e);
				}
				$(this).addClass('play');
				$.post("../../js/playersession.php", {
					session : "playing"
				}, function(data) {
				});
			}
		}
	});

});

function getFlashMovieObject(movieName) {
	if (window.document[movieName]) {
		return window.document[movieName];
	}
	if (navigator.appName.indexOf("Microsoft Internet") == -1) {
		if (document.embeds && document.embeds[movieName])
			return document.embeds[movieName];
	} else// if (navigator.appName.indexOf("Microsoft Internet")!=-1)
	{
		return document.getElementById(movieName);
	}
}

function tabChange(idFocused, idFocusIn, tabindex) {
  setTimeout(function() {
    $(idFocused).next('input').attr('tabindex', tabindex);
    $(idFocused).next('input').bind({
      'keydown.tabChange' : function (e) {
        //e = e || window.event;
        var charCode = e.which || e.keyCode;
        if(charCode == 9) {
          $(idFocusIn).focus();
        }
      }
    });
  }, 200)
}